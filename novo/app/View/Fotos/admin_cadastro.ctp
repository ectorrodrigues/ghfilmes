<?PHP echo $this->GFunction->addShortScript('objArray(\'conteudo\', '.$Conteudo['Conteudo']['id'].', JSON.parse(\''. addslashes( jsonEncode($Conteudo) ) .'\') );'); ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel mb10">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-picture-o"></i> <?PHP echo $title_for_layout; ?>
                </div>
			</div>            
			<div class="panel-body">
            
                <div class="well well-sm">
                    <p class="text-warning"><strong>Aten��o:</strong></p>                    
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-check text-danger"></i> Permitido apenas imagens (.jpg, .png, .gif).</li>
                        <li><i class="fa-li fa fa-check text-danger"></i> Imagens com tamanho m�ximo de 1mb.</li>
                    </ul>
                </div>
            
                <form action="<?PHP echo Router::url('/', true); ?>admin/fotos/ajax" class="dropzone" id="dropZone">
                    <input type="hidden" name="conteudo_id" value="<?=$Conteudo['Conteudo']['id'];?>" />
                    <input type="hidden" name="tipo_acao" value="upload" />
                </form>
                
                <div id="msg_upload" class="mt10"></div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-body">
            
                <div>
                  <ul id="Grid"></ul>
                </div>
            
			</div>
		</div>
	</div>
</div>


<?PHP
echo $this->Html->css( array(  
                            Router::url('/', true) . 'js/vendor/plugins/dropzone/downloads/css/dropzone.css',
                            Router::url('/', true) . 'js/vendor/plugins/mfpopup/dist/magnific-popup.css',
                        ), array('inline' => false)
);

echo $this->Html->script( array( 
                            'vendor/plugins/dropzone/downloads/dropzone.min.js',
                            'vendor/plugins/mfpopup/dist/jquery.magnific-popup.min.js',
                        ), array('inline' => false) 
);
?>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
var Conteudo = null;
function listarFotos() {
    $.ajax({
		url: URL_BASE + "admin/fotos/ajax",
		data: { 'tipo_acao': 'listar_fotos', 'conteudo_id':<?=$Conteudo['Conteudo']['id'];?> },
		async: false,
        type: 'POST',
		dataType: "json",
        beforeSend: function() { dialogLoading(); },
        error: function( jqXHR, textStatus ) { closeLoading(); dialog('Erro', 'Problema na requisi��o.<br />Tente novamente mais tarde.') },
        success: function( data ) {
            if (data.status) {
                
                html = '';
                if (data.retorno.length>0) {
                    $.each(data.retorno, function(key, obj){                        
                        html += '<li class="template-upload">\
                                    <div class="upload-preview">\
                                            <span class="preview">\
                                                <img src="'+ timthumb(obj.Foto._Arquivo.img, 160, 120) +'" width="160" height="120" alt="foto" data-arquivo="'+obj.Foto.arquivo+'" '+((Conteudo.Conteudo.foto_destaque==obj.Foto.arquivo)?'class="destaque"':'')+' >\
                                            </span>\
                                    </div>\
                                    <div class="preview-buttons" id="foto_'+obj.Foto.id+'">\
                                            <a href="'+ obj.Foto._Arquivo.img_path +'" class="btn btn-info btn_foto mr5 btn-sm inline-object fancybox">\
                                                <i class="fa fa-eye"></i>\
                                            </a>\
                                            <a class="btn btn-danger bt_delete_foto mr5 btn-sm inline-object" title="Excluir">\
                                                <i class="fa fa-trash-o"></i>\
                                            </a>\
                                    </div>\
                                </li>';
                    });
                } else {
                    html = '<li class="template-upload">\
                        <div class="upload-preview">\
                            <span class="preview"><?PHP echo $this->Timthumb->image('/img/no-photo.png', array('width' => 160, 'zoom_crop'=> 1)); ?></span>\
                        </div>\
                        <div class="preview-buttons">\
                            <span class="text-white">Sem foto.</span>\
                        </div>\
                    </li>';
                }
                
                $('#Grid').html(html);
                
            } else {
                dialog('Erro', 'Problema ao executar a a��o.');
            }
            
            closeLoading();
		}
	});
}
var excluir_foto = function( obj_id, conteudo_id ) {
    if (obj_id) {
        var id = parseInt( obj_id.replace('foto_','') );
        if (id) {
            $.ajax({
        		url: URL_BASE + "admin/fotos/ajax",
                data: {
                    'tipo_acao': 'excluir_foto',
                    'id' : id,
                    'conteudo_id': conteudo_id
                },
                async: false,
                type: 'POST',
                beforeSend: function() { dialogLoading(); },
                complete: function() { closeLoading(); },
                dataType: "json",
                success: function( data ) {
                    if (data.status) {
                        dialog('Sucesso', 'Exclu�do com sucesso.');
                        $( '#'+obj_id ).closest('li.template-upload').fadeOut(function() {
                            $(this).remove();
                        });
                    } else {
                        dialog('Erro', 'Problema ao executar a a��o.<br />' + trataErroCake( data.error_input ));
                    }
                }
        	});
        } else {
            dialog('Erro', 'Problema ao executar a a��o.<br />' );
        }
    }
}
var destacarFoto = function( obj_id, conteudo_id ) {
    if (obj_id) {
        var id = parseInt( obj_id.replace('foto_','') );
        if (id) {
            $.ajax({
        		url: URL_BASE + "admin/fotos/ajax",
                data: {
                    'tipo_acao': 'destacar_foto',
                    'id' : id,
                    'conteudo_id': conteudo_id
                },
                async: false,
                type: 'POST',
                beforeSend: function() { dialogLoading(); },
                complete: function() { closeLoading(); },
                dataType: "json",
                success: function( data ) {
                    if (data.status) {
                        dialog('Sucesso', 'Marca��o alterada.');
                        $('ul#Grid').find('img').removeClass('destaque');
                        
                        if (data.foto_destaque!='') {
                            $('ul#Grid').find('img[data-arquivo="'+data.foto_destaque+'"]').addClass('destaque');
                        }
                        
                    } else {
                        dialog('Erro', 'Problema ao executar a a��o.<br />' + trataErroCake( data.error_input ));
                    }
                }
        	});
        } else {
            dialog('Erro', 'Problema ao executar a a��o.<br />' );
        }
    }
}

jQuery(document).ready(function() {
    
    Conteudo = objArray('conteudo', <?=$Conteudo['Conteudo']['id'];?>);
    
    listarFotos();
    Dropzone.options.dropZone = {
		paramName: "arquivo",
		maxFilesize: 10,
        acceptedFiles: 'image/*',
        thumbnailWidth: 104,
        dictFallbackMessage: "Seu navegador n�o suporta o upload de arquivos no modo arrastar.",
        dictFileTooBig: "O arquivo � muito grande ({{filesize}}Mb).<br /> Tamanho m�ximo permitido: {{maxFilesize}}Mb.",
        dictInvalidFileType: "Voc� n�o pode fazer upload de arquivos deste tipo.",
		dictDefaultMessage :
		'<i class="fa fa-cloud-upload"></i> \
		 <span class="main-text"><strong>Solte os arquivos aqui</strong></span><br />  \
         <span class="mini">ou</span><br /> \
         <span class="btn btn-default ">selecionar arquivos</span> \
		',
        init: function() {
            this.on("error", function(file, errorMessage) {
                boxAlert( '<strong>'+file.name+': </strong>'+errorMessage, '#msg_upload', 'danger');
            });
            
            this.on("success", function(file, responseText) {
                var jsonReturn = JSON.parse( responseText );
                
                if (!jsonReturn.status) {
                    this.removeFile(file);
                    boxAlert( '<strong>'+file.name+': </strong>' + trataErroCake( jsonReturn.error_input, false ), '#msg_upload', 'danger');
                }
            });
            
            this.on("complete", function(file) {
                this.removeFile(file);
                
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    listarFotos();
                }
            });
        }
	};   
    
    $(document).on('click', 'a.bt_delete_foto', function() {
        var obj_id = $(this).closest('div.preview-buttons').attr('id');        
    	confirmLink( function(){ excluir_foto( obj_id, <?=$Conteudo['Conteudo']['id'];?>  ); } );
    });
    $(document).on('click', 'a.bt_destaque', function() {
        var obj_id = $(this).closest('div.preview-buttons').attr('id');        
    	destacarFoto( obj_id, <?=$Conteudo['Conteudo']['id'];?> );
    });
		
});
<?php $this->Html->scriptEnd(); ?></script>