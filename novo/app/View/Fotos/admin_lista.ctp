<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-table"></i> <?PHP echo $title_for_layout; ?>
                </div>
			</div>
            
			<div class="panel-body">
            
                    <?PHP /** FORM */
                    echo $this->Form->create("Filter", array(
                                'inputDefaults' => array(
                                    'format' => array('label', 'input'),
                                    'div' => false,
                                    'label'=>false,
                                    'class' => 'form-control'
                                )
                    ));
                    ?>
                                
                    <?PHP echo $this->Session->flash(); ?>
                    
                    <?PHP echo $this->element('admin/header_listagem'); ?>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="heading">
                                    <th class="ta-c w-50"><?PHP echo __('Imagem'); ?></th>   
        	                		<th><?PHP echo $this->Paginator->sort('empresa_id', 'Empresa Id<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th><?PHP echo $this->Paginator->sort('legenda', 'Legenda<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th><?PHP echo $this->Paginator->sort('filename', 'Filename<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th class="ta-c w-50"><?PHP echo __('A��es'); ?></th>                                    
                                </tr>                    
                                <tr class="filter">       
                            		<th>&nbsp;</th>                     
                            		<th><?PHP echo $this->Form->input('empresa_id', array()); ?></th>
                            		<th><?PHP echo $this->Form->input('legenda', array()); ?></th>
                            		<th><?PHP echo $this->Form->input('filename', array()); ?></th>
                            		<th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                    
<?PHP foreach ($fotos as $foto): ?>
	<tr>
		<td style="text-align: center;">
            <?php
            $img = $this->GFunction->getImage( $foto['Foto']['arquivo'], 'foto/arquivo/' );
            echo $this->Timthumb->image( '/'.$img, array('width' => 50, 'height' => 50, 'zoom_crop'=> 2), array('class'=>'image'));
            ?>&nbsp;
        </td>
		<td><?php echo h($foto['Foto']['empresa_id']); ?>&nbsp;</td>
		<td><?php echo h($foto['Foto']['legenda']); ?>&nbsp;</td>
		<td><?php echo h($foto['Foto']['filename']); ?>&nbsp;</td>
		<td class="ta-c">
			<div class="btn-group bt_list_table">				<button type="button" class="btn btn-default btn-gradient dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-cogwheel"></span></button>				<ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">					<li><a href="<?PHP echo Router::url(array('action' => 'cadastro', $foto['Foto']['id'])); ?>"  title="Editar"><i class="coloredicon application-edit"></i> Editar</a></li>					<li><a href="<?PHP echo Router::url(array('action' => 'excluir', $foto['Foto']['id'])); ?>" class="confirmLink" title="Excluir"><i class="coloredicon application-delete"></i> Excluir</a></li>				</ul>			</div>		</td>	</tr>
<?PHP endforeach; ?>
                    
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8"><?PHP echo $this->element('admin/paginator'); ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <?PHP echo $this->Form->end(); ?>     
                    
			</div>
		</div>
	</div>
</div>