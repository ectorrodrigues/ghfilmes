<?PHP
echo $this->Html->script(
    array(
        'site/jquery.fancybox',
        'site/jquery-ui.min',
        'site/owl.carousel.min',
        'site/bootstrap.min.js',
        'jquery.maskedinput',
        'jquery.widgets',
        'functions',
        'site/config.js',
        )
);
?>
<script type="text/javascript">
$(document).ready(function(){
});
</script>

<?PHP echo $this->fetch('script'); ?>
