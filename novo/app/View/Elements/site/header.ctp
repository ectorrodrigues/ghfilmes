<header>
    <div class="container">
        <div class="row position-relative justify-content-center">
            <div class="logo col-12 text-center" data-aos="zoom-out" data-aos-delay="400"  data-aos-duration="1000">
                <a class="ml-auto mr-auto" href="<?=Router::url('/', true);?>"><img src="<?=$this->webroot;?>img/site/logo-top.svg" alt="Logo"></a>
            </div>

            <div class="menu col-11">
                <nav class="navbar navbar-expand-lg pt-0 ">
                    <div class="ml-auto text-right">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent"  data-aos="fade-right" data-aos-duration="1500">
                            <ul class="navbar-nav mr-auto">
                                <?= $this->element('site/links'); ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

    </div>
</header>
