<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!--Favicon-->
<link href="<?PHP echo $this->webroot . 'img/favicon.png'; ?>" type="image/x-icon" rel="icon" />
<link href="<?PHP echo $this->webroot . 'img/favicon.png'; ?>" type="image/x-icon" rel="shortcut icon" />

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">



<?php
	echo $this->fetch('css');
    echo $this->Html->css( array(
      'site/jquery-ui.min',
      'site/owl.carousel.min',
      'site/bootstrap.min.css',
      'site/jquery.fancybox.min',
      'site/styles.css',
			'site/aos.css',
    ) );
    echo $this->Html->script( array(
      'site/jquery.min.js',
    ) );
?>

<!-- AO JQuery Transition Effects - Animate On Scroll Library-->

<script type="text/javascript">
var URL_BASE = 'http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot; ?>';
</script>
<style type="text/css">#dialog_system, #dialog_loading, #dialog_system_confirm{display: none;}</style>
