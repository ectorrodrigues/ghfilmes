<?PHP if (count($BannersHome)>0) { ?>
<section class="banner owl-carrousel"  data-aos="flip-up" data-aos-delay="0" data-aos-duration="1000">
    <div class="owl-carousel" id="carousel_banner">
        <!-- <div class="item-video"><a class="owl-video" href="https://www.youtube.com/embed/-JjQalcnvkc"></a></div> -->

        <?PHP foreach($BannersHome as $cont_banner=>$Banner) { ?>

            <a href="https://<?=$Banner['Banner']['url'];?>&autoplay=1" data-fancybox="https://<?=$Banner['Banner']['url'];?>&autoplay=1">
            <div class="item">
              <p style="position:absolute; z-index:2000; left:50%; top:50%; font-size:2.5em;">
                  <i class="fas fa-play"></i>
              </p>
                    <img src="<?=$Banner['Banner']['_Foto']['img_path'];?>">
            </div>
          </a>
        <? } ?>


    </div>
    <div id="nav_banner" class="owl-nav d-flex justify-content-between align-items-center"></div>
</section>
<?PHP } unset($Banner, $cont_banner); ?>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){

    $('#carousel_banner').owlCarousel({
        // autoplay:true,
        // autoplayHoverPause:true,
        video:true,
        loop:true,
        margin: 0,
        items: 1,
        dots: false,
        nav:true,
        navContainer: '#nav_banner',
        onPlayVideo: function(event) {
            $this = $( event.target );
            var $current = $this.find(".owl-item.active");
            $current.find('.owl-video-play-icon').remove();
            $current.find('.owl-video-tn').remove();
        }
    });


});
<?php $this->Html->scriptEnd(); ?></script>
