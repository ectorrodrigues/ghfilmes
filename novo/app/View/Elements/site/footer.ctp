<footer>
  <div class="container">
    <div class="row">

      <div class="col-lg-5">
        <ul class="nav">
          <?= $this->element('site/links'); ?>
        </ul>
        <?= returnNotEmpty($configuracoes[14]['Configuracao']['configuracao'], '<div class="copy">[#var#]</div>') ;?>
      </div>

      <div class="col-lg-3">
        <video class="video_lhama" playsinline autoplay muted loop>
          <source src="<?= $this->webroot; ?>img/site/lhama_vt.mp4" type="video/mp4">
        </video>
      </div>

      <div class="col-lg-4">
        <div class="text-right">
          <ul class="list-inline social ml-auto">
            <?= returnNotEmpty($configuracoes[10]['Configuracao']['configuracao'], '<li class="list-inline-item"><a href="[#var#]" target="_blank"><i class="fab fa-vimeo-v"></i></a></li>') ;?>
            <?= returnNotEmpty($configuracoes[11]['Configuracao']['configuracao'], '<li class="list-inline-item"><a href="[#var#]" target="_blank"><i class="fab fa-youtube"></i></a></li>') ;?>
            <?= returnNotEmpty($configuracoes[12]['Configuracao']['configuracao'], '<li class="list-inline-item"><a href="[#var#]" target="_blank"><i class="fab fa-facebook-f"></i></a></li>') ;?>
            <?= returnNotEmpty($configuracoes[13]['Configuracao']['configuracao'], '<li class="list-inline-item"><a href="[#var#]" target="_blank"><i class="fab fa-instagram"></i></a></li>') ;?>
            <?= returnNotEmpty(preg_replace('/\D/', '', $configuracoes[5]['Configuracao']['configuracao']), '<li class="list-inline-item"><a href="https://wa.me/[#var#]" target="_blank"><i class="fab fa-whatsapp"></i></a></li>') ;?>
          </ul>

          <?= returnNotEmpty($configuracoes[9]['Configuracao']['configuracao'], '<p>'.nl2br($configuracoes[9]['Configuracao']['configuracao']) .'</p>') ;?>
        </div>
      </div>

    </div>
  </div>
</footer>
