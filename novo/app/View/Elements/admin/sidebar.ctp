<aside id="sidebar" class="hidden-print">
    <div id="sidebar-search">
    	<form><h3>Menu</h3></form>
    	<div class="sidebar-toggle tip" id="bt_toogle_menu" title="Ocultar/Exibir Menu" data-placement="left">
    		<i class="fa fa-bars"></i>
    	</div>
    </div>
    <div id="sidebar-menu">
    
    	<ul class="nav sidebar-nav">
			<li <?PHP echo (($this->params['controller']=='dashboard')?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'dashboard', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-home"></span><span class="sidebar-title">In�cio</span></a></li>
            <li <?PHP echo ((($this->params['controller']=='videos'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'videos', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-film"></span>V�deos</a></li>
            <li <?PHP echo ((($this->params['type']=='conteudos'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'conteudos', 'action' => 'index', 'type'=>'conteudos' )); ?>"><span class="glyphicons glyphicon-pencil"></span>P�ginas</a></li>
            <li <?PHP echo ((($this->params['type']=='blog'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'conteudos', 'action' => 'index', 'type'=>'blog' )); ?>"><span class="glyphicons glyphicon-pencil"></span>Blog</a></li>
            <li <?PHP echo ((($this->params['controller']=='parceiros'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'parceiros', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-parents"></span>Parceiros</a></li>
            <li>
				<a href="#nav-cadastros" class="accordion-toggle collapsed"><span class="glyphicons glyphicons-pencil"></span><span class="sidebar-title">Cadastros</span><span class="caret"></span></a>
				<ul class="nav sub-nav" id="nav-cadastros">
                    <li <?PHP echo ((($this->params['controller']=='usuarios'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'usuarios', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-user"></span>Usu�rios</a></li>
                    <li <?PHP echo ((($this->params['controller']=='banners'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'banners', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-picture"></span>Banners</a></li>
                    <li <?PHP echo (($this->params['controller']=='newsletteres')?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'newsletteres', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-notes_2"></span>Newsletter</a></li>
                </ul>
			</li>
    		<li>
				<a href="#nav-sistema" class="accordion-toggle collapsed"><span class="glyphicons glyphicons-cogwheel"></span><span class="sidebar-title">Sistema</span><span class="caret"></span></a>
				<ul class="nav sub-nav" id="nav-sistema">
                    <li <?PHP echo ((($this->params['controller']=='categoria_videos'))?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'categoria_videos', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-cogwheel"></span>Categorias</a></li>
                    <li <?PHP echo (($this->params['controller']=='configuracoes')?'class=active':''); ?>><a href="<?PHP echo Router::url(array('controller' => 'configuracoes', 'action' => 'index' )); ?>"><span class="glyphicons glyphicons-cogwheel"></span>Configura��es</a></li>
                </ul>
			</li>
            <li id="menu_voltar_site"><a href="<?PHP echo Router::url('/'); ?>"><span class="glyphicons glyphicons-left_arrow"></span><span class="sidebar-title">Voltar para o site</span></a></li>
            <li><a href="<?PHP echo Router::url(array('controller' => 'usuarios', 'action' => 'logout' )); ?>/"><span class="glyphicons glyphicons-power"></span><span class="sidebar-title">Sair</span></a></li>
    	</ul>
    </div>
</aside>