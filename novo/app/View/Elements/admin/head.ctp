<base href="<?PHP echo Router::url('/', true); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Font CSS  -->
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />


<link href="<?PHP echo $this->webroot . 'img/favicon.png'; ?>" type="image/x-icon" rel="icon" />
<link href="<?PHP echo $this->webroot . 'img/favicon.png'; ?>" type="image/x-icon" rel="shortcut icon" />

<?php
echo $this->fetch('meta');
echo $this->Html->css( array(
	'font-awesome.min.css',
	'glyphicons.min.css',
    'icomoon.min.css',
	'bootstrap.min.css',
    'jquery.datetimepicker.css',
    'social-coloredicons-buttons.css',
    
    'select2.min.css',
    'jquery.fancybox.css',
	'admin/social-jquery-ui-1.10.0.custom.css',
	'admin/theme.css',
	'admin/pages.css',
	'admin/plugins.css',
	'admin/responsive.css',
	'admin/style.css',
) );

echo $this->fetch('css');
echo $this->Html->script( array(  'jquery.min.js' ) );
?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<noscript><meta http-equiv="refresh" content="0; url=http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot . 'pages/javascript/'; ?>" /></noscript>
<script type="text/javascript">
var URL_BASE = '<?PHP echo Router::url('/', true); ?>';
var URL_WEBROOT = 'http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot; ?>';
var URL_LOGO = 'http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot . $this->Timthumb->imageUrl( '/img/logo_gerall.png', array('width' => 150, 'zoom_crop'=> 2)); ?>';
</script>
<style type="text/css">#dialog_system, #dialog_loading, #dialog_system_confirm{display: none;} .wraper #main{margin-top: 40px; }</style>