
<div class="hide">
    <div id="dialog_conteudo" style="display: block; overflow: auto;"></div>
</div>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
function dialogConteudo(id) {
    var obj_registro = objArray('conteudo', parseInt(id) );    
    if (obj_registro) {
        var str_title = obj_registro.titulo;
        str_title = ((str_title!='')?str_title:'Conte�do');
        
        $("#dialog_conteudo").html( obj_registro.conteudo );
        $("#dialog_conteudo").dialog({
        	autoOpen: false,
        	title: str_title,
        	bgiframe: true,
        	modal: true,
        	resizable: true,
        	draggable: true,
            width:700,
            height:450,
        	buttons: {
                Fechar: function() { $(this).dialog('close'); }
        	}
        });
        $("#dialog_conteudo").dialog('open');
    }
}
<?php $this->Html->scriptEnd(); ?></script>