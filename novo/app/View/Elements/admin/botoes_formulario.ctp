<?PHP
echo $this->Form->input('after_action', array('name'=>'after_action', 'type'=>'hidden'));                

echo $this->Html->link( '<i class="fa fa-arrow-circle-left"></i> Salvar e Voltar', 'javascript:;',
            array('title'=>'Salvar e voltar para listagem', 'id' => 'back', 'class'=>'btn btn-info btn-gradient bt_salvar tip', 'escape' => false)
);
echo $this->Html->link( '<i class="fa fa-edit"></i> Salvar e Editar', 'javascript:;',
            array('title'=>'Salvar e continuar editando', 'id' => 'edit', 'class'=>'btn btn-info btn-gradient bt_salvar tip', 'escape' => false)
);
echo $this->Html->link( '<i class="fa fa-check"></i> Salvar', 'javascript:;',
            array('title'=>'Salvar e come�ar um novo cadastro', 'id' => 'save', 'class'=>'btn btn-info btn-gradient bt_salvar tip', 'escape' => false)
);

$arr_options = array(
    'title'=>'Voltar para listagem',
    'class'=>'btn btn-gradient btn-default bt_voltar tip',
    'escape' => false
);
if ($base_url) { $arr_options['location'] = Router::url(array_merge($base_url, array('action'=>'index'))); }

echo $this->Html->link( '<i class="fa fa-arrow-circle-left"></i> Voltar', 'javascript:;', $arr_options );
echo $this->Html->link( '<i class="fa fa-exchange"></i> Reiniciar', 'javascript:;',
            array('title'=>'Reiniciar formul�rio', 'id' => 'bt_atualizar', 'class'=>'btn btn-gradient btn-default tip', 'escape' => false)
);
echo $this->Html->link( '<i class="fa fa-file"></i> Novo', array('action' => 'cadastro'),
            array('title'=>'Iniciar novo cadastro', 'class'=>'btn btn-gradient btn-default confirmLink tip', 'escape' => false)
);
?>