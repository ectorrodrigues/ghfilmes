<div class="pagination col-sm-12">                                            
    <div class="pull-left">
        <span><?PHP echo $this->Paginator->counter(array('format' => 'Exibindo {:start} at� {:end} do total de {:count}')); ?></span>
    </div>
    <div class="pull-right">
        <ul>
            <?PHP                                                
            echo $this->Paginator->first( '&laquo;', array('tag' => 'li', 'class'=> 'first', 'escape'=>false), null, array('class' => 'disabled first', 'tag' => 'li', 'escape'=>false));
            echo $this->Paginator->prev( '&lsaquo;', array('tag' => 'li', 'escape'=>false), null, array('class' => 'disabled', 'tag' => 'li', 'escape'=>false));
            
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active','tag' => 'li'));
            
            echo $this->Paginator->next( '&rsaquo;', array('tag' => 'li', 'escape'=>false), null, array('class' => 'disabled','tag' => 'li', 'escape'=>false));
            echo $this->Paginator->last( '&raquo;', array('tag' => 'li', 'class'=>'last', 'escape'=>false), null, array('class' => 'disabled last','tag' => 'li', 'escape'=>false));
            ?>
        </ul>    
    </div>
</div>