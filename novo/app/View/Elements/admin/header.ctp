<header class="navbar navbar-fixed-top">
    <div class="pull-left">
    	<a class="navbar-brand" href="<?PHP echo Router::url(array('admin'=>true, 'controller' => 'dashboard', 'action' => 'index')); ?>">
        	<div class="navbar-logo">
				<?PHP echo $this->Timthumb->image( '/img/logo-invertida.png', array('height' => 30, 'zoom_crop'=> 2), array('alt'=>'Gerall', 'class'=>'img-fluid')); ?>
        	</div>
    	</a>        
    </div>
    
    <div class="pull-right header-btns" id="step2">   
    
    	<div class="btn-group user-menu">
        
    		<button type="button" class="btn btn-default btn-gradient btn-sm dropdown-toggle tip" data-toggle="dropdown" data-placement="bottom" title="Editar perfil"><b> <?PHP echo $userLogado['Usuario']['nome']; ?></b></button>
    		<button type="button" class="btn btn-default btn-gradient btn-sm dropdown-toggle padding-none tip" data-toggle="dropdown" data-placement="bottom" title="Editar perfil"><?php echo $this->Timthumb->image($userLogado['Usuario']['_Foto']['img'], array('width' => 28, 'height' => 28, 'zoom_crop'=>2), array('class'=>'user avatar')); ?></button>
            
    		<ul class="dropdown-menu checkbox-persist" role="menu">
    			<li class="menu-arrow"><div class="menu-arrow-up"></div></li>
    			<li class="dropdown-header">Sua conta <span class="pull-right glyphicons glyphicons-user"></span></li>
    			<li>
        			<ul class="dropdown-items">                        
        				<li class="border-bottom-none">
            				<div class="item-icon"><i class="fa fa-cog"></i></div>
                            <a class="item-message"  href="<?PHP echo Router::url(array('controller' => 'usuarios', 'action' => 'perfil' )); ?>">Editar Perfil</a>
                        </li>
                        <li>
                            <div class="item-icon"><i class="fa fa-sign-out"></i></div>
                            <a href="<?PHP echo Router::url(array('controller' => 'usuarios', 'action' => 'logout' )); ?>">Sair</a>
                        </li>
        			</ul>
    			</li>
    		</ul>
    	</div>
    </div>
</header>