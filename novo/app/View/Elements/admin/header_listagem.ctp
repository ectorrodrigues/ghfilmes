<div class="row margin-bottom header_filtro">
    <div class="col-xs-3">
        <div class="btn-group">
            <?PHP
            $bt_novo = ( (!isset($bt_novo)) ? true : $bt_novo);
            if ($bt_novo) {
                echo $this->Html->link( '<i class="glyphicons glyphicons-circle_plus"></i> Novo', array('action' => 'cadastro'), array('title'=>'Novo', 'class'=>'btn btn-primary btn-gradient mr10', 'escape' => false));
            }
            unset($bt_novo);
            ?>
        </div>
    </div>

    <div class="col-xs-9">
        <input type="submit" value="Filtrar" class="btn btn-green2 btn-gradient right mr10" />
        <?PHP echo $this->Html->link( 'Limpar Filtro', array('limpar_filtro'=>true), array('class'=>'btn btn-green2 btn-gradient right') ); ?>
    </div>
</div>