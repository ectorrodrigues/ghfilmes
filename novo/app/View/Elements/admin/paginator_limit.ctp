<?PHP
$arr_paginator = $this->Paginator->params();                    
echo $this->Form->input('limit', array(
    'options' => array(5=>5, 10=>10, 25=>25, 50=>50, 100=>100),
    'class' => 'select_limit form-control',
    'selected' => $arr_paginator['limit']
));
?>