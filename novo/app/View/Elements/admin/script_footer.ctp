<?PHP
echo $this->Html->script(
    array(  'jquery-ui.min.js',
            'bootstrap.min.js',
            'uniform.min.js',
            'jquery.datetimepicker',
            'admin/main.js',
            'jquery.fancybox.pack',
            'jquery.maskedinput',
            'jquery.ui.datepicker-pt-BR',
            'vendor/plugins/chosen/chosen.jquery.min.js',
            'vendor/jquery.price_format.2.0.min.js',
            'jquery.widgets',  
//            'config.js',
            'functions.js',     
            'admin/config.js',
    )
);
?>

<?PHP echo $this->fetch('script'); ?>

<script type="text/javascript">
var bootstrapButton = $.fn.button.noConflict();
$.fn.bootstrapBtn = bootstrapButton;

$(document).ready(function(){
	Core.init();
    
	var bodyAnimate = setTimeout(function() { $('#page-loader').css("display", "none"); },1000);
});
</script>