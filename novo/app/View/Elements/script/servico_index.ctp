<script src="http://www.google.com/jsapi?autoload={'modules':[{name:'maps',version:3,other_params:'sensor=false'}]}" type="text/javascript"></script>
<?PHP echo $this->Html->script( array(  'admin/infobox.js', 'admin/functions-maps.js' ), array('inline' => false) );	?>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
function initialize(lat, lng) {
    var location_init = new google.maps.LatLng(lat, lng); // Toledo
    
    var noPoi = [{
        featureType: "poi",
        stylers: [
          { visibility: "off" }
        ]   
      }
    ];
    var mapOptions = {
        zoom: 19,
        center: location_init,
        zoomControl: true,
        panControl: false,
        scaleControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        styles: noPoi
    };
    
    map = new google.maps.Map(document.getElementById('map'), mapOptions);    
    
    marker = new google.maps.Marker({
        draggable: false,
        map: map,
        position: location_init,
    });
    marker.setMap(map);
    
}
<?php $this->Html->scriptEnd(); ?></script>