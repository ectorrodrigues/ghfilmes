<p>Formulário de contato enviado pelo site em <?=date("d/m/Y H:i");?></p>

<p>
    <strong>Assunto:</strong> <?=$data['Contato']['assunto'];?><br />
    <strong>Nome:</strong> <?=$data['Contato']['nome'];?><br />
    <strong>E-mail:</strong> <?=$data['Contato']['email'];?><br />
</p>
<p>
    <strong>Mensagem:</strong><br />
    <blockquote><?=nl2br( $data['Contato']['mensagem'] );?></blockquote>
</p>

<p>IP enviado: <?=$_SERVER['REMOTE_ADDR'];?></p>