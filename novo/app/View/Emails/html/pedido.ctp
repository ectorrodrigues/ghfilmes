<p>Ol� <?=$Pedido['Usuario']['nome'];?>!</p>

<p>Segue informa��es sobre seu pedido:</p>

<h1>Or�amento #<?=$Pedido['Pedido']['id'];?></h1>

<table style="width:100% !important;">
    <tr>
        <td style="width:50%;">
            <ul style="list-style:none;">
                <li>Status: <b style="color:#2B61D9;"><?=$Pedido['State']['titulo'];?></b> </li>
                <li>Criado em <?=data($Pedido['Pedido']['_created']);?></li>
                <?= returnNotEmpty($Pedido['Pedido']['data_pagamento'], '<li>Pagamento em '. data($Pedido['Pedido']['data_pagamento']).'</li>' );?>
            </ul>
        </td>
        <td style="width:50%;">
            <ul style="list-style:none;">
                <li><strong><?=$Pedido['Usuario']['nome'];?></strong></li>
                <li> <?=$Pedido['Usuario']['endereco'];?></li>
                <li><?=$Pedido['Usuario']['email'];?></span></li>
                <li><?=$Pedido['Usuario']['telefone'];?></span></li>
            </ul>
        </td>
    </tr>
</table>

<hr/>

<table style="width:100%;">
    <tr>
        <td style="text-align:center;"><strong>#</strong></td>
        <td style="text-align:left;"></td>
        <td style="text-align:left;"><strong>Produto</strong></td>
        <td style="text-align:center;"><strong>Qtde</strong></td>
        <td style="text-align:center;"><strong>Valor</strong></td>
        <td style="text-align:right;"><strong>Total</strong></td>
    </tr>
    <?PHP
    $subtotal = 0;
    foreach($Pedido['PedidoProduto'] as $keyProdOrc=> $PedidoProduto) {
        $subtotal += ($PedidoProduto['valor']*$PedidoProduto['qtde']);
        $_url_produto = Router::url( array('admin'=>false, 'controller'=>'produtos', 'action'=>'comprar', $PedidoProduto['Produto']['id'], slugURL($PedidoProduto['Produto']['titulo']) ), true );
        ?>

        <tr>
            <td style="text-align:center;"><?=($keyProdOrc+1);?></td>
            <td style="text-align:left;"><a href="<?=$_url_produto;?>" target="_blank"><?=$PedidoProduto['Produto']['_Foto']['img_full'];?></a></td>
            <td style="text-align:left;"><a href="<?=$_url_produto;?>" target="_blank"><?=$PedidoProduto['Produto']['titulo'];?></a></td>
            <td style="text-align:center;"><?=$PedidoProduto['qtde'];?></td>
            <td style="text-align:center;"><?=(($PedidoProduto['valor']!=0) ? converteDoubleMoeda($PedidoProduto['valor']) : 'consultar');?></td>
            <td style="text-align:right;"><?=(($PedidoProduto['valor']!=0) ? converteDoubleMoeda($PedidoProduto['valor']*$PedidoProduto['qtde']) : 'consultar');?></td>
        </tr>                                            
    <?PHP } ?>
</table>

<table style="width:100%;">
    <tr>
        <td style="width:50%;">
            <p><strong>Observa��o:</strong> <?=$Pedido['Pedido']['observacao'];?></p>
        </td>
        <td style="width:50%;">
            <ul style="list-style:none; float:right;">
                <li>Sub-Total: <strong><?=(($subtotal!=0) ? converteDoubleMoeda($subtotal) : 'consultar');?></strong></li>
                <li>Frete: <strong><?= (($Pedido['Pedido']['valor_frete']!=0) ? converteDoubleMoeda($Pedido['Pedido']['valor_frete']) : 'consultar');?></strong></li>
                <li>Total: <strong><?=(($subtotal+$Pedido['Pedido']['valor_frete']!=0) ? converteDoubleMoeda($subtotal+$Pedido['Pedido']['valor_frete']) : 'consultar');?></strong></li>
            </ul>
        </td>
    </tr>
</table>