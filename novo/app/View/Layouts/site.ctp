<?PHP
$title_page = $configuracoes[1]['Configuracao']['configuracao'];
$keywords = ((isset($keywords)&&($keywords!='')) ? $keywords : $configuracoes[3]['Configuracao']['configuracao']);
$description = ((isset($description)&&($description!='')) ? $description : $configuracoes[4]['Configuracao']['configuracao']);
?><!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_page . returnNotEmpty($title_for_layout, ' - [#var#]'); ?></title>

    <meta name="author" content="Possamai.net.br" />
    <meta name="description" content="<?php echo html_entity_decode($description, ENT_COMPAT, 'ISO-8859-1'); ?>" />
    <meta name="keywords" content="<?php echo html_entity_decode($keywords, ENT_COMPAT, 'ISO-8859-1'); ?>" />

	<?PHP echo $this->element('site/head'); ?>
    <noscript><meta http-equiv="refresh" content="0; url=http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot . 'pages/javascript/'; ?>" /></noscript>
</head>

<body>

    <div id="dialog_system"></div>
    <div id="dialog_system_confirm"></div>
    <div id="dialog_loading"><img src="<?PHP echo $this->webroot; ?>img/loading.gif" alt="loading" style="border: 0px;" /> Aguarde...</div>

    <?PHP echo $this->element('site/header'); ?>

    <?php echo $this->fetch('content'); ?>

    <?php echo $this->element('site/footer'); ?>

    <? // returnNotEmpty(preg_replace('/\D/', '', $configuracoes[6]['Configuracao']['configuracao']), '<div class="button_fixed"><a href="https://wa.me/[#var#]" target="_blank"><i class="fab fa-whatsapp-square"></i></a></div>') ;?>

<?php echo $this->element('site/script_footer'); ?>

<?php echo $this->Html->script( array('site/aos.js',) ); ?>
<script> AOS.init(); </script>

<script>
$(document).ready(function(){
    if(window.matchMedia("(max-width: 980px)").matches){
        // The viewport is less than 768 pixels wide
        $('[data-aos]').attr('data-aos-offset', '150');
				$('[data-aos]').attr('data-aos-duration', '800');
				$('[data-aos]').attr('data-aos-delay', '0');
    }
});
</script>

</body>
</html>
