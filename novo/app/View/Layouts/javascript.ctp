<?PHP
$title_page = 'Gerall';
?><!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_page ?> - <?php echo $title_for_layout; ?></title>
    
	<?php echo $this->element('site/head'); ?>
    <script>window.location.replace('<?PHP echo Router::url('/', true); ?>');</script>
</head>

<body class="breadcrumb-white footer-top-dark">

<div id="page">
    
    <?php echo $this->element('site/header'); ?>

    <div id="main-wrapper">
        <div id="main">
            <div id="main-inner">            
                
                <div class="container">    
                    <div class="block-content">
                        <div class="block-content-inner">
                            <div class="page-header center">
                				<h1>Aten��o</h1>
                			</div>
            
                            <div class="alert alert-warning">
                                <p>Para completa funcionalidade deste site � necess�rio habilitar o JavaScript.</p>
                                <p>Aqui est�o as <a href="http://www.enable-javascript.com/pt/" target="_blank">instru��es de como habilitar no seu navegador</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div><!-- /#main-inner -->
        </div><!-- /#main -->
    </div><!-- /#main-wrapper -->
    
    <div id="footer-wrapper">
        <div id="footer">
            <div id="footer-inner">
            
                <?php echo $this->element('site/footer'); ?>
                
            </div><!-- /#footer-inner -->
        </div><!-- /#footer -->
    </div><!-- /#footer-wrapper -->
</div><!-- /#page -->

</body>
</html>