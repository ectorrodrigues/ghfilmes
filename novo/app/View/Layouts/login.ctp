<?PHP
$title_page = 'Gerall';
?><!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
	<title><?php echo $title_page ?> - <?php echo $title_for_layout; ?></title>
	
	<?PHP echo $this->element('admin/head'); ?>
</head>    
<body class="login-page">    
    <div id="dialog_system"></div>
    <div id="dialog_system_confirm"></div>
    <div id="dialog_loading"><img src="<?PHP echo $this->webroot; ?>img/loading.gif" alt="loading" style="border: 0px;" /> Aguarde...</div>
    
    <a href="<?PHP echo $this->base; ?>" id="return-arrow"> <i class="fa fa-arrow-circle-left fa-3x text-blue2"></i> <span> Voltar <br />ao Site </span> </a>   

	<div id="main">
        <div class="container">
        	<div class="row">
        		<div id="page-logo">
					<a href="<?PHP Router::url(array(), true); ?>" style="text-align: center;display: block;">
                        <img src="<?PHP echo $this->webroot; ?>img/logo-invertida.png" alt="Logo" style="border: 0px; max-height:80px;" />
                    </a>
        		</div>
        	</div>
        	<div class="row">    
                <?php echo $this->fetch('content'); ?>
        	</div>
            
        </div>
    </div>
    
    <?php // echo $this->element('admin/sql_dump'); ?>
    <?php echo $this->element('admin/script_footer'); ?>
  
</body>
</html>