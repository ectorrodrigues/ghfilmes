<?PHP
$url_base = Router::url('/', true);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<style>@import url(http://fonts.googleapis.com/css?family=Droid+Sans);</style>

<body style="color: #003744; font-family: 'Droid Sans', sans-serif; font-size:13px;">
    <table width="700px" style="padding:30px;" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" style="background-color: #1F1F1F; padding: 10px 0px">
                
                <table style="padding:10px 0px; width: 100%;" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td style="text-align: left;"><a href="<?PHP echo $url_base; ?>"><img src="<?PHP echo $url_base .'/img/logo.png'; ?>" alt="Logo" /></a></td>
                    	<td style="text-align: right;">GH Filmes</td>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr>
            <td style="padding: 10px 0px 50px 0px;">
                <?php echo $this->fetch('content'); ?>
            </td>
        </tr>
        <tr>
            <td align="center" style="border-top: 5px solid #232b19; padding: 10px 0px;">
                
                <h2 style="color:#8cc642; font-size:2em;">Confira agora mesmo!</h2>
                
            </td>
        </tr>
    </table>
</body>
</html>