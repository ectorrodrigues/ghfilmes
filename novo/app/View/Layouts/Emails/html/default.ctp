<?PHP
$url_base = Router::url('/', true);
$url_base = 'http://www.ghfilmes.com/';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<style>@import url(http://fonts.googleapis.com/css?family=Droid+Sans);</style>

<body style="color: #000; font-family: 'Droid Sans', sans-serif; font-size:13px;">
    <table width="700px" style="padding:30px;" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" style="background-color: #FFF; padding: 10px 0px; border-top: 20px solid #000; border-bottom: 20px solid #000;">

                <table style="padding:10px; width: 100%;" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td style="text-align: center;"><a href="<?PHP echo $url_base; ?>"><img src="<?PHP echo $url_base .'img/logo-email.png'; ?>" alt="Logo" /></a></td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td style="padding: 30px 0px; color:#000;">
                <?php echo $this->fetch('content'); ?>
            </td>
        </tr>
        <tr>
            <td align="center" style="background-color: #000; padding: 10px 0px; border-bottom: 20px solid #000;">

                <h4><a href="<?PHP echo $url_base; ?>" style="color:#FFF; font-size:2em;"><?PHP echo $url_base; ?></a></h4>

            </td>
        </tr>
    </table>
</body>
</html>
