<!doctype html>
<html>
<head>
<title>Em desenvolvimento!</title>
<meta charset="utf-8"/>
<meta name="robots" content="noindex"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; color: #333;  }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>
</head>
<body>
<article>
    <h1>Em breve!</h1>
    <div>
        <p>Estamos a todo vapor por aqui, para concluir o desenvolvimento do site!</p>
        <p>Aguardem...</p>
    </div>
</article>
</body>
</html>