<?PHP
$title_page = 'Gerall';
?><!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_page ?> - <?php echo $title_for_layout; ?></title>
	
	<?PHP echo $this->element('admin/head'); ?>
</head>
<body <?PHP echo ((isset($bodyClass)) ? 'class="'.$bodyClass.'"' : '');?>>
    <div id="dialog_system"></div>
    <div id="dialog_system_confirm"></div>
    <div id="dialog_loading"><img src="<?PHP echo $this->webroot; ?>img/loading.gif" alt="loading" style="border: 0px;" /> Aguarde...</div>  

    <?PHP echo $this->element('admin/header'); ?>

	<div id="main">
		<?PHP echo $this->element('admin/sidebar'); ?>		
		<section id="content">		
            <?PHP echo $this->element('admin/breadcrumb'); ?>			
            <div class="container">
				<?php echo $this->fetch('content'); ?>       
				<?php echo $this->element('admin/sql_dump'); ?>     
            </div>
        </section>
    </div>
    
    <?php echo $this->element('admin/script_footer'); ?>
   
   
</body>
</html>