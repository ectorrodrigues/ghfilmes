<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Ops!</title>

	<?PHP echo $this->element('site/head'); ?>
    <noscript><meta http-equiv="refresh" content="0; url=http://<?PHP echo $_SERVER['HTTP_HOST'] . $this->webroot . 'pages/javascript/'; ?>" /></noscript>
</head>

<body class="cnt-home">
    <div id="dialog_system"></div>
    <div id="dialog_system_confirm"></div>
    <div id="dialog_loading"><img src="<?PHP echo $this->webroot; ?>img/loading.gif" alt="loading" style="border: 0px;" /> Aguarde...</div>

    <div class="wrapper">

        <?PHP echo $this->element('site/header'); ?>
        <section class="lista_videos">

            <div class="container border-top">

                <div class="row justify-content-around m-h">
                    <div class="col-12 p-20">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>

        </section>

        <?php echo $this->element('site/footer'); ?>

    </div>


<?php echo $this->element('site/script_footer'); ?>

</body>
</html>
