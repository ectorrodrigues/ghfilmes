<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo __( $title_for_layout ); ?>
                </div>
			</div>            
			<div class="panel-body">

                <?PHP echo $this->Session->flash(); ?>                                
                <?PHP
                $Objeto = $this->request->data;

                echo $this->Form->create('Banner', array(
                    'data-model' => 'Banner',
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class="col-lg-10">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                
                echo $this->Form->input('id');
                echo $this->Form->input('tamanho_id', array('empty'=>'Selecione'));
                echo $this->Form->input('titulo');
                echo $this->Form->input('url', array('class'=>'url form-control', 'placeholder'=>'www.', 'between' => '<div class="col-lg-10"><div class="input-group"><span class="input-group-addon">https:// </span>', 'after' => '</div></div>'));
                echo $this->Form->input('data_inicio', array('class'=>'datepicker form-control','type'=>'text'));
                echo $this->Form->input('data_fim', array('class'=>'datepicker form-control','type'=>'text'));
                
                echo $this->Form->input('foto', array( 'type' => 'file'));                  
                if ( (isset($Objeto['Banner'])) && (isset($Objeto['Banner']['_Foto'])) ) {              
                //if ($Objeto['foto'] && (!empty($Objeto['id']))) {
                    $img = $this->GFunction->getImage( $Objeto['Banner']['foto'], 'banner/foto/');
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $this->webroot.$img; ?>">
                                <?PHP echo $this->Timthumb->image('/'.$img, array('width' => 80, 'height' => 80, 'zoom_crop'=> 1), array('class'=>'thumb')); ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['Banner']['foto']; ?></code></p>
                        </div>
                    </div>
                </div>
                <?PHP }
                
                echo $this->element('admin/input_historico_cadastro');
                ?>                
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?PHP echo $this->element('admin/botoes_formulario'); ?>
                    </div>
                </div>
                
                <?PHP echo $this->Form->end(); ?>                    
			</div>
		</div>
	</div>
</div>