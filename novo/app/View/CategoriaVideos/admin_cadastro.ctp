<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo __( $title_for_layout ); ?>
                </div>
			</div>            
			<div class="panel-body">

                <?PHP echo $this->Session->flash(); ?>                                
                <?PHP
                $Objeto = $this->request->data;

                echo $this->Form->create('CategoriaVideo', array(
                    'data-model' => 'CategoriaVideo',
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class="col-lg-10">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                
                echo $this->Form->input('id');
                echo $this->Form->input('titulo');
                ?>                

                <?PHP /*
                echo $this->Form->input('foto', array( 'type' => 'file'));
                if ( (isset($Objeto['CategoriaVideo'])) && (!empty($Objeto['CategoriaVideo']['foto'])) ) {
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $Objeto['CategoriaVideo']['_Foto']['img_path']; ?>">
                                <?PHP echo $Objeto['CategoriaVideo']['_Foto']['img_full']; ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['CategoriaVideo']['foto']; ?></code></p>
                        </div>
                    </div>
                </div>
                <?PHP } */ ?>
                
                <?PHP
                echo $this->Form->input('icone', array( 'type' => 'file'));
                if ( (isset($Objeto['CategoriaVideo'])) && (!empty($Objeto['CategoriaVideo']['icone'])) ) {
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $Objeto['CategoriaVideo']['_Icone']['img_path']; ?>">
                                <?PHP echo $Objeto['CategoriaVideo']['_Icone']['img_full']; ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['CategoriaVideo']['icone']; ?></code></p>
                        </div>
                    </div>
                </div>
                <?PHP } ?>    
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?PHP echo $this->element('admin/botoes_formulario'); ?>
                    </div>
                </div>
                
                <?PHP echo $this->Form->end(); ?>                    
			</div>
		</div>
	</div>
</div>