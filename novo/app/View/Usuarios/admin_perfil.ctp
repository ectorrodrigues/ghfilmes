<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo __( $title_for_layout ); ?>
                </div>
			</div>
            
			<div class="panel-body">

                <?PHP echo $this->Session->flash(); ?>                                
                <?PHP                
                echo $this->Form->create('Usuario', array(
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'data-model' => 'NivelSistema',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class="col-lg-10">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                
                $Objeto = $this->request->data['Usuario'];
                                
                echo $this->Form->input('nome');
                echo $this->Form->input('email', array('class'=>'email form-control', 'autocomplete'=>'off'));
                echo $this->Form->input('password', array('value' => '', 'autocomplete'=>'off'));
                echo $this->Form->input('telefone', array('class'=>'telefone form-control'));                
                echo $this->Form->input('telefone_2', array('class'=>'telefone form-control'));                
                echo $this->Form->input('newsletter', array('class'=> 'checkbox') );
                echo $this->Form->input('endereco');
                
                echo $this->Form->input('foto', array( 'type' => 'file'));                
                if ($Objeto['foto'] && (!empty($Objeto['id']))) {
                    $img = $this->GFunction->getImage( $Objeto['foto'], 'usuario/foto/');
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $this->webroot.$img; ?>">
                                <?PHP echo $this->Timthumb->image('/'.$img, array('width' => 80, 'height' => 80, 'zoom_crop'=> 1), array('class'=>'thumb')); ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['foto']; ?></code></p>
                            <?php echo $this->Html->link( '<i class="coloredicon application-delete"></i> Remover', array('action' => 'remover_avatar'), array('title'=>'Excluir', 'class'=>'btn btn-default btn-gradient confirmLink', 'escape' => false)); ?>
                        </div>
                    </div>
                </div>
                <?PHP }
                
                echo $this->element('admin/input_historico_cadastro');
                ?>
                                
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?PHP     
                        echo $this->Html->link( '<i class="fa fa-edit"></i> Salvar', 'javascript:;',
                                    array('class'=>'btn btn-info btn-gradient bt_salvar', 'escape' => false)
                        );
                        ?>
                    </div>
                </div>
                
                <?PHP echo $this->Form->end(); ?>                    
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    $('input[name="data[Usuario][email]"]').change(function(){
        $('input[name="data[Usuario][email_confirmado]"]').attr('checked', false);
        $.uniform.update();
    });
});
<?php $this->Html->scriptEnd(); ?></script>