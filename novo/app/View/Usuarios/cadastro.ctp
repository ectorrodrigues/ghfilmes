<div class="container">
    <div class="row justify-content-left">
        <div class="breadcrumb">
                <ul class="page-breadcrumb list-unstyled list-inline">
                    <li class="list-inline-item"><a href="<?PHP echo Router::url('/', true); ?>"><i class="fa fa-home"></i> In�cio</a> <i class="fa fa-angle-double-right"></i></li>
                    <li class="list-inline-item"><span>Acesse sua conta</span> </li>
                </ul>
        </div>
    </div>

    <div class="row justify-content-around py-5">

        <div class="col-12"><?PHP echo $this->Session->flash(); ?></div>

        <div class="col-6 mb-30">
            <h2><i class="fa fa-angle-right"></i> Entrar no sistema</h2>
            
            <?PHP                                            
            echo $this->Form->create('UsuarioLogin', array(
                    'autocomplete'=>'off',
                    'inputDefaults' => array(
                        'format' => array('label', 'input', 'error'),
                        'class'=>'form-control', 'required'=>true,
                        'div' => 'form-group ',
                        'error' => array('attributes' => array('wrap' => 'label',  'class' => 'help-inline error'))
                    )
            ) );
            
            echo $this->Form->input('action', array('type'=>'hidden', 'name'=>'action', 'value'=>'login'));
            
            echo $this->Form->input('email', array('class'=>'email form-control', 'label'=>'E-mail'));
            echo $this->Form->input('password', array('value' => '', 'autocomplete'=>'off', 'required'=>true, 'div'=>array('class'=>'form-group required')));
            
            echo $this->Form->submit('Acessar', array('div'=>false, 'class'=>'btn btn-blue btn-block'));                                            
            echo $this->Form->end();
            ?>
        </div>

        <div class="col-12 py-3 text-center mb-30">
            <h2><i class="fa fa-angle-right"></i> Fa�a seu cadastro</h2>

            <a href="javascript:;" class="btn" id="btn_cadastro">N�o tem cadastro? Cadastre-se agora mesmo</a>
        </div>
        
        <div class="col-12" id="form_cadastro">
            <?PHP                                            
            echo $this->Form->create('Usuario', array( 
                    'autocomplete'=>'off',  
                    'inputDefaults' => array(
                        'format' => array('label', 'input', 'error'),
                        'class'=>'form-control', 'div'=>'form-group',
                        'error' => array('attributes' => array('wrap' => 'label',  'class' => 'help-inline error'))
                    )
            ) );

            $data = $this->request->data;
            
            echo $this->Form->input('action', array('type'=>'hidden', 'name'=>'action', 'value'=>'cadastro'));
            ?>
            <div class="row">
                <div class="col-6">
                    <h4 class="mb-20">Dados Pessoais:</h4>
                    <?PHP
                    echo $this->Form->input('nome', array( 'required'=>'required'));
                    echo $this->Form->input('email', array('class'=>'email form-control', 'required'=>'required'));
                    echo $this->Form->input('password', array( 'required'=>'required'));
                    ?>

                    <div class="form-group mt-60">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tipo_pessoa" id="bt_pf" value="pf" <?= ((!isset($data['tipo_pessoa']) || ($data['tipo_pessoa']=='pf')) ? 'checked="checked"' : ''); ?>>
                            <label class="form-check-label" for="bt_pf">Pessoa F�sica</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tipo_pessoa" id="bt_pj" value="pj" <?= ((isset($data['tipo_pessoa']) && ($data['tipo_pessoa']=='pj')) ? 'checked="checked"' : ''); ?>>
                            <label class="form-check-label" for="bt_pj">Pessoa Jur�dica</label>
                        </div>
                    </div>

                    <div id="div_pf">
                        <?PHP
                        echo $this->Form->input('cpf', array('class'=>'cpf form-control'));
                        ?>
                    </div>
                    <div id="div_pj" style="display:none;">
                        <?PHP
                        echo $this->Form->input('cnpj', array('class'=>'cnpj form-control'));
                        echo $this->Form->input('razao_social', array( 'label'=>'Raz�o Social'));
                        // echo $this->Form->input('contato', array( 'label'=>'Falar com'));
                        ?>
                    </div>                    
                    
                    <?PHP
                    echo $this->Form->input('telefone', array('class'=>'telefone form-control', 'required'=>'required'));
                    echo $this->Form->input('telefone_2', array('class'=>'telefone form-control', 'label'=>'Celular'));
                    ?>
                </div>
                <div class="col-6">
                    <h4 class="mb-20">Endere�o:</h4>
                    <?PHP
                    echo $this->Form->input('cep', array('class'=>'cep form-control', 'required'=>'required'));
                    echo $this->Form->input('endereco', array( 'required'=>'required'));
                    echo $this->Form->input('numero', array( 'required'=>'required'));
                    echo $this->Form->input('bairro', array( 'required'=>'required'));
                    echo $this->Form->input('cidade', array( 'required'=>'required'));
                    ?> 

                    <div class="form-group">
                        <label for="UsuarioCidade">Estado</label>
                        <?= $this->Estados->select('estado', 'PR', array('class'=>'cep form-control') );?>
                    </div>
                    
                    <div class="form-group required">
                        <?PHP echo $this->Form->input('captcha', array('required'=>true, 'maxlength'=>6, 'autocomplete'=>'off', 'placeholder'=>'C�digo Anti-spam', 'class'=>'w-250 form-control mb-0', 'div'=>array('class'=>'required'))); ?>
                        <?PHP echo $this->Html->image(array('controller' => 'ajax', 'action' => 'get_captcha'), array('id' => 'captcha_image')); ?><a href="javascript:;" id="btn_reload_captcha" class="btn" data-toggle="tooltip" title="Atualizar C�digo"><i class="fa fa-sync"></i></a>
                    </div>
                </div>
            </div>                            
            <div class="row">
                <div class="col-12">                                    
                    <?PHP echo $this->Form->submit('Cadastrar', array('div'=>false, 'class'=>'btn float-right')); ?>     
                </div>
            </div>
            
            <?PHP echo $this->Form->end(); ?>
        
        </div>

    </div>
</div>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
function validaTipo() {    
    if ($('input[name="tipo_pessoa"]:checked').val()=='pf') {
        $( '#div_pf' ).show();
        $( '#div_pj' ).hide().find('input').val('');            
    } else {   
        $( '#div_pj' ).show();
        $( '#div_pf' ).hide().find('input').val('');
    }
}

$(document).ready(function(){
    $('#btn_reload_captcha').click(function() {
        var captcha = $("#captcha_image");
        captcha.attr('src', captcha.attr('src')+'?'+Math.random());
        return false;
    });

    $('#btn_cadastro').click(function(){
        $('#form_cadastro').slideToggle();
    });

    $('form#UsuarioCadastroForm').submit(function(){
        var boo = true;
        if ($('input[name="tipo_pessoa"]:checked').val()=='pj') {

            if (($('input[name="data[Usuario][cnpj]"]').val()=='') || ($('input[name="data[Usuario][razao_social]"]').val()=='')) {
                dialog('Erro', 'Por favor, preencha seu CNPJ e raz�o social.')
                boo = false;
            }

        }

        return boo;
    });
    
    validaTipo();
    $('input[name="tipo_pessoa"]').click(function() { validaTipo(); });
});  
<?php $this->Html->scriptEnd(); ?></script>