<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo __( $title_for_layout ); ?>
                </div>
			</div>
            
			<div class="panel-body">

                <?PHP echo $this->Session->flash(); ?>                                
                <?PHP                
                echo $this->Form->create('Usuario', array(
                    'data-model' => 'Usuario',
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class="col-lg-10">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                
                $Objeto = $this->request->data;
                
                echo $this->Form->input('id');
                echo $this->Form->input('nivel_id', array('empty' => 'Selecione'));
                echo $this->Form->input('nome');
                echo $this->Form->input('email', array('class'=>'email form-control', 'autocomplete'=>'off'));
                echo $this->Form->input('password', array('value' => '', 'autocomplete'=>'off'));
                echo $this->Form->input('telefone', array('class'=>'telefone form-control'));                
                echo $this->Form->input('telefone_2', array('class'=>'telefone form-control'));                
                echo $this->Form->input('newsletter', array('class'=> 'checkbox') );
                echo $this->Form->input('cep', array('class'=>'cep form-control') );
                echo $this->Form->input('endereco', array('type'=>'text', 'label' => array('text'=>'Endere�o', 'class' => 'col-lg-2 control-label')) );
                echo $this->Form->input('numero', array('label' => array('text'=>'N�mero', 'class' => 'col-lg-2 control-label')) );
                echo $this->Form->input('bairro' );
                echo $this->Form->input('cidade' );
                ?>                
				<div class="form-group">
					<label for="Estado" class="col-lg-2 control-label">Estado</label>
					<div class="col-lg-10"><?PHP echo $this->Estados->select('estado', 'PR', array('class'=>'form-control') ); ?></div>
				</div>
                
                <?PHP
                echo $this->Form->input('foto', array( 'type' => 'file'));                
                if (!empty($Objeto['Usuario']['foto']) && (!empty($Objeto['Usuario']['id']))) {
                    $img = $this->GFunction->getImage( $Objeto['Usuario']['foto'], 'usuario/foto/');
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $this->webroot.$img; ?>">
                                <?PHP echo $this->Timthumb->image('/'.$img, array('width' => 80, 'height' => 80, 'zoom_crop'=> 1), array('class'=>'thumb')); ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['Usuario']['foto']; ?></code></p>
                            <?php echo $this->Html->link( '<i class="coloredicon application-delete"></i> Remover', array('action' => 'remover_foto', $Objeto['Usuario']['id']), array('title'=>'Excluir', 'class'=>'btn btn-default btn-gradient confirmLink', 'escape' => false)); ?>
                        </div>
                    </div>
                </div>
                <?PHP } ?>                

                <?PHP echo $this->element('admin/input_historico_cadastro'); ?>
                                
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?PHP echo $this->element('admin/botoes_formulario'); ?>
                    </div>
                </div>
                
                <?PHP echo $this->Form->end(); ?>                    
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    
});
<?php $this->Html->scriptEnd(); ?></script>