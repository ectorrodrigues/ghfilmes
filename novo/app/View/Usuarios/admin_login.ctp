<div class="panel">
	<div class="panel-heading">
		<div class="panel-title"><i class="fa fa-lock"></i> Login</div>
	</div>
    
    <?php echo $this->Form->create('Usuario', array(
                'class'=>'cmxform',
                'id' => 'loginForm',
                'inputDefaults' => array(
                    'format' => array('input'),
                    'class' => 'form-control '
                )
    ) ); ?>
    
		<div class="panel-body">
            
            <?php echo $this->Session->flash(); ?>
                        
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <?PHP echo $this->Form->input('email', array('class'=>'form-control email', 'tabindex'=>'1', 'required' => 'required', 'placeholder'=>'E-mail')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <?PHP echo $this->Form->input('password', array('tabindex'=>'2', 'required' => 'required', 'placeholder'=>'Senha')); ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="form-group margin-bottom-none">
                <?PHP
                echo $this->Html->link( '<i class="imoon imoon-enter"></i> Entrar', 'javascript:;',
                    array('title'=>'Salvar', 'tabindex'=>'3', 'class'=>'btn btn-gradient btn-primary pull-right bt_salvar', 'escape' => false)
                );
                ?>
				<div class="clearfix">
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(array('style' => 'display:none;', 'div' => false, 'class'=>'btn btn-info right', 'id' => 'loginBtn', 'label'=>__('Entrar') )); ?>
</div>


<?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){ $('#UsuarioEmail').focus(); });
<?php $this->Html->scriptEnd(); ?>