<!--Page Title-->
<section class="page-title" style="background-image:url(<?PHP echo $this->webroot; ?>img/site/background/contato.jpg);">
    <div class="auto-container">
        <h1>Representantes</h1>
    </div>
    <!--Page Info-->
    <div class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?PHP echo Router::url('/', true); ?>">In�cio</a></li>
                    <li><a href="<?PHP echo Router::url(array('controller'=>'usuarios', 'action'=>'representantes'), true); ?>">Representantes</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->



<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side col-xs-12">

                <div clas="row">
                    <div class="col-xs-6">
                        <?=$this->element('mapa_brasil');?>
                    </div>

                    <div class="col-xs-6">
                        <div class="sidebar-title mb-15">
                            <h2>Selecione um estado</h2>
                        </div>

                        <div class="row clearfix" id="lista_representantes"></div>
                    </div>
                </div>
                

            </div>
            
        </div>
    </div>
</div>




<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
jQuery(document).ready(function() {
    $('#svg-map path, #svg-map text').click(function(e){
        e.preventDefault();

        $('#svg-map a').removeClass('active');

        var $this = $(this);
        var estado = $this.closest('a').addClass('active').attr('xlink:href').replace('#','');

        $.ajax({
            url: URL_BASE + "usuarios/ajax",
            data: { 'tipo_acao': 'listar_representantes', 'estado':estado },
            async: false,
            type: 'POST',
            dataType: "json",
            beforeSend: function() { dialogLoading(); },
            error: function( jqXHR, textStatus ) { closeLoading(); dialog('Erro', 'Problema na requisi��o.<br />Tente novamente mais tarde.') },
            success: function( data ) {
                
                var _html = '<h3 class="c-theme">'+ $this.closest('a').find('title').text() +'</h3>';
                if ((data.status)&&(data.retorno.length>0)) {
                    $.each(data.retorno, function(key, obj) {

                        var _UF_vendedor = '';
                        $.each( obj.Estado, function(key, objE) {
                            _UF_vendedor += objE.titulo + ', ';
                        });

                        _html += '<div class="services-block col-xs-12">\
                                <div class="inner-box hvr-float">\
                                    <div class="lower-box">\
                                        <h3>'+ obj.Usuario.nome +'</h3>\
                                        <p>\
                                            '+ obj.Usuario.email +'<br />\
                                            '+ obj.Usuario.telefone +'<br />\
                                            '+ obj.Usuario.telefone_2 +'<br />\
                                            '+ obj.Usuario.endereco +'<br />\
                                            Estados: '+ _UF_vendedor +'\
                                        </p>\
                                    </div>\
                                </div>\
                            </div>';                        
                    });
                } else { _html += '<p>Nenhum representante encontrado.</p>'; }
                $('#lista_representantes').html( _html );
                
                closeLoading();
            }
        });
        return false;
    });
});
<?php $this->Html->scriptEnd(); ?></script>