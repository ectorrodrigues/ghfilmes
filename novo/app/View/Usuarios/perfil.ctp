<div class="container">

    <div class="row mt-2">
        <div class="breadcrumb">
            // MEUS DADOS
        </div>
    </div>

    <div class="row justify-content-around pb-5">
        <div class="col-3">
            <?PHP echo $this->element('site/menu-usuario'); ?>
        </div>

        <div class="col-9 p-20">
                
            <?PHP echo $this->Session->flash(); ?>

            <?PHP
            /** Cadastro */                                            
            echo $this->Form->create('Usuario', array(
                                        'type' => 'file',
                                        'inputDefaults' => array(
                                            'format' => array('label', 'input', 'error'),
                                            'class' => 'form-control txt',
                                            'div' => array('class' => 'form-group'),
                                            'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error'))
                                        )
            ) );                    
            $Objeto = $this->request->data['Usuario'];
            ?>

            <div class="row">
                
            <div class="col-6">
                    <h4 class="mb-20">Dados Pessoais:</h4>
                    <?PHP
                    echo $this->Form->input('nome', array( 'required'=>'required'));
                    echo $this->Form->input('email', array('class'=>'email form-control', 'required'=>'required'));
                    echo $this->Form->input('password', array('value' => '', 'autocomplete'=>'off'));
                    ?>

                    <div class="form-group mt-60">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tipo_pessoa" id="bt_pf" value="pf" <?= ((!empty($Objeto['cpf'])) ? 'checked="checked"' : ''); ?>>
                            <label class="form-check-label" for="bt_pf">Pessoa F�sica</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tipo_pessoa" id="bt_pj" value="pj" <?= ((!empty($Objeto['cnpj'])) ? 'checked="checked"' : ''); ?>>
                            <label class="form-check-label" for="bt_pj">Pessoa Jur�dica</label>
                        </div>
                    </div>

                    <div id="div_pf">
                        <?PHP
                        echo $this->Form->input('cpf', array('class'=>'cpf form-control'));
                        ?>
                    </div>
                    <div id="div_pj" style="display:none;">
                        <?PHP
                        echo $this->Form->input('cnpj', array('class'=>'cnpj form-control'));
                        echo $this->Form->input('razao_social', array( 'label'=>'Raz�o Social'));
                        // echo $this->Form->input('contato', array( 'label'=>'Falar com'));
                        ?>
                    </div>                    
                    
                    <?PHP
                    echo $this->Form->input('telefone', array('class'=>'telefone form-control', 'required'=>'required'));
                    echo $this->Form->input('telefone_2', array('class'=>'telefone form-control', 'label'=>'Celular'));
                    ?>
                </div>
                <div class="col-6">
                    <h4 class="mb-20">Endere�o:</h4>
                    <?PHP
                    echo $this->Form->input('cep', array('class'=>'cep form-control', 'required'=>'required'));
                    echo $this->Form->input('endereco', array( 'required'=>'required'));
                    echo $this->Form->input('numero', array( 'required'=>'required'));
                    echo $this->Form->input('bairro', array( 'required'=>'required'));
                    echo $this->Form->input('cidade', array( 'required'=>'required'));
                    ?> 

                    <div class="form-group">
                        <label for="UsuarioCidade">Estado</label>
                        <?= $this->Estados->select('estado', 'PR', array('class'=>'cep form-control') );?>
                    </div>
                    
                    <div class="form-group">
                        <?PHP echo $this->Form->submit('Salvar', array('div'=>false, 'class'=>'btn pull-right')); ?>   
                    </div>
                </div>
            </div>
            <?PHP echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
function validaTipo() {    
    if ($('input[name="tipo_pessoa"]:checked').val()=='pf') {
        $( '#div_pf' ).show();
        $( '#div_pj' ).hide().find('input').val('');            
    } else {   
        $( '#div_pj' ).show();
        $( '#div_pf' ).hide().find('input').val('');
    }
}

$(document).ready(function() {

    $('form#UsuarioPerfilForm').submit(function(){
        var boo = true;
        if ($('input[name="tipo_pessoa"]:checked').val()=='pj') {

            if (($('input[name="data[Usuario][cnpj]"]').val()=='') || ($('input[name="data[Usuario][razao_social]"]').val()=='')) {
                dialog('Erro', 'Por favor, preencha seu CNPJ e raz�o social.')
                boo = false;
            }

        }

        return boo;
    });
    
    validaTipo();
    $('input[name="tipo_pessoa"]').click(function() { validaTipo(); });
});  
<?php $this->Html->scriptEnd(); ?></script>