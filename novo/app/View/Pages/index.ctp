<?= $this->element('site/banner'); ?>

<?PHP if(count($categorias_home)>0) { ?>
<section class="categorias">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">

                <div class="row justify-content-center">
                    <?PHP  //for($i=0; $i<4; $i++) { ?>
                    <?PHP $aosodd_inc = 1;  $aosoffset_inc = '100'; foreach($categorias_home as $Categoria) { ?>
                      <?PHP
                          if(($aosodd_inc % 2) == 0){
                              $dataaos = 'data-aos="fade-right"';
                          } else{
                              $dataaos = 'data-aos="fade-left"';
                          }

                          if($aosodd_inc == 3){
                            $aosoffset_inc = 100;
                          }
                      ?>
                        <div class="col-lg-5 col-12 text-center item" <?PHP echo $dataaos; ?> data-aos-offset="<?PHP echo(800+$aosoffset_inc);?>"  data-aos-duration="1000">
                            <a href="<?=$Categoria['CategoriaVideo']['_url'];?>"><?PHP echo $this->Timthumb->image( $Categoria['CategoriaVideo']['_Icone']['img'], array('width' => 398, 'height' => 302, 'zoom_crop'=> 1), array('class'=>'img-fluid', 'alt'=>'Foto categoria')); ?></a>
                            <a href="<?=$Categoria['CategoriaVideo']['_url'];?>" class="title"><?=$Categoria['CategoriaVideo']['titulo'];?></a>
                        </div>
                    <?PHP $aosodd_inc++; $aosoffset_inc = $aosoffset_inc+100; } ?>
                </div>

                <div class="row" style="z-index:400;">
                    <div class="col-12">
                        <a href="<?=Router::url(array('controller'=>'videos', 'action'=>'categorias'), true);?>" class="ver_mais btn_bottom">+ Ver Todos os segmentos</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?PHP } ?>

<section class="empresa px-lg-0">
    <div class="title" data-aos="fade-down" data-aos-offset="900"  data-aos-duration="1500" style="z-index:-1;">
        <img src="<?=$this->webroot; ?>img/site/title_empresa.png" alt="Conex�o experi�ncia e resultado" class="img-fluid">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center px-4">
                <h4 class="mb-5">PRODUZIR EXPERI�NCIAS <br/>QUE GERAM RESULTADOS</h4>
                <?= returnNotEmpty($configuracoes[8]['Configuracao']['configuracao'], '<p>'.nl2br($configuracoes[8]['Configuracao']['configuracao']) .'</p>') ;?>
            </div>
        </div>
    </div>
</section>


<?PHP if(count($videos_home)>0) { ?>

<section class="porfolio">
        <h4>Portfolio</h4>

        <div class="row">
            <? // for($i=0; $i<3; $i++) { ?>

              <?PHP $aosodd_inc = 1;  $aosoffset_inc = '100'; foreach($videos_home as $Video) { ?>

                <div class="col-lg-4 item mb-lg-3 mb-5 px-lg-4 px-4" data-aos="fade-down" data-aos-delay="<?PHP echo(100+$aosoffset_inc);?>" data-aos-offset="800" data-aos-duration="1000">
                    <a href="<?=$Video['Video']['_url'];?>"><?PHP echo $this->Timthumb->image( $Video['Video']['_Foto']['img'], array('width' => 360, 'height' => 204, 'zoom_crop'=> 1), array('class'=>'img-fluid', 'alt'=>'Foto Video')); ?></a>
                    <a href="<?=$Video['Video']['_url'];?>" class="title"><?=$Video['Video']['titulo'];?></a>
                    <?=returnNotEmpty($Video['Video']['cliente'], '<span class="cliente">[#var#]</span>');?>
                </div>
            <?PHP $aosodd_inc++; $aosoffset_inc = $aosoffset_inc+400;  } ?>
        </div>

        <div class="d-block text-center mt-3">
            <a href="<?=Router::url(array('controller'=>'videos', 'action'=>'categorias'), true);?>" class="ver_mais">+ Ver mais</a>
        </div>
</section>
<?PHP } ?>


<?PHP if(count($parceiros)>0) { ?>
<section class="parceiros">

    <h4 class="mb-5">Parceiros</h4>

    <div class="carrousel">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 owl-carrousel">
                    <div class="owl-carousel" id="carousel_parceiros">
                        <?PHP // for($i=0; $i<3; $i++) { ?>
                        <?PHP foreach($parceiros as $Parceiro) { ?>
                            <div class="item">
                            <?PHP echo $this->Timthumb->image( $Parceiro['Parceiro']['_Foto']['img'], array('width' => 400, 'height' => 400, 'zoom_crop'=> 2), array( 'alt'=>'Parceiro')); ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?PHP } ?>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<? // $this->element('site/menu'); ?>
<? // Router::url('/', true); ?>
<? // $this->webroot; ?>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    $('#carousel_parceiros').owlCarousel({
        autoplay:true,
        loop:true,
        margin: 100,
        dots: false,
        nav:false,
        responsive:{
            0:{
                items:1,
            },
            768:{
                items:2,
            },
            992:{
                items:4,
            }
        }
    });
});
<?php $this->Html->scriptEnd(); ?></script>
