<?php if ($Conteudo['Conteudo']['youtube']) { ?>
    
    <?PHP $url_youtube = 'https://www.youtube.com/watch?v='. getIdYoutube($Conteudo['Conteudo']['youtube']) .'&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0'; ?>
    <section class="foto_institucional">
        <a href="<?=$url_youtube;?>" data-fancybox="<?=$url_youtube;?>" class="btn_video d-block" >
            <img src="<?=$this->webroot; ?>img/site/bg_institucional.jpg" alt="Foto paisagem com Lhama" class="img-fluid">
            <p style="position:absolute; z-index:200; left:50%; top:50%; font-size:2.5em;">
              <i class="fas fa-play"></i>
            </p>
        </a>
    </section>

<?php } else { ?>
    <section class="foto_institucional">
        <img src="<?=$this->webroot; ?>img/site/bg_institucional.jpg" alt="Foto paisagem com Lhama" class="img-fluid">
    </section>
<?php }  ?>


<section class="empresa parallax" style="margin-top:-50px !important; background-image:url('../../novo/app/img/site/bg_portfolio.jpg'); background-repeat: no-repeat; background-size:120%;" >
    <div class="title">
        <img src="<?=$this->webroot; ?>img/site/title_empresa.png" alt="Conex�o experi�ncia e resultado" class="img-fluid">
    </div>


        <div class="row pb-5">
            <div class="col-md-10 offset-md-1 text-center pb-5 px-lg-0 px-5">
                <h3 class="mb-5">PRODUZIR EXPERI�NCIAS <br/>QUE GERAM RESULTADOS</h3>
                <?= returnNotEmpty($configuracoes[8]['Configuracao']['configuracao'], '<p>'.nl2br($configuracoes[8]['Configuracao']['configuracao']) .'</p>') ;?>
            </div>
        </div>

</section>


<section class="historia mt-lg-5 pt-lg-5 mt-0 pt-0 justify-content-center">

    <h4 class="mt-lg-5 pt-lg-5 mt-0 pt-0 mb-5 text-center">NOSSA HIST�RIA</h4>

    <!-- ADICIONAR CAROUSEL DE FOTOS E VIDEOS (COM ADMIN) -->
    <div class="row mb-5 justify-content-center">
        <div class="col-10">
            <section class="owl-carrousel">
                <div class="owl-carousel" id="carousel_fotos">
                    <div class="item">
                        <?=$this->Timthumb->image($Conteudo['Conteudo']['_Foto']['img'], array('width' => 960, 'height' => 540, 'zoom_crop'=>1), array('class'=>'img-fluid') );?>
                    </div>
                    <?PHP foreach($Conteudo['Foto'] as $Foto) { ?>
                        <div class="item">
                            <?=$this->Timthumb->image($Foto['_Arquivo']['img'], array('width' => 960, 'height' => 540, 'zoom_crop'=>1), array('class'=>'img-fluid') );?>
                        </div>
                    <?PHP } ?>
                </div>
                <div id="nav_fotos" class="owl-nav d-flex justify-content-between align-items-center"></div>
            </section>
        </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-9">
        <?= $Conteudo['Conteudo']['conteudo'] ;?>
        </div>
      </div>
    </div>

</section>


<?PHP if(count($parceiros)>0) { ?>
<section class="parceiros mt-lg-5 pt-lg-5 mt-0 pt-0">

    <h4 class=" mt-lg-5 pt-lg-5 mt-0 pt-0 mb-5">Parceiros</h4>

    <div class="carrousel">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 owl-carrousel">
                    <div class="owl-carousel" id="carousel_parceiros">
                        <?PHP // for($i=0; $i<3; $i++) { ?>
                        <?PHP foreach($parceiros as $Parceiro) { ?>
                            <div class="item">
                            <?PHP echo $this->Timthumb->image( $Parceiro['Parceiro']['_Foto']['img'], array('width' => 400, 'height' => 400, 'zoom_crop'=> 2), array( 'alt'=>'Parceiro')); ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?PHP } ?>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<? // $this->element('site/menu'); ?>
<? // Router::url('/', true); ?>
<? // $this->webroot; ?>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    
    $('#carousel_fotos').owlCarousel({
        // autoplay:true,
        // autoplayHoverPause:true,
        video:true,
        loop:true,
        margin: 0,
        items: 1,
        dots: false,
        nav:true,
        navContainer: '#nav_fotos',
        onPlayVideo: function(event) {
            $this = $( event.target );
            var $current = $this.find(".owl-item.active");
            $current.find('.owl-video-play-icon').remove();
            $current.find('.owl-video-tn').remove();
        }
    });

    $('#carousel_parceiros').owlCarousel({
        autoplay:true,
        loop:true,
        margin: 100,
        dots: false,
        nav:false,
        responsive:{
            0:{
                items:1,
            },
            768:{
                items:2,
            },
            992:{
                items:4,
            }
        }
    });
});
<?php $this->Html->scriptEnd(); ?></script>

