
<section class="contato px-lg-5 px-1">

        <div class="row justify-content-center pb-5">
            <div class="col-10"><h4><i class="fas fa-play"></i>Contato</h4></div>
        </div>

        <div class="row justify-content-around pb-5">

          <div class="col-10">
            <div class="row">



            <div class="col-md-6">
                <?PHP echo $this->Session->flash(); ?>

                <h5>Fale Conosco.</h5>

                <?PHP
                /** Formul�rio e-mail */
                    echo $this->Form->create('Contato', array(
                                                'autocomplete'=>'off',
                                                'class'=>'row no-gutters',
                                                'inputDefaults' => array(
                                                    'required'=>true, 'class'=>'form-control',
                                                    'format' => array('input', 'error'),
                                                    'div' => array('class' => 'form-group col-12'),
                                                    'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                                                )
                    ) );
                    ?>
                    <?PHP echo $this->Form->input('nome', array('placeholder'=>'Nome *')); ?>

                    <?PHP echo $this->Form->input('email', array( 'class'=>'email form-control', 'placeholder'=>'E-mail *')); ?>
                    <?PHP echo $this->Form->input('telefone', array( 'class'=>'telefone form-control', 'placeholder'=>'Telefone')); ?>
                    <?PHP echo $this->Form->input('assunto', array('placeholder'=>'Assunto')); ?>
                    <?PHP echo $this->Form->input('mensagem', array('type'=>'textarea', 'placeholder'=>'Mensagem *')); ?>

                    <div class="form-group col-lg-6 col-12 required">
                        <?PHP echo $this->Form->input('captcha', array('required'=>true, 'maxlength'=>6, 'autocomplete'=>'off', 'placeholder'=>'C�digo Anti-spam', 'class'=>'form-control mb-0', 'div'=>array('class'=>'required mb-lg-0 mb-3'))); ?>
                        <?PHP echo $this->Html->image(array('controller' => 'ajax', 'action' => 'get_captcha'), array('id' => 'captcha_image')); ?><a href="javascript:;" id="btn_reload_captcha" class="btn btn-theme" data-toggle="tooltip" title="Atualizar C�digo"><i class="fa fa-sync"></i></a>
                    </div>
                    <div class="form-group col-6 required">
                        <?PHP echo $this->Form->submit('Enviar', array('class'=>'btn btn-theme float-right', 'div'=>false)); ?>
                    </div>

                    <?PHP echo $this->Form->input('tipo_contato', array( 'type'=>'hidden', 'value'=>'contato', )); ?>
                <?PHP echo $this->Form->end(); ?>
            </div>

            <div class="col-md-6 text-right information">

                <strong><?= $configuracoes[6]['Configuracao']['configuracao']; ?></strong>
                <?= returnNotEmpty(preg_replace('/\D/', '', $configuracoes[5]['Configuracao']['configuracao']), '<a href="https://wa.me/[#var#]" target="_blank"><strong><i class="fab fa-whatsapp mr-3"></i>'.$configuracoes[5]['Configuracao']['configuracao'].'</strong></a>') ;?>
                <p><?= nl2br( $configuracoes[9]['Configuracao']['configuracao'] ); ?></p>
                <br />

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3624.2332557304053!2d-53.75917868517837!3d-24.71887111123591!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94f395eaf234d4cf%3A0xc1de2b634d39cdd0!2sGH%20Filmes!5e0!3m2!1spt-BR!2sbr!4v1585118795484!5m2!1spt-BR!2sbr" style="width:90%;" width="90%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

            </div>

        </div>
      </div>
    </div>

</section>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    $('#btn_reload_captcha').click(function() {
        var captcha = $("#captcha_image");
        captcha.attr('src', captcha.attr('src')+'?'+Math.random());
        return false;
    });
});
<?php $this->Html->scriptEnd(); ?></script>
