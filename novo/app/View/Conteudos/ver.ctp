<?PHP
switch($this->params['type']) {
    case 'conteudos':$tipo = 'Conte�dos'; break;
}
?>
<section class="conteudo">

        <div class="row justify-content-center">
            <div class="col-12">
                <h1 class="title text-center"><?=$Conteudo['Conteudo']['titulo'];?></h1>
                <div class="d-block text-center p-4 author">Publicado em <?=data( (($Conteudo['Conteudo']['data'])?$Conteudo['Conteudo']['data']:$Conteudo['Conteudo']['created']) );?> por GH Filmes | <div class="addthis_inline_share_toolbox_nk3r"></div></div>
            </div>
        </div>

        <div class="row mb-5 justify-content-center">
            <div class="col-10">

                <section class="owl-carrousel">
                    <div class="owl-carousel" id="carousel_fotos">
                        <div class="item">
                            <?=$this->Timthumb->image($Conteudo['Conteudo']['_Foto']['img'], array('width' => 960, 'height' => 540, 'zoom_crop'=>1), array('class'=>'img-fluid') );?>
                        </div>
                        <?PHP foreach($Conteudo['Foto'] as $Foto) { ?>
                            <div class="item">
                                <?=$this->Timthumb->image($Foto['_Arquivo']['img'], array('width' => 960, 'height' => 540, 'zoom_crop'=>1), array('class'=>'img-fluid') );?>
                            </div>
                        <?PHP } ?>
                    </div>
                    <div id="nav_fotos" class="owl-nav d-flex justify-content-between align-items-center"></div>
                </section>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-10 content">
                <?=$Conteudo['Conteudo']['conteudo'];?>
            </div>
        </div>

</section>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4db7594a06bb0f2e"></script>
<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){

    $('#carousel_fotos').owlCarousel({
        // autoplay:true,
        // autoplayHoverPause:true,
        video:true,
        loop:true,
        margin: 0,
        items: 1,
        dots: false,
        nav:true,
        navContainer: '#nav_fotos',
        onPlayVideo: function(event) {
            $this = $( event.target );
            var $current = $this.find(".owl-item.active");
            $current.find('.owl-video-play-icon').remove();
            $current.find('.owl-video-tn').remove();
        }
    });
});
<?php $this->Html->scriptEnd(); ?></script>
