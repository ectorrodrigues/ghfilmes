<section class="conteudo row justify-content-center">
    <div class="lista_conteudo col-10 justify-content-center">
        <div class="row justify-content-center pb-5 px-5">
            <div class="col-lg-6"><h4><i class="fas fa-play"></i>Blog</h4></div>
            <div class="col-lg-4 ml-auto">

            <?PHP /** FORM */
            echo $this->Form->create("Filter", array(
                        'id' => 'FormConteudoFiltro',
                        'inputDefaults' => array(
                            'format' => array('input'),
                            'div' => false,
                            'label'=>false,
                            'class' => 'form-control'
                        )
            ));?>

            <div class="form-group">
                <?PHP echo $this->Form->input('keyword', array('class'=>'form-control')); ?>
                <button class="btn"><i class="fa fa-search"></i></button>
            </div>

            <?PHP echo $this->Form->end(); ?>
            </div>
        </div>


        <?PHP if(count($conteudos)>0) { foreach($conteudos as $key=> $Conteudo) { ?>
            <div class="row">
                <div class="col-md-6 text-center">
                    <a href="<?=$Conteudo['Conteudo']['_url'];?>"><?= $this->Timthumb->image($Conteudo['Conteudo']['_Foto']['img'], array('width' => 480, 'height' => 360, 'zoom_crop'=>1), array('class'=>'img-fluid', 'alt'=>'Foto Blog') ); ?></a>
                </div>
                <div class="col-md-6 p-5">
                    <a href="<?=$Conteudo['Conteudo']['_url'];?>" class="text-uppercase"><?=$Conteudo['Conteudo']['titulo'];?></a>
                    <p><?=excerpt( strip_tags($Conteudo['Conteudo']['conteudo']), 300 );?></p>
                    <a class="btn_ver_mais" href="<?=$Conteudo['Conteudo']['_url'];?>">+ ver mais</a>
                </div>
            </div>
        <?PHP } } else {  ?>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Ops. N�o encontramos nenhum registro.</p>
                </div>
            </div>
        <?PHP } ?>
<!--
        <div class="row">
            <div class="col-12">
                <ul>
                    <?PHP
                    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active','tag' => 'li'));
                    ?>
                </ul>
            </div>
        </div> -->
    </div>
</section>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){
    $('#btn_reload_captcha').click(function() {
        var captcha = $("#captcha_image");
        captcha.attr('src', captcha.attr('src')+'?'+Math.random());
        return false;
    });
});
<?php $this->Html->scriptEnd(); ?></script>
