<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-table"></i> <?PHP echo $title_for_layout; ?>
                </div>
			</div>
            
			<div class="panel-body">
            
                    <?PHP /** FORM */
                    echo $this->Form->create("Filter", array(
                                'inputDefaults' => array(
                                    'format' => array('label', 'input'),
                                    'div' => false,
                                    'label'=>false,
                                    'class' => 'form-control'
                                )
                    ));
                    ?>
                                
                    <?PHP echo $this->Session->flash(); ?>
                    
                    <?PHP echo $this->element('admin/header_listagem'); ?>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="heading">
                            		<th><?PHP echo $this->Paginator->sort('id', 'Id<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th><?PHP echo $this->Paginator->sort('arquivo', 'Arquivo<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th class="ta-c w-50"><?PHP echo __('A��es'); ?></th>                                </tr>
                                <tr class="filter">
                            		<th><?PHP echo $this->Form->input('id', array('class' => 'number')); ?></th>
                            		<th><?PHP echo $this->Form->input('arquivo', array()); ?></th>
                            		<th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                    
<?PHP foreach ($uploads as $upload): ?>
	<tr id="row_<?PHP echo $upload['Upload']['id']; ?>">
		<td><?php echo h($upload['Upload']['id']); ?>&nbsp;</td>
		<td><?php echo h($upload['Upload']['arquivo']); ?>&nbsp;</td>
		<td class="ta-c">
			<div class="btn-group bt_list_table">
				<button type="button" class="btn btn-default btn-gradient dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-cogwheel"></span></button>
				<ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
					<li><a href="<?PHP echo Router::url(array('action' => 'cadastro', $upload['Upload']['id'])); ?>"  title="Editar"><i class="coloredicon application-edit"></i> Editar</a></li>
					<li><a href="<?PHP echo Router::url(array('action' => 'excluir', $upload['Upload']['id'])); ?>" class="confirmLink" title="Excluir"><i class="coloredicon application-delete"></i> Excluir</a></li>
				</ul>
			</div>
		</td>
	</tr>
<?PHP endforeach; ?>
                    
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"><?PHP echo $this->element('admin/paginator'); ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
					<?PHP echo $this->element('admin/footer_listagem'); ?>
                    
                    <?PHP echo $this->Form->end(); ?>     
                    
			</div>
		</div>
	</div>
</div>