<div class="row" id="step1">
	<div class="col-md-5 col-lg-4">
		<div class="row">
			<div class="col-md-12">
				<div class="panel profile-panel">
					<div class="panel-heading">
						<div class="panel-title">
							<i class="fa fa-user"></i> Usu�rio: <?PHP echo $userLogado['Usuario']['nome']; ?>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-5" id="profile-avatar">
                                <a href="javascript:;" id="bt_alterar_foto_perfil"></a>
                                <?php echo $this->Timthumb->image($userLogado['Usuario']['_Foto']['img'], array('width' => 150, 'height' => 112, 'zoom_crop'=>2), array('class'=>'img-fluid')); ?>
							</div>
							<div class="col-xs-7">
								<div class="profile-data">
									<span class="profile-name"><b class="text-primary"><?PHP echo $userLogado['Usuario']['nome']; ?></b></span><br />
                                    <span class="profile-email"> <?PHP echo $userLogado['Usuario']['email']; ?> </span>
                                    
									<ul class="profile-info list-unstyled">                                        
                                        <?PHP echo returnNotEmpty($userLogado['Usuario']['telefone'], '<li><i class="fa fa-phone"></i> [#var#]</li>'); ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
    <?PHP /*
	<div class="col-md-7 col-lg-8">
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						<div class="panel-title">
							<i class="fa fa-star-o"></i> Atualiza��es
						</div>
					</div>
            
        			<div class="panel-body" style="min-height: 126px;">
                        <h3 class="text-primary">Aguarde!</h3>
                        
                    
                    </div>
				</div>
			</div>
		</div>
	</div>
    */ ?>
</div>