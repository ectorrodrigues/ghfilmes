<?php
class TreeHelper extends Helper {
	var $helpers = array('Form', 'Html', 'Timthumb');
    
    public $block_start = '<ul>';
    public $context_start = '<li>';
    public $context = array('<span>%s</span>' => array( 'titulo' ));
    public $context_end = '</li>';
    public $block_end = '</ul>';
    public $carret = '<span class="caret margin-right-sm"></span>';

    /** Fun��o que transforma um Array Threaded (Tree), em array para select (group) */
    function threadedToArrSelect($array=array(), $alias='', $chave='id', $valor='titulo') {
        $arr_retorno = array();
        if ($alias!='') {
            foreach($array as $key=>$val) {
                if ( (isset($val['children'])) && (count($val['children'])>0) ) {
                    $arr_retorno[$val[$alias][$valor]] = $this->threadedToArrSelect( $val['children'], $alias, $chave, $valor );
                } else {
                    if (isset($val[$alias][$chave]) && isset($val[$alias][$valor])) {
                        $arr_retorno[$val[$alias][$chave]] = $val[$alias][$valor];
                    }
                }
            }
        }
        return $arr_retorno;
    }
    
    /** Fun��o gen�rica que trata Tree */
    function show($data, $modelAlias ) {
        $return = $this->list_element($data, $modelAlias, 0);
        return $return;
    }

    function list_element($data, $modelAlias, $level) {        
        $output = '';
        if (count($data)>0) {
            $output .= $this->_outElement( $this->block_start, $modelAlias, $data[0] ); // '<ul class="no-style '. (($level>0)?'hide':'') .'">';
            
            foreach ($data as $key => $val) {
                $output .= $this->_outElement( $this->context_start, $modelAlias, $val );
                $output .= $this->_outElement( $this->context, $modelAlias, $val, isset($val['children'][0]) );
                
                if (isset($val['children'][0])) $output .= $this->list_element($val['children'], $modelAlias, $level + 1);
                
                $output .= $this->_outElement( $this->context_end, $modelAlias, $val );
            }
            $output .= $this->_outElement( $this->block_end, $modelAlias, $data[0] ); // '<ul class="no-style '. (($level>0)?'hide':'') .'">';
        }
        return $output;
    }
    
    function _outElement($elements, $modelAlias, $obj, $children=false) {
        if (is_array($elements)) {
            $str_elementos = ''; 
            foreach($elements as $element => $attributes) {
        
                $arr_valores = array();                
                if (is_array($attributes)) {
                    foreach($attributes as $option) {
                        $valor_retorno = '';
                        if ( is_string($option) && !empty($option) && isset($obj[$modelAlias][$option]) ) {
                            $valor_retorno = $obj[$modelAlias][$option];
                            
                        } else if (is_array($option)) {                            
                            foreach($option as $key_opt=>$value) {
                            
//                            debug($key_opt);
//                            debug($obj);
                            
                                if ( is_string($value) && !empty($value) && isset($obj[$modelAlias][$value]) ) {
                                    $valor_retorno = $obj[$modelAlias][$value];
                                }       
                                
                                switch($key_opt) {
                                    case 'name':
                                        $valor_retorno = 'data[' . implode('][', $value) . ']';
                                        break;                                                                                                                    
                                    case 'checked':
                                        $value = ((!is_array($value)) ? array($value) : $value );
                                        $valor_retorno = ((in_array($obj[$modelAlias]['id'], $value)) ? 'checked="checked"' : '');
                                        break;
                                    case 'selected':
                                        $value = ((!is_array($value)) ? array($value) : $value );
                                        $valor_retorno = ((in_array($obj[$modelAlias]['id'], $value)) ? 'selected="selected"' : '');
                                        break;
                                    case 'strtolower':
                                        $valor_retorno = mb_strtolower($valor_retorno);
                                        break;
                                    case 'code':
                                        $valor_retorno = Inflector::slug(utf8_encode(strtolower($valor_retorno)), '_');
                                        break;
                                    case 'carret':
                                        $valor_retorno = (($children)?$this->carret:'');
                                        break;
                                    case 'text':                                        
                                        if (is_array($value)) {
                                            $valor_retorno = (isset($value[1]) ? $value[1] : '' ) .  $obj[$modelAlias][$value[0]]  . (isset($value[2]) ? $value[2] : '' );
                                        } else {
                                            $valor_retorno = $obj[$modelAlias][$value];
                                        }
                                        break;
                                    case 'moeda':                                        
                                        $valor_retorno = 'R$ ' . number_format($obj[$modelAlias][$value],2,'.',',');
                                        break;
                                        
                                    case 'img':
                                        $img = (($valor_retorno<>'')?'/files/categoria/icone/'.$valor_retorno : '/img/_blank.png');
                                        
                                        App::import('Helper', 'Timthumb');
                                        $valor_retorno = '<img src="'. Router::url( '/' ) . $this->Timthumb->imageUrl( $img, array('width' => 30, 'zoom_crop'=> 2) ) .'" />';
                                        break;
                                        
                                    case 'img_categoria':
                                        $img = '';
                                        
                                        $obj_full =  $obj[$modelAlias]['_Full'];
                                        if (isset($obj_full['_class'])) {
                                            $valor_class = $obj_full;
                                            $img = '<i class="'. $valor_class['_class_tamanho'] .'2x '. $valor_class['_class'] .' obj_class_categoria"></i>';
                                            
                                        } else if (isset($obj_full['img_path'])) {
                                            App::import('Helper', 'Timthumb');
                                            $valor_img = $obj_full;
                                            $img = '<img src="'. $valor_img['img_path'] .'" class="obj_class_categoria" />';
                                        }
                                        
                                        $valor_retorno = $img;
                                        break;
                                    
                                    case 'link_if_filho':
                                        $_id = (isset($obj[$modelAlias]['id']) ? $obj[$modelAlias]['id'] : 0);
                                        $_str = (isset($obj[$modelAlias][$value]) ? $obj[$modelAlias][$value] : null);
                                        
                                        $valor_retorno = '';
                                        if ($_str) {
                                            if ($children) {
                                                $valor_retorno .= '<a href="javascript:;" class="bt_categoria" id="bt_cat_'.$_id.'">'. $this->carret .'<span>'. $_str .'</span></a>';
                                            } else {
                                                $valor_retorno .= '<span>'. $_str .'</span>';
                                            }
                                        }
                                        break;
                                        
                                    case 'link_if_filho_1':
                                        $_id = (isset($obj[$modelAlias]['id']) ? $obj[$modelAlias]['id'] : 0);
                                        $_str = (isset($obj[$modelAlias]['titulo']) ? $obj[$modelAlias]['titulo'] : null);
                                        
                                        $_class_bold = ((in_array($obj[$modelAlias]['id'], $value)) ? 'class="bold"' : '');
                                        $_checked = ((in_array($obj[$modelAlias]['id'], $value)) ? 'checked="checked"' : '');
                                        
                                        
                                        $valor_retorno = '';
                                        $valor_retorno = '<label class="hidden"><input type="checkbox" '. $_checked .' /></label>';
                                        if ($_str) {
                                            if ($children) {
                                                $valor_retorno .= '<a href="javascript:;" class="bt_categoria" id="bt_cat_'.$_id.'">'. $this->carret .'<span>'. $_str .'</span></a>';
                                            } else {
                                                $valor_retorno .= '<span '. $_class_bold .'>'. $_str .'</span>';
                                            }
                                        }
                                        break;
                                        
                                }                             
                            } //foreach $option
                        }
                        $arr_valores[] = $valor_retorno;
                        
                    } //foreach $attributes
                    
                } else $arr_valores[] = (string)$attributes;
                
                $str_elementos .= vsprintf($element, $arr_valores);
            } //foreach $elements
            
            return $str_elementos;
        } else return (string)$elements;
    }
    
}