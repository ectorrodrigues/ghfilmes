<?php  
App::uses('AppHelper', 'View/Helper');
class GFunctionHelper extends Helper { 
    var $helpers = array('Form', 'Timthumb');

    public function getImage( $value, $folder, $str_replace = 'img/logo.png' ) {		
        $img = (($value<>'') ? 'files/' . $folder . $value : $str_replace);
        $img = (  is_file(WWW_ROOT . str_replace('/', DS, $img)  ) ? $img : $str_replace);
        return $img;
    } 
    
    public function generateInputs( $arr, $option = array() ) {
        $str = '';
        if (is_array($arr)) {
            foreach($arr as $key=>$val) {
                if (is_array($val)) {
                    $str .= $this->generateInputs( $val, $option );
                } else {
                    if (   ((count($option)>0) && (in_array($key, $option))) ||
                            (count($option)==0)
                        ) {
                        $str .= $this->Form->input($key, array('value'=>$val, 'name'=>'data['.$key.']', 'id'=>false, 'class'=>false, 'type'=>'hidden', 'format'=>'input'));
                    }
                }
            }   
        }
        return $str;
    }
    
    public function addShortScript( $script = '', $documentReady=true ) {
        if (!empty($script)) {
            if ($documentReady) return '<script type="text/javascript">$(document).ready(function(){ '. $script .' });</script>';
            else return '<script type="text/javascript">'. $script .'</script>';
        }
    }
    
    public function addArrayShortScript( $arr = array(), $model, $name='' ) {
        $str='';
        if ($model && $name) {
            $str = '<script type="text/javascript">$(document).ready(function(){ ';
            foreach($arr as $key=>$val) {
                $str .=  'objArray(\''.$name.'\', '.$val[$model]['id'].', JSON.parse(\''. addslashes( jsonEncode($val[$model]) ) .'\') );' . "\n";
            }
            $str .= ' });</script>';
        }
        return $str;
    }
    public function addArrayFullShortScript( $arr = array(), $model, $name='' ) {
        $str='';
        if ($model && $name) {
            $str = '<script type="text/javascript">$(document).ready(function(){ ';
            foreach($arr as $key=>$val) {
                $str .=  'objArray(\''.$name.'\', '.$val[$model]['id'].', JSON.parse(\''. addslashes( jsonEncode($val) ) .'\') );' . "\n";
            }
            $str .= ' });</script>';
        }
        return $str;
    }
    
    public function singulplural($quantidade=1, $singular='', $plural='', $showNumber=true) {
        if ($quantidade>1) {
            $str = $plural;
        } else {
            $str = $singular;
        }
        return ($showNumber ? $quantidade . ' ' : '') . $str;
    }
    
    public function verifyRole($user_nivel_id=null, $arr_niveis=array()) {
        $arr_niveis = ((is_array($arr_niveis)) ? $arr_niveis : array($arr_niveis));
        return in_array($user_nivel_id, $arr_niveis);        
    }
    
    public function verifyRole2($user = array(), $id_empresa=0) {
        $boo = false;
        if ((count($user)>0)||($id_empresa<>0)) {
            
            if (($user['Usuario']['nivel_sistema_id']==1) || ($user['Usuario']['nivel_sistema_id']==2)) { // Admin ou Vendedor 
                $boo = true;
                
            } else if ($id_empresa<>0) {                
                $arr_id_niveis = Set::combine($user['_empresas_associadas'], '{n}.Empresa.id', '{n}.Empresa.nivel_usuario');
                if ( (isset($arr_id_niveis[$id_empresa])) && ($arr_id_niveis[$id_empresa]==1) ) {
                    $boo = true;
                }
            }
                                    
        }
        return $boo;        
    }
    
    public function concatWords($arr=array(), $str=' ', $prefix=false) {
        $retorno = ''; $contador = 0; $total = count($arr);
        foreach($arr as $key=>$val) {
            if ($val<>'') {
                if ($prefix) {
                    $retorno .= ($contador>0 ? $str : '')  . $val;
                } else {
                    $retorno .= $val . ($key<($total-1) ? $str : '');
                }                
                $contador++;
            }
        }
        return $retorno;
    }
    
    public function formStaticText($name='', $value=' ', $id='', $layout=1) {
        switch($layout) {
            case 2:
                $retorno = '<div class="form-group"><label class="col-lg-3 control-label">'. $name .'</label><div class="col-lg-9"><p class="form-control-static text-muted" '. (($id<>'')?'id="'.$id.'"':'') .'>'. $value .'</p></div></div>';
                break;
            case 1:
            default:
                $retorno = '<div class="form-group"><label class="col-lg-2 control-label">'. $name .'</label><div class="col-lg-10"><p class="form-control-static text-muted" '. (($id<>'')?'id="'.$id.'"':'') .'>'. $value .'</p></div></div>';
                break;
        }        
        return $retorno;
    }
    
    public function linkImagem($url='', $img='', $w=30, $h=30, $zc=1, $link=true) {
        $img = $this->Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=>$zc), array('class'=>'img-fluid', 'width'=>'100%'));
        if ($link) {
            $retorno = (($url)?'<a href="http://'. $url .'" class="img-fluid">':'') . $img . (($url)?'</a>':'');
        } else {
            $retorno = '<a href="'. (($url)?$url:'javascript:;') .'" class="img-fluid">' . $img . '</a>';
        }
                
        return $retorno;
    }

} 
?>