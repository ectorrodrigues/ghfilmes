<?php  
App::uses('AppHelper', 'View/Helper');
class BreadcrumbHelper extends Helper { 

    var $helpers    = array('Html');

    public function display($aBreadcrumbs) {
		$aBreadcrumbs = array_reverse($aBreadcrumbs);
        if (is_array($aBreadcrumbs)) { 
			
			$returnHTML = '<ul class="breadcrumb">'; 
			foreach($aBreadcrumbs as $key => $value) {
				if (isset( $value['url'] )) {
					$returnHTML .= '<li>' . $this->Html->link($value['title'], $value['url'], array('escape'=>false)) . '</li>'; 
				} else {
					$returnHTML .= '<li>' . $value['title'] . '</li>'; 
				}
			} 
			$returnHTML .= '</ul>'; 
			return $returnHTML; 
        } 

    } 

} 
?>