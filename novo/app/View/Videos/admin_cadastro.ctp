<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo __( $title_for_layout ); ?>
                </div>
			</div>            
			<div class="panel-body">

                <?PHP echo $this->Session->flash(); ?>                                
                <?PHP
                $Objeto = $this->request->data;

                echo $this->Form->create('Video', array(
                    'data-model' => 'Video',
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class="col-lg-10">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                
                echo $this->Form->input('id');
                echo $this->Form->input('destaque', array('class'=> 'checkbox') );
                echo $this->Form->input('categoria_video_id', array('empty'=>'Selecione'));
                echo $this->Form->input('titulo');
                echo $this->Form->input('descricao');
                echo $this->Form->input('youtube', array('class'=>'url form-control', 'placeholder'=>'www.', 'between' => '<div class="col-lg-10"><div class="input-group"><span class="input-group-addon">https:// </span>', 'after' => '</div></div>'));
                echo $this->Form->input('foto', array( 'type' => 'file'));
                if ( (isset($Objeto['Video'])) && (array_key_exists('_Foto',$Objeto['Video'])) ) {
                ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">         
                        <div class="left mr10">
                            <a class="thumbnail btn fancybox" href="<?PHP echo $Objeto['Video']['_Foto']['img_path']; ?>">
                                <?PHP echo $Objeto['Video']['_Foto']['img_full']; ?>
                            </a>
                        </div>
                        <div class="left">
                            <p> <code><?PHP echo $Objeto['Video']['foto']; ?></code></p>
                        </div>
                    </div>
                </div>
                <?PHP
                }
                echo $this->Form->input('cliente');
                echo $this->Form->input('desafio', array('label'=>array('text'=>'Desafio e Solu��o', 'class'=>'col-lg-2 control-label')));

                
                echo $this->Form->input('keyword', array('label'=>array('text'=>'Palavras-Chaves', 'class'=>'col-lg-2 control-label')));
                echo $this->Form->input('description', array('label'=>array('text'=>'Descri��o do conte�do', 'class'=>'col-lg-2 control-label')));
                ?>                
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?PHP echo $this->element('admin/botoes_formulario'); ?>
                    </div>
                </div>
                
                <?PHP echo $this->Form->end(); ?>                    
			</div>
		</div>
	</div>
</div>


<?PHP
echo $this->Html->script( array( 
                            'vendor/editor/ckeditor.js',
                            'vendor/editor/adapter-jquery.js',
                        ), array('inline' => false) 
);
?>

<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
jQuery(document).ready(function() {
     CKEDITOR.disableAutoInline = true;
     
     $( 'textarea#VideoDescricao' ).ckeditor();
     $( 'textarea#VideoDesafio' ).ckeditor();
     
});
<?php $this->Html->scriptEnd(); ?></script>