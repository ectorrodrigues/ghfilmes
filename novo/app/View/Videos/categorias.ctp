<section class="lista_videos">

    <div class="row">
        <div class="col-12">
            <ul class="nav justify-content-center">
                <?PHP foreach($categorias as $Categoria) { if ($Categoria[0]['qtde_videos']) { ?>
                    <li class="nav-item"><a class="nav-link <?=(($Categoria['CategoriaVideo']['id']==$selected_category_id)?'active':'');?>" href="<?=$Categoria['CategoriaVideo']['_url'];?>"><?=$Categoria['CategoriaVideo']['titulo'];?></a></li>
                <?PHP } } ?>
            </ul>
        </div>
    </div>

    <div class="row">
        <?PHP foreach($videos as $Video) { ?>
        <div class="col-12 item" data-aos="fade-up" data-aos-offset="200">
          <?PHP $url_youtube = 'https://www.youtube.com/watch?v='. getIdYoutube($Video['Video']['youtube']) .'&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0'; ?>
            <a href="<?= $Video['Video']['_url'];?>"> <!--data-fancybox="<?=$url_youtube;?>"-->
                <?PHP echo $this->Timthumb->image( $Video['Video']['_Foto']['img'], array('width' => 1440, 'height' => 651, 'zoom_crop'=> 1), array('class'=>'img-fluid', 'alt'=>'Foto V�deo')); ?>
                <p>
                    <i class="fas fa-play"></i>
                    <span class="title"><?=$Video['Video']['titulo'];?></span>
                    <?=returnNotEmpty($Video['Video']['cliente'], '<span class="client">[#var#]</span>');?>
                </p>
            </a>
        </div>
        <?PHP } ?>
    </div>

</section>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<? // $this->element('site/menu'); ?>
<? // Router::url('/', true); ?>
<? // $this->webroot; ?>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){

});
<?php $this->Html->scriptEnd(); ?></script>
