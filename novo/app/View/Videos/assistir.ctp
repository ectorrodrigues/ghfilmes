<section class="video">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title"><?=$Video['Video']['titulo'];?></h1>
                <?=returnNotEmpty($Video['Video']['cliente'], '<span class="client">[#var#]</span>');?>
            </div>
        </div>
    </div>

    <?PHP $url_youtube = 'https://www.youtube.com/watch?v='. getIdYoutube($Video['Video']['youtube']) .'&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0'; ?>
    <a href="<?=$url_youtube;?>" data-fancybox="<?=$url_youtube;?>" class="btn_video" >
        <?PHP echo $this->Timthumb->image( $Video['Video']['_Foto']['img'], array('width' => 1440, 'height' => 651, 'zoom_crop'=> 1), array('class'=>'img-fluid', 'alt'=>'Foto V�deo')); ?>
        <p><i class="fas fa-play"></i></p>
    </a>

    <div class="description">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-10">

              <?=returnNotEmpty($Video['Video']['cliente'], '<p><strong class="text-uppercase">Cliente</strong><br/>[#var#]</p>');?>
              <?=returnNotEmpty($Video['Video']['desafio'], '<p><strong class="text-uppercase">DESAFIO E SOLU��O</strong><br/>[#var#]</p>');?>

              <p><strong class="text-uppercase">DESCRI��O</strong></p>
              <div class="d-block"><?=$Video['Video']['descricao'];?></div>
            </div>

          </div>
        </div>
    </div>

</section>


<section class="lhama">
    <img src="<?=$this->webroot; ?>img/site/ico_lhama.png" alt="Lhama">
</section>



<? // $this->element('site/menu'); ?>
<? // Router::url('/', true); ?>
<? // $this->webroot; ?>



<script type="text/javascript"><?php $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function(){

});
<?php $this->Html->scriptEnd(); ?></script>
