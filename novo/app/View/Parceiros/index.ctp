<?PHP if ( isset($BannersHome) && count($BannersHome)>0) { ?>  
<div class="container-fluid first-fluid">
    <div class="row">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="width:100% !important">
            <div class="carousel-inner" role="listbox" style="width:100% !important">

                <?PHP foreach($BannersHome as $cont_banner=>$Banner) { ?>
                  <div class="carousel-item <?=(($cont_banner==0) ? 'active' : '');?>"  style="width:100% !important">
                    <a href="<?=(($Banner['Banner']['url'])?$Banner['Banner']['url']:'javascript:;');?>" style="width:100% !important">
                      <div class="d-block img-fluid w-100" style="width:100% !important">
                        <img src="<?= Router::url('/', true) . $this->Timthumb->imageUrl($Banner['Banner']['_Foto']['img'], array('width' => 1367, 'height' => 401, 'zoom_crop'=>1) ); ?>" style="width:100% !important">
                      </div>
                    </a>
                  </div>
                <? } ?>

                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>

    </div>
</div>
<?PHP } unset($Banner, $cont_banner); ?>


<div class="container mt-5" id="main-institutional-container">
    <div class="row mt-5 mb-5">
        <div class="col-12">
            <h4 style="font-weight: bold;">// SERVI�OS</h4>
        </div>
    </div>

    <div class="row">
        
        <?PHP foreach($servicos as $key => $Servico) { ?>
        <div class="col-12 mt-5 mb-5">
            <div class="row">
                <div class="col-sm-12 col-lg-6 <?= (($key%2 == 0) ? '' : 'order-12');?>">
                    <?=$this->Timthumb->image($Servico['Servico']['_Foto']['img'], array('width' => 480, 'height' => 218, 'zoom_crop'=>1), array('class'=>'institutional-images') );?>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="row">
                        <div class="col-12"><h4 style="font-weight: bold;"><?= $Servico['Servico']['titulo'];?></h4></div>
                        <div class="col-12 mt-5 mb-5"><?= $Servico['Servico']['descricao'];?></div>
                    </div>
                </div>
            </div>
        </div>
        <?PHP } ?>

    </div>
</div>