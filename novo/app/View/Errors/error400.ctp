<div class="row">
	<div class="col-md-12">
        <div class="panel">
        	<div class="panel-heading">
        		<div class="panel-title">
        			<i class="fa fa-ban"></i> Opsss!
        		</div>
        	</div>
        	<div class="panel-body alerts-panel">
                
                <div class="alert alert-danger alert-dismissable">
                    <h2><?php echo $message; ?></h2>
                    <p class="error">
                    	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
                    	<?php printf(
                    		__d('cake', 'The requested address %s was not found on this server.'),
                    		"<strong>'{$url}'</strong>"
                    	); ?>
                    </p>
                </div>
                
                <?php
                if (Configure::read('debug') > 0):
                	echo $this->element('exception_stack_trace');
                endif;
                ?>
    
            
        	</div>
        </div>
    </div>
 </div>