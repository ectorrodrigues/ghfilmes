<div class="row">
    <div class="col-md-12">
        <div class="panel">
        <div class="panel-heading">
            <div class="panel-title"> <i class="fa fa-th-large"></i> <?php echo __d('cake_dev', 'Database Error'); ?> </div>
        </div>
        <div class="panel-body">
        
<h2><?php echo __d('cake_dev', 'Database Error'); ?></h2>

<div class="alert alert-danger alert-dismissable">
    <button data-dismiss="alert" class="close" type="button">�</button>
    
    <p class="error">
    	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
    	<?php echo $message; ?>
    </p>
    <?php if (!empty($error->queryString)) : ?>
    	<p class="notice">
    		<strong><?php echo __d('cake_dev', 'SQL Query'); ?>: </strong>
    		<?php echo h($error->queryString); ?>
    	</p>
    <?php endif; ?>
    
    
</div>
        
<?php if (!empty($error->params)) : ?>
		<strong><?php echo __d('cake_dev', 'SQL Query Params'); ?>: </strong>
		<?php echo Debugger::dump($error->params); ?>
<?php endif; ?>

<?php echo $this->element('exception_stack_trace'); ?>

            
        </div>
        </div>
    </div>
</div>    