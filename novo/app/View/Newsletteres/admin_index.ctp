<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-table"></i> <?PHP echo $title_for_layout; ?>
                </div>
			</div>
            
			<div class="panel-body">
            
                    <?PHP /** FORM */
                    echo $this->Form->create("Filter", array(
                                'inputDefaults' => array(
                                    'format' => array('label', 'input'),
                                    'div' => false,
                                    'label'=>false,
                                    'class' => 'form-control'
                                )
                    ));
                    ?>
                                
                    <?PHP echo $this->Session->flash(); ?>

                    <div class="row margin-bottom header_filtro">
                        <div class="col-xs-3">
                            <div class="btn-group">
                                <?PHP
                                echo $this->Html->link( '<i class="glyphicons glyphicons-circle_plus"></i> Novo', array('action' => 'cadastro'), array('title'=>'Novo', 'class'=>'btn btn-primary btn-gradient', 'escape' => false));
                                echo $this->Html->link( '<i class="glyphicon glyphicon-export"></i> Exportar', array('action' => 'exportar'), array('title'=>'Exportar', 'class'=>'btn btn-primary btn-gradient mr10', 'escape' => false));
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-9">
                            <input type="submit" value="Filtrar" class="btn btn-green2 btn-gradient right mr10" />
                            <?PHP echo $this->Html->link( 'Limpar Filtro', array('limpar_filtro'=>true), array('class'=>'btn btn-green2 btn-gradient right') ); ?>
                        </div>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="heading">
                            		<th>Tipo</th>
        	                		<th><?PHP echo $this->Paginator->sort('email', 'Email<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th><?PHP echo $this->Paginator->sort('created', 'Criado<span class="order"></span>', array('escape' => false)); ?></th>
        	                		<th class="ta-c w-50"><?PHP echo __('A��es'); ?></th>
                                </tr>
                                <tr class="filter">
                            		<th>&nbsp;</th>
                            		<th><?PHP echo $this->Form->input('email', array('type'=>'text')); ?></th>
                            		<th>&nbsp;</th>
                            		<th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                    
<?PHP foreach ($newsletteres as $newsletter): ?>
	<tr id="row_<?PHP echo $newsletter['Newsletter']['id']; ?>">
		<td><?php echo ucwords($newsletter['Newsletter']['tipo']); ?>&nbsp;</td>
		<td><?php echo h($newsletter['Newsletter']['email']); ?>&nbsp;</td>
		<td><?php echo $newsletter['Newsletter']['created']; ?>&nbsp;</td>
		<td class="ta-c">
			<div class="btn-group bt_list_table">
				<button type="button" class="btn btn-default btn-gradient dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-cogwheel"></span></button>
				<ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
					<li><a href="<?PHP echo Router::url(array('controller'=>(($newsletter['Newsletter']['tipo']=='newsletter')?'newsletteres':'usuarios'), 'action' => 'cadastro', $newsletter['Newsletter']['id'])); ?>"  title="Editar"><i class="coloredicon application-edit"></i> Editar</a></li>
                    <?PHP if ($newsletter['Newsletter']['tipo']=='newsletter') { ?>
					<li><a href="<?PHP echo Router::url(array('action' => 'excluir', $newsletter['Newsletter']['id'])); ?>" class="confirmLink" title="Excluir"><i class="coloredicon application-delete"></i> Excluir</a></li>
                    <?PHP } ?>
				</ul>
			</div>
		</td>
	</tr>
<?PHP endforeach; ?>
                    
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"><?PHP echo $this->element('admin/paginator'); ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
					<?PHP echo $this->element('admin/footer_listagem'); ?>
                    
                    <?PHP echo $this->Form->end(); ?>     
                    
			</div>
		</div>
	</div>
</div>