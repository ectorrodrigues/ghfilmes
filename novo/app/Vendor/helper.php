<?PHP

/**
 * FUNCOES PHP
 * @author Anderson Possamai
 * @copyright 2009
 */
 
define('LF', PHP_EOL);
define('NL', "\n"); // new line
define('CR', "\r"); // carriage return
define('TB', "\t"); // tabulator
define('BR', '<br />'); // line break
 
if (substr(PHP_OS, 0, 3) == 'WIN') {
    define('WINDOWS', true);
} else {
    define('WINDOWS', false);
}

/**
 * @tutorial - ARRUMAR Fun��es e suas Vari�veis
 * 
 * gerarLog($str_arquivo, $str_conteudo)
 * codificaJSON( $arr_original )
 * valida_email( $email )
 * valorPorExtenso($valor = 0, $maiusculas = false)
 * transformarLink( $str )
 * getmicrotime()
 * limparTexto()
 * cleanString( $string ) : Limpa String
 * mesExtenso: Converte o mes para extenso date(m) em ingl�s
 * getIdYoutube: 
 */


/**
 * gerarLog: Gera o log um arquivo .txt
 *
 * @param string $str_arquivo
 * @param string $str_conteudo
 */
function gerarLog($str_arquivo, $str_conteudo) {
    $data = date("d-m-y H:i:s");
    $ip = $_SERVER['REMOTE_ADDR'];

    $texto = "[$data][$ip] > $str_conteudo \r\n";

    $fp = fopen( $str_arquivo, "a+");
    $fw = fwrite($fp, $texto);
    $fc = fclose($fp);
}


/**
 * codificaJSON: Codifica um Array para JSON
 *
 * @param array $arr_original
 * @return string
 */
function codificaJSON($arr_original) {
    $str_retorno = '';

    // {"status":false, "retorno":"Mensagem Retorno."}
    if (is_array($arr_original)) {
        $str_retorno = '{';
        foreach ($arr_original as $key => $value) {
            $str_retorno .= '"' . $key . '":"' . $value . '",';
        }
        $str_retorno = substr($str_retorno, 0, -1) . '}';
    } else {
        $str_retorno = 'N�o � array.';
    }
    return $str_retorno;
}


function codificaArrayUtf8($arr_original) {
    foreach ($arr_original as $key => $val) {
        if (is_array($val)) {
            $arr_original[$key] = codificaArrayUtf8($val);
        } else {
            $arr_original[$key] = utf8_encode($val);
        }
    }
    return $arr_original;
}


/**
 * valida_email: Valida se o email foi preenchido corretamente
 *
 * @param string $email
 *
 * return boolean
 */
function valida_email($email) {

    $mail_correto = false;
    if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email,
        0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@")) {
        if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) &&
            (!strstr($email, "\$")) && (!strstr($email, " "))) {

            if (substr_count($email, ".") >= 1) {
                $term_dom = substr(strrchr($email, '.'), 1);

                if (strlen($term_dom) > 1 && strlen($term_dom) < 5 && (!strstr($term_dom, "@"))) {
                    $antes_dom = substr($email, 0, strlen($email) - strlen($term_dom) - 1);
                    $caracter_ult = substr($antes_dom, strlen($antes_dom) - 1, 1);

                    if ($caracter_ult != "@" && $caracter_ult != ".") {
                        $mail_correto = true;
                    }
                }
            }
        }
    }
    return $mail_correto;
}


/**
 * valorPorExtenso: Escreve um valo por extenso
 *
 * @param string $valor
 * @param boolean $maiuscula
 *
 * return boolean
 */
function valorPorExtenso($valor = 0, $maiusculas = false) {
    // verifica se tem virgula decimal
    if (strpos($valor, ",") > 0) {
        // retira o ponto de milhar, se tiver
        $valor = str_replace(".", "", $valor);

        // troca a virgula decimal por ponto decimal
        $valor = str_replace(",", ".", $valor);
    }

    $singular = array(
        "centavo",
        "real",
        "mil",
        "milh�o",
        "bilh�o",
        "trilh�o",
        "quatrilh�o");
    $plural = array(
        "centavos",
        "reais",
        "mil",
        "milh�es",
        "bilh�es",
        "trilh�es",
        "quatrilh�es");

    $c = array(
        "",
        "cem",
        "duzentos",
        "trezentos",
        "quatrocentos",
        "quinhentos",
        "seiscentos",
        "setecentos",
        "oitocentos",
        "novecentos");
    $d = array(
        "",
        "dez",
        "vinte",
        "trinta",
        "quarenta",
        "cinquenta",
        "sessenta",
        "setenta",
        "oitenta",
        "noventa");
    $d10 = array(
        "dez",
        "onze",
        "doze",
        "treze",
        "quatorze",
        "quinze",
        "dezesseis",
        "dezesete",
        "dezoito",
        "dezenove");
    $u = array(
        "",
        "um",
        "dois",
        "tr�s",
        "quatro",
        "cinco",
        "seis",
        "sete",
        "oito",
        "nove");

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    for ($i = 0; $i < count($inteiro); $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    $rt = '';
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " :
            "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000")
            $z++;
        elseif ($z > 0)
            $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];
        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i <
                $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
        return ($rt ? $rt : "zero");
    } elseif ($maiusculas == "2") {
        return (strtoupper($rt) ? strtoupper($rt) : "Zero");
    } else {
        return (ucwords($rt) ? ucwords($rt) : "Zero");
    }

}


/**
 * transformarLink: Transforma os links do texto
 *
 * @param string $str
 *
 * return boolean
 */
function transformarLink($str = '') {
    if (($str <> '') && (is_string($str))) {
        /*
        $er = "/(http(s)?:\/\/(www|.*?\/)?((\.|\/)?[a-zA-Z0-9&%_?=-]+)+)/i";
        preg_match_all($er, $texto, $match);
        
        foreach ($match[0] as $link) {
        $link = strtolower($link);
        $link_len = strlen($link);
        
        //troca "&" por "&amp;", tornando o link v�lido pela W3C
        $web_link = str_replace("&", "&amp;", $link);
        
        $texto = str_ireplace($link, "<a href=\"" . $web_link . "\" target=\"_blank\" title=\"" .
        $web_link . "\" rel=\"nofollow\">" . (($link_len > 60) ? substr($web_link, 0, 25) .
        "..." . substr($web_link, -15) : $web_link) . "</a>", $texto);
        
        }
        */
        $str = preg_replace("/((www\.|(http|https|ftp|news|file)+\:\/\/)[_.a-z0-9-]+\.[a-z0-9\/_:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])/",
            '<a href="$1" target="_blank">$1</a>', $str);
        $str = str_replace('href="www', 'href="http://www', $str);
    }

    return $str;
}


/**
 * getmicrotime: Gera microtime
 *
 * return string
 */
function getmicrotime() {
    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    return $d->format("Y-m-d H:i:s.u");
}


/**
 * limparArray( $array ) : Remove campos em branco
 * 
 */
function limparArray(&$array) {
    $array = array_values(array_filter($array));
    /*
    if (is_array($array)) {
    foreach ($array as $key => $val){                
    if (is_array($val)) {
    limpaArray($val);
    } else {
    $array = array_values(array_filter($array));
    }
    }  
    }
    */
}


/**
 * mesExtenso: Converte o mes para extenso date(m) em ingl�s
 *
 * @param date('m') $date
 *
 * return string
 */
function mesExtenso($mes = '') {

    if ($mes === '') {
        $mes = date('m');
    }

    switch ($mes) {
        case 1:
            $mes = "Janeiro";
            break;
        case 2:
            $mes = "Fevereiro";
            break;
        case 3:
            $mes = "Mar�o";
            break;
        case 4:
            $mes = "Abril";
            break;
        case 5:
            $mes = "Maio";
            break;
        case 6:
            $mes = "Junho";
            break;
        case 7:
            $mes = "Julho";
            break;
        case 8:
            $mes = "Agosto";
            break;
        case 9:
            $mes = "Setembro";
            break;
        case 10:
            $mes = "Outubro";
            break;
        case 11:
            $mes = "Novembro";
            break;
        case 12:
            $mes = "Dezembro";
            break;
    }

    return $mes;
}

/**
 * getIdYoutube: pega Id youtube
 *
 * @param str $str
 *
 * return string
 */
function getIdYoutube($str = '') {
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#",
        $str, $matches);

    return ((isset($matches[0])) ? $matches[0] : '');
}

//App::import('Vendor', 'TheNameOfMyClass', array('file' => 'DavesLibrary'.DS.'DavesClass.php'));

/**
 * utf8_converter: Transforma array em utf8
 * 
 * @param (utf8 / iso)
 *
 * return string
 */
function utf8IsoConverter($array, $charset = 'utf8') {

    if (is_array($array)) {
        foreach ($array as $key => $val) {
            $array[$key] = utf8IsoConverter($val);
        }
        return $array;
    } else {
        if ($charset == 'utf8') {
            return utf8_encode($array);
        } else
            if ($charset == 'iso') {
                return utf8_decode($array);
            }
    }
    return;
}
function addslashesArray($array) {
    if (is_array($array)) {
        foreach ($array as $key => $val) {
            $array[$key] = addslashesArray($val);
        }
        return $array;
    } else {
        return addslashes($array);
    }
    return;
}

function jsonEncode($val = '', $charset = 'utf8', $slashes = true, $json = true) {
    $val = (($slashes) ? addslashesArray($val) : $val);
    $val = (($charset) ? utf8IsoConverter($val, $charset) : $val);
    $val = (($json) ? json_encode($val, JSON_HEX_TAG | JSON_HEX_QUOT) : $val);

    return $val;
}

function converteReplaceSql($campo = '', $arr_caracter = array(), $char = '') {
    $str_retorno = '';
    if (!empty($campo)) {
        if (is_array($arr_caracter)) {
            $int_total = count($arr_caracter);
            foreach ($arr_caracter as $key => $val) {
                if ($key == 0) {
                    $str_retorno = 'replace( ' . $campo . ', "' . $val . '", "' . $char . '" )';
                } else {
                    $str_retorno = 'replace( ' . $str_retorno . ', "' . $val . '", "' . $char .
                        '" )';
                }
            }
        } else {
            $str_retorno = 'replace( ' . $campo . ', "' . (string )$arr_caracter . '", "' .
                $char . '" )';
        }
    }

    return $str_retorno;
}

function converteMoedaDouble($str) {
    if ($str == null) {
        $str = 0;
    }
    $str = trim(str_replace('R$', '', $str));

    if ( !(strpos($str, ',')===false) ) {
        $str = str_replace('.', '', trim($str));
        $str = str_replace(',', '.', $str);
    }

    return (float)$str;
}
function converteDoubleMoeda($valor) {
    return 'R$ ' . number_format($valor, 2, ',', '.');
}

function unserializeCakeArrJquery($model, $arr) {
    $arr_obj = array();
    foreach ($arr as $arr_form) {
        $_atribute = str_replace(array('data[', ']'), '', $arr_form['name']);
        $arr_obj[$model][$_atribute] = utf8_decode($arr_form['value']);
    }
    return $arr_obj;
}

function singulplural($quantidade = 1, $singular = '', $plural = '', $showNumber = true) {
    if ($quantidade > 1) {
        $str = $plural;
    } else {
        $str = $singular;
    }
    return ($showNumber ? $quantidade . ' ' : '') . $str;
}

function plural($str) {
    return acentuar(Inflector::pluralize(removeAcentos($str)));
}

function singular($str) {
    return acentuar(Inflector::singularize(removeAcentos($str)));
}

function acentuar($palavra) {
    $espacamentos = array(' ', '_');
    foreach ($espacamentos as $espacamento) {
        if (strpos($palavra, $espacamento) !== false) {
            $palavra = explode($espacamento, $palavra);
            $saida = '';
            foreach ($palavra as $pedaco) {
                $saida .= acentuar($pedaco) . $espacamento;
            }
            return rtrim($saida, $espacamento);
        }
    }
    if (preg_match('/(.*)cao$/', $palavra, $matches)) {
        return $matches[1] . '��o';
    }
    if (preg_match('/(.*)ao(s)?$/', $palavra, $matches)) {
        return $matches[1] . '�o' . (isset($matches[2]) ? $matches[2] : '');
    }
    if (preg_match('/(.*)coes$/', $palavra, $matches)) {
        return $matches[1] . '��es';
    }
    if (preg_match('/(.*)oes$/', $palavra, $matches)) {
        return $matches[1] . '�es';
    }
    if (preg_match('/(.*)aes$/', $palavra, $matches)) {
        return $matches[1] . '�es';
    }
    return $palavra;
}

/***
* Fun��o para remover acentos de uma string
*
* @autor Thiago Belem <contato@thiagobelem.net>
*/
function removeAcentos($string, $slug = false) {
    $string = $string;

    // C�digo ASCII das vogais
    $ascii['a'] = range(224, 230);
    $ascii['e'] = range(232, 235);
    $ascii['i'] = range(236, 239);
    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
    $ascii['u'] = range(249, 252);

    // C�digo ASCII dos outros caracteres
    $ascii['b'] = array(223);
    $ascii['c'] = array(231);
    $ascii['d'] = array(208);
    $ascii['n'] = array(241);
    $ascii['y'] = array(253, 255);

    foreach ($ascii as $key => $item) {
        $acentos = '';
        foreach ($item as $codigo)
            $acentos .= chr($codigo);
        $troca[$key] = '/[' . $acentos . ']/i';
    }

    $string = preg_replace(array_values($troca), array_keys($troca), $string);

    // Slug?
    if ($slug) {
        // Troca tudo que n�o for letra ou n�mero por um caractere ($slug)
        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
        // Tira os caracteres ($slug) repetidos
        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
        $string = trim($string, $slug);
    }

    return $string;
}

/**
 * limparTexto: Remove acentua��o
 *
 * return string
 */
function limparTexto($variavel) {
    $variavel = htmlentities(strtolower($variavel));
    $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
    $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
    $variavel = trim($variavel, "-");
    return $variavel;
}


function getArrayValue($arr = array(), $model = '', $str = '', $key_return = null) {
    $arr_return = array();
    $_key = -1;
    if ((count($arr) > 0) && (!empty($model)) && (!empty($str))) {
        foreach ($arr as $key => $obj) {
            if ((isset($obj[$model])) && (isset($obj[$model][$str]))) {
                if ($obj[$model][$str] <> '') {
                    $_key = ((($key_return) && (isset($obj[$model][$key_return]))) ? $obj[$model][$key_return] :
                        $_key + 1);
                    $arr_return[$_key] = $obj[$model][$str];
                }
            }
        }
    }
    return $arr_return;
}

function returnNotEmpty($var = '', $str_retorno = '') {
    return (($var <> '') ? str_replace('[#var#]', $var, $str_retorno) : '');
}

function removeBreakLine($str = '') {
    return str_replace(array(
        "\r",
        "\n",
        "\r\n"), "", $str);
}

function data($date = null, $formato = 'd/m/Y', $hoje = false) {
    if ($date) {
        /** Converte $date para formato YYYY-mm-dd */
        $arr_data = explode(' ', $date);
        
        if (strpos($arr_data[0], ':')!==false) { // Se tiver apenas hora
            $arr_data[1] = $arr_data[0];
            $arr_data[0] = date('d/m/Y');
        }        
        
        $hora = '00:00:00';
        if (count($arr_data) > 1) {
            $hora = explode(':', $arr_data[1]);
            switch (count($hora)) {
                case 0:
                    $hora = '00:00:00';
                    break;
                case 1:
                    $hora = implode(':', $hora) . ':00:00';
                    break;
                case 2:
                    $hora = implode(':', $hora) . ':00';
                    break;
                default:
                    $hora = implode(':', $hora);
                    break;
            }
        }
        if (strpos($arr_data[0], '-') === false) {
            $data = implode('-', array_reverse(explode('/', $arr_data[0])));
        } else {
            $data = $arr_data[0];
        }
        $date = $data . ' ' . $hora;        
        $date = date($formato, strtotime($date));
    } else {
        $date = (($hoje) ? date($formato) : '');
    }
    return $date;
}

function slugURL($str='') {
    return strtolower(Inflector::slug( utf8_encode(strtolower($str)) ,'-'));
}

function excerpt($texto='', $limite = 50, $quebra='...') {
    $retorno = '';
    if (!empty($texto)) {
        if (strlen($texto) <= $limite) {
            $retorno = $texto;
        } else {
            
            $arr_aux = explode(' ', $texto);
            if (strlen($arr_aux[0]) > $limite) {
                $retorno = trim(substr($texto, 0, $limite)) .$quebra;
            } else {
                $ultimo_espaco = strrpos(substr($texto, 0, $limite), " ");
                $retorno = trim(substr($texto, 0, $ultimo_espaco)) .' '. $quebra;
            }
        }
    }
    return $retorno;
}
    
function compareDate($dte1=null, $dte2=null) {
    $dte1 = (($dte1) ? $dte1 : date('Y-m-d'));
    $dte2 = (($dte2) ? $dte2 : date('Y-m-d'));
    
    $dte1 = new DateTime(($dte1) ? $dte1 : date() );
    $dte2 = new DateTime(($dte2) ? $dte2 : date() );
    
    $retorno = 0;
    if ($dte1>$dte2) { $retorno = 1; } 
    else if ($dte2>$dte1) { $retorno = -1; }
    
    return $retorno;
}

function generateRandomString($length=5, $chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    $charsLength = strlen($chars);
    $randomString = '';
    for ($i=0;$i<$length;$i++) {
        $randomString .= $chars[rand(0,$charsLength-1)];
    }
    return $randomString;
}

function xml2array( $xmlObject, $out = array () ) {
    $arr = array();
    foreach ($xmlObject as $element) {
        $tag = $element->getName();
        $e = get_object_vars($element);
        if (!empty($e)){
            $arr[$tag] = $element instanceof SimpleXMLElement ? xml2array($element) : $e;
        } else {
            $arr[$tag] = trim($element);
        }
    }
    return $arr;
}

function calculaFrete($cod_servico='41106,40010', $cep_origem, $cep_destino, $peso=1, $altura='2', $largura='11', $comprimento='16', $diametro='0', $valor_declarado='18.50') {
    ###########################################
    # C�digo dos Servi�os dos Correios
    # 41106 PAC sem contrato
    # 40010 SEDEX sem contrato
    # 40045 SEDEX a Cobrar, sem contrato
    # 40215 SEDEX 10, sem contrato
    # dimens�es - m�ximo 105 cm
    ############################################
 
    $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$cep_origem."&sCepDestino=".$cep_destino."&nVlPeso=".$peso."&nCdFormato=1&nVlComprimento=".$comprimento."&nVlAltura=".$altura."&nVlLargura=".$largura."&sCdMaoPropria=n&nVlValorDeclarado=".$valor_declarado."&sCdAvisoRecebimento=n&nCdServico=".$cod_servico."&nVlDiametro=".$diametro."&StrRetorno=xml";
    $xml = simplexml_load_file($correios);
    $json = json_encode($xml);
    return json_decode($json,TRUE);
}

function unserializeCakeArrJquery2($arr) {
    $arr_obj = array();
    foreach ($arr as $arr_form) {
        $arr_obj[$arr_form['name']] = utf8_decode($arr_form['value']);
    }
    return $arr_obj;
}

?>