<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-table"></i> <?PHP echo "<?PHP echo \$title_for_layout; ?>\n"; ?>
                </div>
			</div>
            
			<div class="panel-body">
            
                    <?PHP
                    echo "<?PHP /** FORM */
                    echo \$this->Form->create(\"Filter\", array(
                                'inputDefaults' => array(
                                    'format' => array('label', 'input'),
                                    'div' => false,
                                    'label'=>false,
                                    'class' => 'form-control'
                                )
                    ));
                    ?>
                    ";
                    ?>
            
                    <?PHP echo "<?PHP echo \$this->Session->flash(); ?>\n"; ?>
                    
                    <?PHP echo "<?PHP echo \$this->element('admin/header_listagem'); ?>\n"; ?>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="heading">
            <?php foreach ($fields as $field): ?>
                <?php echo "\t\t<th><?PHP echo \$this->Paginator->sort('{$field}', '". Inflector::humanize($field) . "<span class=\"order\"></span>', array('escape' => false)); ?></th>\n"; ?>
        	<?php endforeach; ?>
                <?PHP echo "\t\t<th class=\"ta-c w-50\"><?PHP echo __('A��es'); ?></th>"; ?>
                                </tr>
                                <tr class="filter">
            <?php foreach ($fields as $key=> $field): ?>
                <?php echo "\t\t<th><?PHP echo \$this->Form->input('{$field}', array(". (($field=='id')? "'class' => 'number'" :'') .")); ?></th>\n"; ?>
            <?php endforeach; ?>
                <?PHP echo "\t\t<th>&nbsp;</th>\n"; ?>
                                </tr>
                            </thead>
                            <tbody>
                    
<?php
echo "<?PHP foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
echo "\t<tr id=\"row_<?PHP echo \${$singularVar}['{$modelClass}']['id']; ?>\">\n";
	foreach ($fields as $field) {
		$isKey = false;
		if (!empty($associations['belongsTo'])) {
			foreach ($associations['belongsTo'] as $alias => $details) {
				if ($field === $details['foreignKey']) {
					$isKey = true;
					echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'cadastro', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
					break;
				}
			}
		}
		if ($isKey !== true) {
			echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
		}
	}
    
    
	echo "\t\t<td class=\"ta-c\">\n";
    echo "\t\t\t<div class=\"btn-group bt_list_table\">\n";
    echo "\t\t\t\t<button type=\"button\" class=\"btn btn-default btn-gradient dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicons glyphicons-cogwheel\"></span></button>\n";
    echo "\t\t\t\t<ul class=\"dropdown-menu checkbox-persist pull-right text-left\" role=\"menu\">\n";
    echo "\t\t\t\t\t<li><a href=\"<?PHP echo Router::url(array('action' => 'cadastro', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\"  title=\"Editar\"><i class=\"coloredicon application-edit\"></i> Editar</a></li>\n";
    echo "\t\t\t\t\t<li><a href=\"<?PHP echo Router::url(array('action' => 'excluir', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\" class=\"confirmLink\" title=\"Excluir\"><i class=\"coloredicon application-delete\"></i> Excluir</a></li>\n";
    echo "\t\t\t\t</ul>\n";
    echo "\t\t\t</div>\n";
    echo "\t\t</td>\n";

/*
	echo "\t\t<td class=\"actions\">\n";    
    echo "\t\t\t<?php echo \$this->Html->link( '<i class=\"coloredicon application-edit\"></i>', array('action' => 'cadastro', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('title'=>'Editar', 'class'=>'btn btn-default btn-gradient','escape' => false)); ?>\n";
    echo "\t\t\t<?php echo \$this->Html->link( '<i class=\"coloredicon application-delete\"></i>', array('action' => 'excluir', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('title'=>'Excluir', 'class'=>'btn btn-default btn-gradient confirmLink', 'escape' => false)); ?>\n";
	echo "\t\t</td>\n";
 */
 
echo "\t</tr>\n";

echo "<?PHP endforeach; ?>\n";
?>
                    
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="<?PHP echo (count($fields)+1); ?>"><?php echo "<?PHP echo \$this->element('admin/paginator'); ?>"; ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <?PHP echo "<?PHP echo \$this->Form->end(); ?>"; ?>     
                    
			</div>
		</div>
	</div>
</div>