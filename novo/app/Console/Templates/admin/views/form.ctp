<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-file-text-o"></i> <?PHP echo "<?PHP echo __( \$title_for_layout ); ?>\n"; ?>
                </div>
			</div>            
			<div class="panel-body">

                <?PHP echo "<?PHP echo \$this->Session->flash(); ?>"; ?>
                                
                <?php
                echo "<?PHP
                \$Objeto = \$this->request->data;\n
                echo \$this->Form->create('{$modelClass}', array(
                    'data-model' => '{$modelClass}',
                    'type' => 'file',
                    'class'=>'form-horizontal verifyEditable',
                    'inputDefaults' => array(
                        'class' => 'form-control',
                        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                        'div' => array('class' => 'form-group'),
                        'label' => array('class' => 'col-lg-2 control-label'),
                        'between' => '<div class=\"col-lg-10\">',
                        'after' => '</div>',
                        'error' => array('attributes' => array('wrap' => 'label', 'class' => 'help-inline error')),
                    )
                ));
                ";
                
        		foreach ($fields as $field) {
        			if (!in_array($field, array('created', 'modified', 'updated'))) {
        				echo "
                echo \$this->Form->input('{$field}');";
        			}
        		}
        		if (!empty($associations['hasAndBelongsToMany'])) {
        			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
        				echo "echo \$this->Form->input('{$assocName}');\n";
        			}
        		}
                
      		    echo "
                ?>";
                ?>
                
                
                <div class="form-group form-actions">
                    <label class="col-lg-2 text-right">&nbsp;</label>
                    <div class="col-lg-10">
                        <?php echo "<?PHP echo \$this->element('admin/botoes_formulario'); ?>\n"; ?>
                    </div>
                </div>
                
                <?php echo "<?PHP echo \$this->Form->end(); ?>"; ?>
                    
			</div>
		</div>
	</div>
</div>