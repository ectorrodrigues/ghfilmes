<?php
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'index'));
    Router::connect('/admin', array( 'admin'=>true, 'controller' => 'dashboard', 'action' => 'index'));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
    Router::connect('/produtos', array('controller' => 'produtos', 'action' => 'listar'));
    Router::connect('/carrinho/*', array('controller' => 'carrinhos'));
    
    Router::connect('/:type', array('admin'=>false, 'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    Router::connect('/:type/:action', array('admin'=>false, 'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    Router::connect('/:type/:action/*', array('admin'=>false, 'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    Router::connect('/admin/:type',array('admin'=>true, 'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    Router::connect('/admin/:type/:action',array('admin'=>true,'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    Router::connect('/admin/:type/:action/*',array('admin'=>true,'controller'=>'conteudos'), array('type'=>'conteudos|blog', 'persist' => array('type')));
    
    Router::parseExtensions('json');
	CakePlugin::routes();
    
	require CAKE . 'Config' . DS . 'routes.php';
