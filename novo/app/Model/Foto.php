<?php
App::uses('AppModel', 'Model');
class Foto extends AppModel {
    
    public $actsAs = array(
        'Upload.Upload' => array(
            'arquivo' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}arquivo{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}',
                'resize' => array(800, 600),
                'thumbnailMethod'  => 'php',
            ),
        )      
    );
    
	public $validate = array(
		'arquivo' => array(
			'isValidExtension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png')),
                'message' => 'Extens�o inv�lida.',
                'allowEmpty' => false,
			),
		),
	);
    
	public $belongsTo = array(
		'Conteudo' => array(
			'className' => 'Conteudo',
			'foreignKey' => 'conteudo_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
            'counterCache' => false,
		)
	);
        
    public function afterFind( $results, $primary ) {
        parent::afterFind($results, $primary);
        $results = $this->arrFoto($results, 'Foto', 'arquivo');
        return $results;
    } 
    
    public function beforeSave($options = array()) {        
        if (!empty( Router::getRequest()->data['Foto']['arquivo']['name'] )) {
            $this->data['Foto']['filename'] = Router::getRequest()->data['Foto']['arquivo']['name'];                        
        } else if (!empty( Router::getRequest()->params['form']['arquivo']['name'] )) {
            $this->data['Foto']['filename'] = Router::getRequest()->params['form']['arquivo']['name'];            
        }
        return true;
    }    
}
