<?php
App::uses('AppModel', 'Model');
class Parceiro extends AppModel {

	public $displayField = 'titulo';
    public $actsAs = array(	
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}',
                'resize' => array(800, 600)
            ),
        )   
	);

	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'foto' => array(
			'extension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
                'message' => 'Extens�o inv�lida',
                'on'      => 'create'
			),  
			'isAboveMinSize' => array(
                'rule' => array('isAboveMinSize', 0),
				'message' => 'Arquivo n�o pode ser vazio.',
                'on'      => 'create'
			),
		),
	);


    function afterFind($results, $primary=false) {
		$results = $this->arrFoto($results, $this->alias, 'foto', 'img/placeholder.jpg');
    	return $results;
    }
}
