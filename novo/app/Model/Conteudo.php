<?php
App::uses('AppModel', 'Model');
class Conteudo extends AppModel {

	public $displayField = 'titulo';	
    public $actsAs = array(	
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
            ),
        )   
	);
	
	public $validate = array(
		'tipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Deve ser num�rico.',
			),
		),
		'titulo' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'conteudo' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'foto' => array(
			'extension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
				'message' => 'Extens�o inv�lida',
				'allowEmpty' => true
			),
		),
	);

	public $belongsTo = array(
		'Tipo' => array(
			'className' => 'Tipo',
			'foreignKey' => 'tipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
	public $hasMany = array(
		'Foto' => array(
			'className' => 'Foto',
			'foreignKey' => 'conteudo_id',
			'dependent' => false,
		)
	);
    
    
    function afterFind($results, $primary=false) {

		$results = $this->arrFoto($results, $this->alias, 'foto', 'img/placeholder.jpg');
    	foreach ($results as $key => $val) {
            if (isset($val['Conteudo'])) {
                switch($val['Conteudo']['tipo_id']) {
                    case 1: $type = 'blog'; break;
                    case 2: $type = 'conteudos'; break;
                }
                
                $results[$key]['Conteudo']['_url'] = Router::url( array('admin'=>false, 'controller'=>'conteudos', 'action'=>'ver', 'type'=>$type , $val['Conteudo']['id'], slugURL($val['Conteudo']['titulo']) ), true );
            }
        }
    	return $results;
    }
}
