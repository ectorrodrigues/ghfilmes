<?php
App::uses('AppModel', 'Model');
class Banner extends AppModel {

	public $useTable = 'banners';
	public $displayField = 'titulo';
    public $actsAs = array(
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}',
                'resize' => array(1920, 420),
            ),
        )      
    );
    
	public $validate = array(
		'tamanho_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Deve ser num�rico',
			),
		),
		'titulo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'foto' => array(
			'isValid' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
                'message' => 'Extens�o inv�lida',
                'allowEmpty' => false,
                'on'      => 'create'
			),
            
		),
	);
    
	public $belongsTo = array(
		'Tamanho' => array(
			'className' => 'Tamanho',
			'foreignKey' => 'tamanho_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function afterFind( $results, $primary ) {
        parent::afterFind($results, $primary);
        $results = $this->arrFoto($results, 'Banner', 'foto');        
        return $results;
    }
    
}
