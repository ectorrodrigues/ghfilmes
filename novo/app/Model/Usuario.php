<?php
App::uses('AppModel', 'Model');
App::uses('BrValidation', 'Localized.Validation');
App::uses('AuthComponent', 'Controller/Component');

class Usuario extends AppModel {
    
    var $name = "Usuario";
	public $displayField = 'nome';
    public $actsAs = array(		
        'Containable',
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
            ),
        )
    );

	public $validate = array(
        'nivel_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Deve ser num�rico.',
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Necess�rio e-mail v�lido.',
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este e-mail j� est� cadastrado.',
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
                'on'      => 'create'
			),
		),
        'cpf' => array(
            'valid' => array(
                'rule' => array('ssn', null, 'br'),
				'message' => 'Necess�rio CPF v�lido.',
                'allowEmpty' => true,
            ),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este cpf j� est� cadastrado.',
                'allowEmpty' => true,
			),
        ),
        'cnpj' => array(
            'valid' => array(
                'rule' => array('ssn', null, 'br'),
				'message' => 'Necess�rio CNPJ v�lido.',
                'allowEmpty' => true,
            ),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este CNPJ j� est� cadastrado.',
                'allowEmpty' => true,
			),
        ),
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
        'telefone' => array(
            'valid' => array(
                'rule' => array('phone', null, 'br'),
                'message' => 'Necess�rio telefone v�lido.',
                'allowEmpty' => false,
            )
		),
        'telefone_2' => array(
            'valid' => array(
                'rule' => array('phone', null, 'br'),
                'message' => 'Necess�rio telefone v�lido.',
                'allowEmpty' => true,
            )
        ),
	);
    
    
	public $belongsTo = array(
		'Nivel' => array(
			'className' => 'Nivel',
			'foreignKey' => 'nivel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
    
    public function beforeSave($options = array()) {        
        if (isset($this->data[$this->alias]['password'])) {
            if (($this->data[$this->alias]['password']=='')||($this->data[$this->alias]['password']==null)) {
                unset($this->data[$this->alias]['password']);
            } else {
                $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
            }
        }
        return true;
    }
    
    public function afterFind( $results, $primary ) {
        parent::afterFind($results, $primary);
        $results = $this->arrFoto($results, 'Usuario', 'foto', 'img/avatar-user.png');
        
        return $results;
    }
}
?>