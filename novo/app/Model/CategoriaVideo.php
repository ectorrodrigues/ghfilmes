<?php
App::uses('AppModel', 'Model');
class CategoriaVideo extends AppModel {

	public $displayField = 'titulo';	
    public $actsAs = array(		
        'Containable',
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
			),
			'icone' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}icone{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
            ),
        )   
	);

	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		// 'foto' => array(
		// 	'extension' => array(
		// 		'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
		// 		'message' => 'Extens�o inv�lida',
		// 		'allowEmpty' => false
		// 	),
		// ),
		'icone' => array(
			'extension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
				'message' => 'Extens�o inv�lida',
				'allowEmpty' => true
			),
		),
	);

	public $hasMany = array(
		'Video' => array(
			'className' => 'Video',
			'foreignKey' => 'categoria_video_id',
		)
	);
    
    function afterFind($results, $primary=false) {

		// $results = $this->arrFoto($results, $this->alias, 'foto', 'img/placeholder.jpg');
		$results = $this->arrFoto($results, $this->alias, 'icone', 'img/placeholder.jpg');

    	foreach ($results as $key => $val) {
            if (isset($val['CategoriaVideo'])) {                
                $results[$key]['CategoriaVideo']['_url'] = Router::url( array('admin'=>false, 'controller'=>'videos', 'action'=>'categorias', $val['CategoriaVideo']['id'], slugURL($val['CategoriaVideo']['titulo']) ), true );
            }
        }
    	return $results;
    }

}
