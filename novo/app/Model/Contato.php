<?php
App::uses('AppModel', 'Model');
App::uses('BrValidation', 'Localized.Validation');
App::uses('AuthComponent', 'Controller/Component');


class Contato extends AppModel {
    
    var $name = "Contato";
    var $useTable = false;

	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Necess�rio e-mail v�lido.',
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		// 'assunto' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		'message' => 'N�o pode ser vazio.',
		// 	),
		// ),
		'mensagem' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
	);
    
}
?>