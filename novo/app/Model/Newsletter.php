<?php
App::uses('AppModel', 'Model');
class Newsletter extends AppModel {

	public $useTable = 'newsletters';

	public $displayField = 'email';
	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'E-mail inv�lido.',
				'allowEmpty' => false,
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este e-mail j� est� cadastrado.',
			),
		),
	);
    
    
    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {    
        $recursive = -1;
        
        $this->useTable = false;
        
        $sql = 'SELECT * FROM (
                    SELECT id, "" AS nome, email, created, "newsletter" AS tipo FROM newsletters UNION 
                    SELECT id, nome AS nome, email, created, "usu�rio" AS tipo FROM usuarios WHERE usuarios.newsletter = 1
        ) AS Newsletter ';
        
        if (count($conditions)>0) {
            $sql .= $this->getDataSource()->conditions($conditions); 
        }
        
        $sql .=  ' LIMIT ' . (($page - 1) * $limit) . ', ' . $limit;
        
        $results = $this->query($sql);        
        return $results;
    }
    
    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {        
        
        $sql = 'SELECT * FROM (
                    SELECT id, "" AS nome, email, created, "newsletter" AS tipo FROM newsletters UNION 
                    SELECT id, nome AS nome, email, created, "usu�rio" AS tipo FROM usuarios WHERE usuarios.newsletter = 1
        ) AS Newsletter ';
        
        if (count($conditions)>0) {
            $sql .= $this->getDataSource()->conditions($conditions); 
        }
        
                
        $this->recursive = $recursive;        
        $results = $this->query($sql);        
        return count($results);
    }
    
}
