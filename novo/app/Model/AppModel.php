<?php
App::uses('Model', 'Model');

class AppModel extends Model {
    function afterFind($results, $primary=false) {
    	foreach ($results as $key => $val) {        
            if (isset($val[$this->name])) {
                if (isset($val[$this->name]['created'])) {
                    $results[$key][$this->name]['_created'] = $val[$this->name]['created'];
                    $results[$key][$this->name]['created'] = (($val[$this->name]['created']<>'0000-00-00 00:00:00')?$this->datetimeFormatAfterFind($val[$this->name]['created']):'');
                }
                if (isset($val[$this->name]['modified'])) {
                    $results[$key][$this->name]['_modified'] = $val[$this->name]['modified'];
                    $results[$key][$this->name]['modified'] = (($val[$this->name]['modified']<>'0000-00-00 00:00:00')?$this->datetimeFormatAfterFind($val[$this->name]['modified']):'');
                }
            }
    	}
    	return $results;
    }

     
    function datetimeFormatAfterFind($datetimeString) {
    	return date('d/m/Y - H:i:s', strtotime($datetimeString));
    }
    
    public function getPathFile($field=null){
        if ($field) {
            return $this->Behaviors->Upload->settings[$this->alias][$field]['path'];
        } else {
            throw new NotFoundException(__('Campo inv�lido para gerar caminho da imagem.'));
        }
    }
    
    public function getNewName($filename = ''){
        return uniqid();
    }
    
    
    public function arrFoto($results=array(), $str_model='', $str_campo='', $str_foto='img/photo2.png', $w=30, $h=30){
        if ($str_foto==null) { $str_foto = 'img/photo2.png'; }
        if ((count($results)>0) && (!empty($str_model)) && (!empty($str_campo))) {            
            $view = new View(null);
            $Timthumb = $view->loadHelper('Timthumb.Timthumb');
            $str_name = '_' .ucwords(strtolower($str_campo));
            

            if (isset($results[$str_campo])) {
                $img = (($results[$str_campo]<>'')?'files/'.Inflector::underscore($str_model) .'/'.$str_campo.'/'.$results[$str_campo]:$str_foto);
                
                $folder = explode('/', $img);
                $filename = array_pop($folder);
                $folder = implode('/', $folder).'/';
                
                $arr_file = explode('.', $filename);
                $extension = array_pop($arr_file);
                $file = ($results[$str_campo]<>'') ? implode('.', $arr_file) : '';
                $filename = ($file ? $file.'.'.$extension : '');
    
                $_full_path = (($filename)?$folder.$filename:$img);
                $img_f = Router::url('/', true) . $_full_path;                            
                $img = '/'.$img;
                
                $arr_retorno = array(
                    'path' => Router::url('/', true),
                    'folder' => $folder,
                    'file' => $file,
                    'extension' => $extension,
                    'filename' => $filename,
                    'img' => $img,
                    'img_path' => $img_f,
                    'img_full' => '<img src="'. $img_f .'" alt="img" style="width:'.$w.'px; height:'.$h.'px;" />',
                    'img_timthumb' => ($Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb'))),
                    'fancybox' => '<a href="'. Router::url('/', true).$img .'" class="fancybox">' . ($Timthumb->image( $img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb'))).'</a>'
                );
                $results[$str_name] = $arr_retorno;
            } else {
                foreach($results as $key=>$Obj) {
                    $img = $str_foto;
                       
                    $folder = explode('/', $img);
                    $filename = array_pop($folder);
                    $folder = implode('/', $folder).'/';
                    
                    $arr_file = explode('.', $filename);
                    $extension = array_pop($arr_file);
                    $file = '';
                    $filename = ($file ? $file.'.'.$extension : '');
                                        
                    $_full_path = (($filename)?$folder.$filename:$img);
                    $img_f = Router::url('/', true) . $_full_path;                            
                    $img = '/'.$img;  
                    
                    $arr_retorno = array(
                        'path' => Router::url('/', true),
                        'folder' => $folder,
                        'file' => $file,
                        'extension' => $extension,
                        'filename' => $filename,
                        'img' => $img,
                        'img_path' => $img_f,
                        'img_full' => '<img src="'. $img_f .'" alt="img" style="width:'.$w.'px; height:'.$h.'px;" />',
                        'img_timthumb' => ($Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))),
                        'fancybox' => '<a href="'. $img_f .'" class="fancybox">' . ($Timthumb->image( $img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))).'</a>'
                    );
                        
                    if (isset($Obj[$str_model])) {
                        if ((is_array($Obj[$str_model])) && (array_key_exists($str_campo, $Obj[$str_model]))) {
                            $img = (($Obj[$str_model][$str_campo])?'files/'.Inflector::underscore($str_model) .'/'.$str_campo.'/'.$Obj[$str_model][$str_campo]:$str_foto);
                            
                            $folder = explode('/', $img);
                            $filename = array_pop($folder);
                            $folder = implode('/', $folder).'/';
                            
                            $arr_file = explode('.', $filename);
                            $extension = array_pop($arr_file);
                            $file = ($Obj[$str_model][$str_campo]<>'') ? implode('.', $arr_file) : '';
                            $filename = ($file ? $file.'.'.$extension : '');
                            
                            $_full_path = (($filename)?$folder.$filename:$img);
                            $img_f = Router::url('/', true) . $_full_path;                            
                            $img = '/'.$img;
                            
                            $arr_retorno = array(
                                'path' => Router::url('/', true),
                                'folder' => $folder,
                                'file' => $file,
                                'extension' => $extension,
                                'filename' => $filename,
                                'img' => $img,
                                'img_path' => $img_f,
                                'img_full' => '<img src="'. $img_f .'" alt="img" style="width:'.$w.'px; height:'.$h.'px;" />',
                                'img_timthumb' => ($Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))),
                                'fancybox' => '<a href="'. $img_f .'" class="fancybox">' . ($Timthumb->image( $img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))).'</a>'
                            );
                            
                            $results[$key][$str_model][$str_name] = $arr_retorno;  
                        }
                    } else if (isset($Obj['Parent'.$str_model])) {
                        if ((is_array($Obj['Parent'.$str_model])) && (array_key_exists($str_campo, $Obj['Parent'.$str_model]))) {
                            $img = (($Obj['Parent'.$str_model][$str_campo])?'files/'.Inflector::underscore($str_model) .'/'.$str_campo.'/'.$Obj['Parent'.$str_model][$str_campo]:$str_foto);
                            
                            $folder = explode('/', $img);
                            $filename = array_pop($folder);
                            $folder = implode('/', $folder).'/';
                            
                            $arr_file = explode('.', $filename);
                            $extension = array_pop($arr_file);
                            $file = ($Obj['Parent'.$str_model][$str_campo]<>'') ? implode('.', $arr_file) : '';
                            $filename = ($file ? $file.'.'.$extension : '');
                            
                            $_full_path = $folder . $filename;
                            $img_f = Router::url('/', true) . $_full_path;                            
                            $img = '/'.$img;                            
                            
                            $arr_retorno = array(
                                'path' => Router::url('/', true),
                                'folder' => $folder,
                                'file' => $file,
                                'extension' => $extension,
                                'filename' => $filename,
                                'img' => $img,
                                'img_path' => $img_f,
                                'img_full' => '<img src="'. $img_f .'" alt="img" style="width:'.$w.'px; height:'.$h.'px;" />',
                                'img_timthumb' => ($Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))),
                                'fancybox' => '<a href="'. $img_f .'" class="fancybox">' . ($Timthumb->image( $img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))).'</a>'
                            );
                            $results[$key]['Parent'.$str_model][$str_name] = $arr_retorno;  
                        }
                                          
                    } else if (is_array($Obj) && (array_key_exists($str_campo, $Obj))) {
                        $img = (($Obj[$str_campo])?'files/'.Inflector::underscore($str_model) .'/'.$str_campo.'/'.$Obj[$str_campo]:$str_foto);
                            
                        $folder = explode('/', $img);
                        $filename = array_pop($folder);
                        $folder = implode('/', $folder).'/';
                        
                        $arr_file = explode('.', $filename);
                        $extension = array_pop($arr_file);
                        $file = ($Obj[$str_campo]<>'') ? implode('.', $arr_file) : '';
                        $filename = ($file ? $file.'.'.$extension : '');
                        
                        $_full_path = (($filename)?$folder.$filename:$img);
                        $img_f = Router::url('/', true) . $_full_path;                            
                        $img = '/'.$img;
                        
                        $arr_retorno = array(
                            'path' => Router::url('/', true),
                            'folder' => $folder,
                            'file' => $file,
                            'extension' => $extension,
                            'filename' => $filename,
                            'img' => $img,
                            'img_path' => $img_f,
                            'img_full' => '<img src="'. $img_f .'" alt="img" style="width:'.$w.'px; height:'.$h.'px;" />',
                            'img_timthumb' => ($Timthumb->image($img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))),
                            'fancybox' => '<a href="'. $img_f .'" class="fancybox">' . ($Timthumb->image( $img, array('width' => $w, 'height' => $h, 'zoom_crop'=> 2), array('class'=>'thumb img-fluid'))).'</a>'
                        );

                        $results[$key][$str_name] = $arr_retorno;

                    } else  {
                        //$results[$str_name] = $arr_retorno;
                    }
                }      
            }
        }
        
        return $results;
    }
    
}