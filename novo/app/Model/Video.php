<?php
App::uses('AppModel', 'Model');
class Video extends AppModel {

	public $displayField = 'titulo';	
    public $actsAs = array(	
        'Upload.Upload' => array(
            'foto' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}foto{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
			),
        )   
	);


	public $validate = array(
		'categoria_video_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'titulo' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'descricao' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'youtube' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'N�o pode ser vazio.',
			),
		),
		'foto' => array(
			'extension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png'), false),
				'message' => 'Extens�o inv�lida',
				'allowEmpty' => true
			),
		),
	);
	
	public $belongsTo = array(
		'CategoriaVideo' => array(
			'className' => 'CategoriaVideo',
			'foreignKey' => 'categoria_video_id',
		)
	);

    function afterFind($results, $primary=false) {
		$results = $this->arrFoto($results, $this->alias, 'foto', 'img/placeholder.jpg');

    	foreach ($results as $key => $val) {
            if (isset($val['Video'])) {                
                $results[$key]['Video']['_url'] = Router::url( array('admin'=>false, 'controller'=>'videos', 'action'=>'assistir', $val['Video']['id'], slugURL($val['Video']['titulo']) ), true );
            }
        }
    	return $results;
    }
}
