<?php
App::uses('AppModel', 'Model');
class Upload extends AppModel {

	public $displayField = 'arquivo';
    
    public $actsAs = array(
        'Upload.Upload' => array(
            'arquivo' => array(
                'path'=>'{ROOT}webroot{DS}files{DS}{model}{DS}arquivo{DS}',
                'pathMethod'=>'flat',
                'customName' => '{!getNewName}'
            ),
        )      
    );
    
	public $validate = array(
		'arquivo' => array(
			'isValidSize' => array(
                'rule' => array('isBelowMaxSize', 1000000),
                'message' => 'Arquivo muito grande.'
			),            
			'isValidExtension' => array(
				'rule' => array('isValidExtension', array('jpg', 'gif', 'png')),
                'message' => 'Extens�o inv�lida.',
                'allowEmpty' => false,
			),
		),
	);
        
    public function afterFind( $results, $primary ) {
        parent::afterFind($results, $primary);
        $results = $this->arrFoto($results, 'Upload', 'arquivo');
        return $results;
    }     
}
