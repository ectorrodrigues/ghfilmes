<?php
App::uses('AppModel', 'Model');
class Tipo extends AppModel {

	public $displayField = 'titulo';
	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
	);

	public $hasMany = array(
		'Conteudo' => array(
			'className' => 'Conteudo',
			'foreignKey' => 'tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
