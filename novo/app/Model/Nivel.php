<?php
App::uses('AppModel', 'Model');
class Nivel extends AppModel {
    
	public $displayField = 'titulo';
	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'N�o pode ser vazio.',
			),
		),
	);
}
