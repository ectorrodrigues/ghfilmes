<?php
App::uses('AppModel', 'Model');
class Tamanho extends AppModel {

	public $displayField = 'titulo';
	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'N�o pode ser vazio.',
				//'allowEmpty' => false,
				//'required' => false,
			),
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Banner' => array(
			'className' => 'Banner',
			'foreignKey' => 'tamanho_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
