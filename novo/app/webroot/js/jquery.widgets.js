var widget_ajax;
(function( $ ) {
    
    /** Filter list */
    var timer_filter;
    $.widget( "ui.inputF", {
            _create: function() {
                var el = this.element;
                this.options.altField = el;
                var lista = this.lista = $('<div class="lista_option"><ul class="col-lg-12"></ul></div>').insertAfter( el ).hide();
                
                var callback = ((typeof el.data('callback') === 'undefined') ? '' : el.data('callback'));
                if (callback) {
                    this.lista.on('click', 'a.bt_action', function(event) {                    
                        event.preventDefault();
                        if (typeof window[callback] == "function") { window[callback]( $(this) ); }
                        setTimeout(function() { 
                            lista.hide().children('ul').html('');
                        }, 100);
                    });
                }
                
                el.focus(function(){
                    if ((el.val()!='')&&(lista.find('li').length>0)) {$(this).next('div.lista_option').show();}
                                        
                }).blur(function(){
                    lista.hide();
                    
                }).keyup(function() {
                    var obj_this = $(this);
                    
                    var $str_search = obj_this.val().trim();
                    var $str_action = obj_this.data('action');
                    var $str_campo = obj_this.data('field');
                    var $content = ((typeof el.data('content') === 'undefined') ? '' : el.data('content'));
                    
                    var min_length = ((typeof obj_this.data('min-length') === 'undefined') ? 5 : obj_this.data('min-length'));
                    
                    clearTimeout(timer_filter);
                    timer_filter = setTimeout(function() {
                        
                        lista.children('ul').html('');
                        
                        if (($str_search.length>=min_length)&&($str_action!='')&&($str_campo!='')) {                            
                            if (typeof widget_ajax !== 'undefined') { widget_ajax.abort(); }        
                                                
                            widget_ajax = $.ajax({
                        		url: URL_BASE + $str_action +"/ajax",
                        		data: {
                                    'tipo_acao': 'busca_registro',
                                    'campo': $str_campo,
                                    'busca' : $str_search
                                },
                        		async: true,
                                type: 'POST',
                        		dataType: "json",
                                beforeSend: function() { lista.show().children('ul').html('Pesquisando...'); },
                                success: function( data ) {
                                    html = '';
                                    if (data.status) {
                                        if (data.retorno.length>0) {
                                            if (($content)&&(typeof window[$content] == "function")) {
                                                html = window[$content]( data.retorno, $str_campo );
                                            } else {
                                                html = '<li>Falta gerador de conte�do.<li>';
                                            }
                                        } else {
                                            html = '<li>Nenhum registro encontrado.<li>';
                                        }
                                    }
                                    lista.children('ul').html( html );                                
                        		}
                        	});
                        }
                    }, 500); 
                }); // Fim el.keyup
                
            },
            destroy: function() {
                this.lista.remove();
                this.lista.off('click', 'a.bt_action', function() {});
                $.Widget.prototype.destroy.call( this );
            }
    });
    
    $.widget( "ui.postAjax", {
            _create: function() {
                var el = this.element;
                
                
                
                /*
                $.ajax({
            		url: URL_BASE + $str_action +"/ajax",
            		data: {
                        'tipo_acao': 'busca_registro',
                        'campo': $str_campo,
                        'busca' : $str_search
                    },
            		async: true,
                    type: 'POST',
            		dataType: "json",
                    beforeSend: function() { lista.show().children('ul').html('Pesquisando...'); },
                    success: function( data ) {
                        html = '';
                        if ((data.status) && (data.retorno.length>0)) {                                        
                            if (($content)&&(typeof window[$content] == "function")) {
                                html = window[$content]( data.retorno, $str_campo );
                            } else {
                                html = '<li>Falta gerador de conte�do.<li>';
                            }
                            lista.show().children('ul').html( html );
                        }                                    
            		}
            	});
                */
                
            },
            destroy: function() {
                this.lista.remove();
                this.lista.off('click', 'a.bt_action', function() {});
                $.Widget.prototype.destroy.call( this );
            }
    });
    
    /** Datepicker + Mask */
    $.widget( "ui.dp", {
            _create: function() {
                var el = this.element.hide();
                this.options.altField = el;
                var input = this.input = $('<input type="text">').insertBefore( el );
                input.addClass( el.attr('class') );
                input.focusout(function(){
                    if(input.val() == ''){
                        el.val('');
                    } else if (!validateDate(input.val())) {
                        input.val('');
                        el.val('');
                    }
                });
                if ( jQuery().uniform ) { $.uniform.update(); }
                input.datepicker(this.options).mask('99/99/9999');
                this.setDate(el.val());
            },
            setDate: function( date ) {
                if(convertDate(date) != null){
                    this.input.datepicker('setDate', convertDate(date));
                } else {
                    this.input.datepicker('setDate', '');
                }
            },
            destroy: function() {
                this.input.remove();
                this.element.show();
                $.Widget.prototype.destroy.call( this );
            }
    });
    
    /** Moeda */
    $.widget( "ui.moeda", {
            _create: function() {
                var el = this.element;                
                el.priceFormat(this.options)
                .change(function(){
                    if (($(this).val()=='R$ 0,00')||($(this).val()=='0,00')) { $(this).val(''); }
                }).blur(function(){
                    if (($(this).val()=='R$ 0,00')||($(this).val()=='0,00')) { $(this).val(''); }
                });
            },
            destroy: function() {
                $.Widget.prototype.destroy.call( this );
            }
    });
    
    var convertDate = function(date){
      if(typeof(date) != 'undefined' && date != null && date != ''){
        var parts = date.match(/(\d+)/g);
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
      } else {
        return null;
      }
    }
    
    var validateDate = function(date){
        var comp = date.split('/');
        var d = parseInt(comp[0], 10);
        var m = parseInt(comp[1], 10);
        var y = parseInt(comp[2], 10);
        var date = new Date(y,m-1,d);        
        return (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d);
    }
    
    
    // Valida extens�o
    $.fn.hasExtension = function(exts) {
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test( $(this).val() );
    }
       
})( jQuery );