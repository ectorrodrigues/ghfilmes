var validaCampo = function (elemento, boo, msg) {
    if (!msg) { msg = 'Campo Obrigat�rio'; }

    elemento.closest('div.form-group').removeClass('error').find('label.help-inline').remove();
    if (!boo) {
        if (!elemento.hasClass('error')) {
            elemento
                .after('<label class="help-inline error">' + msg + '</label>')
                .closest('div.form-group').addClass('error');
        }
    }
};

$(document).ready(function(){
    
    $("a.fancybox").fancybox();
    /** Bot�es */
    $( 'a#bt_atualizar' ).click(function(){
       window.location = window.location;
    });
    $( 'a.bt_voltar' ).click(function(){
        if ($(this).attr('location')=='') {
            window.history.back();
        } else {
            window.location = $(this).attr('location');
        }
    });
    $( 'a.bt_salvar' ).click(function(){
        $('#after_action').val( $(this).attr('id') );
        $(this).closest('form').submit();
    });
    
    $('.bt_submit').click(function(){
        $(this).closest('form').submit();
    });
    
    /** Formul�rio */
    $('form').submit(function() {
        validate = validaFormulario( $(this) );
        if (!validate) { dialog('Erro', 'Favor verificar os campos inv�lidos.') };
        
        return validate;
    });
    
    $('input.integer').on('keypress', function (evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        var arr_allow = new Array(8, 9, 13, 27, 35, 36, 37, 38, 39, 39, 116);
        if (jQuery.inArray(key, arr_allow) < 0) {
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    });
    $( 'input[type="number"]' ).keypress(function(evt){
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        var arr_allow = new Array(8, 13, 27, 35, 36, 37, 39, 46);
        if ( jQuery.inArray( key, arr_allow ) < 0  ) {
            key = String.fromCharCode( key );
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    });
    $( 'input[required="required"], textarea[required="required"]' ).change(function() {
        if ($(this).val()=='') {
            validaCampo( $(this), false  );                
        } else {
            validaCampo( $(this), true  );
        }
    });
    $( 'input.email' ).change(function() {
        if (!validaEmail($(this).val())){
            validaCampo( $(this), false, 'E-mail inv�lido.'  );                
        } else {
            validaCampo( $(this), true  );
        }
    });
    
    
    $( "input.cpf" )
        .removeAttr('maxlength')
        .mask("999.999.999-99")
        .change(function(){
          if (!validaCPF( $(this).val() )) {
            validaCampo( $(this), false, 'CPF inv�lido.' );
          } else {
            validaCampo( $(this), true  );
          }
    });
    $( "input.cnpj" )
        .removeAttr('maxlength')
        .mask("99.999.999/9999-99")
        .change(function(){
          if (!validaCNPJ( $(this).val() )) {
            validaCampo( $(this), false, 'CNPJ inv�lido.' );
          } else {
            validaCampo( $(this), true  );
          }
    });
    $( "input.cep" )
        .removeAttr('maxlength')
        .mask("99999-999")
        .change(function(){
            if ($(this).val() != "") {
                attr = $(this).attr('name');
                if (typeof attr !== typeof undefined && attr !== false) {
                    var name_search = $(this).attr('name').replace('[cep]','');
                    endereco = buscaEndereco( $(this).val() );
                    if (endereco) {
                        $(this).closest('form').find( 'input[name="'+ name_search + '[endereco]"]' ).val( endereco.logradouro );
                        $(this).closest('form').find( 'input[name="'+ name_search + '[bairro]"]' ).val( endereco.bairro );
                        $(this).closest('form').find( 'input[name="'+ name_search + '[cidade]"]' ).val( endereco.cidade );
                        $(this).closest('form').find( 'select[name="'+ name_search + '[estado]"]' ).val( endereco.uf );
                    } else {
                        dialog('Erro', 'N�o foi poss�vel localizar este CEP em nosso sistema.');
                    }

                }
    		}	
        });
    $("input.telefone")
        .removeAttr('maxlength')
        .mask("(99) 9999-9999?9")
        .change(function () { adjustMaskPhone($(this)); })
        .each(function () { adjustMaskPhone($(this)); });
    
    $('a.confirmLink').click(function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        
        $('#dialog_system_confirm').html( 'Deseja confirmar esta opera��o?' );
    	$('#dialog_system_confirm').dialog({
    		autoOpen: false,
    		title: "Aten��o",
    		bgiframe: true,
    		modal: true,
    		resizable: false,
    		draggable: false,
    		buttons: {
    			'Ok': function() {
    			     window.location = url;
    				$(this).dialog('close');
    			},
                'Cancelar': function() {
    				$(this).dialog('close');
    			}
    		}
    	 });
    	 $('#dialog_system_confirm').dialog('open');
    });    
});