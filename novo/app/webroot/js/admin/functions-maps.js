var map; var geocoder; var marker; var infowindow;
var arr_marker = new Array(); var arr_infowindow = new Array();

/**
KEY:
js?key=
*/

/** Marcador principal */
function DistanceWidget(map, nova, name_input, callback, drag, circleSize) {
    drag = ((typeof drag === 'undefined') ? true : drag);
    this.set('map', map);
    this.set('position', map.getCenter());
    //this.set('callback', callback);
    
    marker = new google.maps.Marker({ draggable: drag });
    marker.bindTo('map', this);
    marker.bindTo('position', this);
    
    google.maps.event.addListener(marker, 'dragend', function() {
        mudarMarker(  marker.getPosition(), null, nova, name_input, callback);
    });
    
    var radiusWidget = new RadiusWidget(circleSize);
    radiusWidget.bindTo('map', this);
    radiusWidget.bindTo('center', this, 'position');
    this.bindTo('distance', radiusWidget);
    this.bindTo('bounds', radiusWidget);
}
DistanceWidget.prototype = new google.maps.MVCObject();

/** C�rculo */
function RadiusWidget(circleSize) {
    circleSize = ((typeof circleSize === 'undefined') ? 0.09 : circleSize);
            
    var circle = new google.maps.Circle({
      strokeWeight: 2,
      fillOpacity: 0,
      clickable: false,
    });
    this.set('distance', circleSize);
    this.bindTo('bounds', circle);
    circle.bindTo('center', this);
    circle.bindTo('map', this);
    circle.bindTo('radius', this);
}
RadiusWidget.prototype = new google.maps.MVCObject();         
 
RadiusWidget.prototype.distance_changed = function() {
    this.set('radius', this.get('distance') * 1000);
};

function mudarMarker(location, id, nova, name_input, callback) {
    id = (((typeof id === 'undefined')||(id==null)) ? false : id);
    nova = ((typeof nova === 'undefined') ? true : nova);
    name_input = (((typeof name_input === 'undefined')||(name_input == null)) ? 'data[Coordenada]' : name_input);
    callback = ((typeof callback === 'undefined') ? false : callback);
            
    marker.setPosition(location);
    map.panTo(location);
    
    $('input[name="'+name_input+'[latitude]"]').val( location.lat() );
    $('input[name="'+name_input+'[longitude]"]').val( location.lng() );
        
    setTimeout(function() {
        if (callback && (typeof callback == "function")) { callback( location ); }
    }, 500);
}

/**
function moverMarker(id, lat, lng, name_input) {
    id = ((typeof id === 'undefined') ? false : id);
    lat = ((typeof lat === 'undefined') ? false : lat);
    lng = ((typeof lng === 'undefined') ? false : lng);
    
    if (id) {
        $.ajax({
            url: URL_BASE + "coordenadas/ajax",
            data: {
                'tipo_acao': 'busca_marcador',
                'id' : id
            },
            async: false,
            type: 'POST',
            beforeSend: function() { dialogLoading(); },
            complete: function() { closeLoading(); },
            dataType: "json",
            success: function(data) {
                if (data.status) {
                    mudarMarker( new google.maps.LatLng(data.marcador.Coordenada.latitude, data.marcador.Coordenada.longitude), id, false, name_input);
                    $('#bt_complemento_' + data.marcador.Coordenada.complemento_id).trigger('click');
                }
            }
        });
    } else if (lat && lng) {
        alert('a');
    }
}*/

function fecharInfoWindow() {
    $.each(arr_infowindow, function(key, obj) {
        obj.close();
    });
}

function limparMarcadores() {
    $.each(arr_marker, function(key, obj) {
        obj.setMap(null);
        arr_infowindow[key].close();
    });
    arr_marker= [];
    arr_infowindow = [];
}

function PosicionarEndereco( endereco, zoom, name_input ) {
    if (endereco != '') {
        if (geocoder) {
            geocoder.geocode( { 'address': endereco }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    mudarMarker( results[0].geometry.location, null, true, name_input );
                    
                    if (zoom) {
                        map.setZoom( zoom );
                    }
                } else {
                    alert("N�o encontramos a localiza��o no mapa: " + status);
                }
            });
        } 
        
    }
}