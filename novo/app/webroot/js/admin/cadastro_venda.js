var _div_profile = $('div.profile-page');
var _div_form_cliente = $('#div_clientes');

var slt_categoria = $('select#categoria_id');
var slt_produto = $('select#produto_id');

var table_produtos = $('#lista_produtos');
var key_list_product = parseInt(table_produtos.find('tbody tr').length);
   
function callbackContentUsuario( json ) {
    html = '';
    limpaObjArray('usuario_search');
    $.each(json, function(key, objeto) {
        objArray('usuario_search', objeto.Usuario.id, objeto.Usuario );
        
        html += '<li id="cliente_'+ objeto.Usuario.id +'"> \
            <a href="javascript:;" class="bt_action"> \
                '+ stripslashes( objeto.Usuario._Foto.img_timthumb ) +' \
                <span><strong>'+ objeto.Usuario['nome'] +'</strong></span><br /> \
                <span>'+ concatWord(new Array(objeto.Usuario['telefone'], objeto.Usuario['telefone_2']), ' - ') +'</span> <br /> \
                <span>'+ objeto.Usuario['cpf'] +'</span> \
            </a> \
        </li>';
    });
    return html;
}
function callbackFunctionUsuarioSearch( element ) {
    var id = parseInt( element.parent('li').attr('id').replace('cliente_', '') );
    
    if (id!=0) {
        var obj = objArray('usuario_search', id);
        preencheUsuario( obj );
    }
}

function preencheUsuario(obj) {
    var foto = '';
    
    if (typeof obj !== 'undefined') {
        foto = '<div class="pull-right">'+obj._Foto.img_timthumb+'</div>';
        _div_profile.removeClass('hidden');
        if (_div_form_cliente.css('display')=='block') {  $('a#btn_change_clientes').trigger('click');  }
        
    } else {
        var obj = {
            'id':'', 'nome':'', 'email':'',
            'cpf':'', 'telefone':'', 'telefone_2':'',
            'cep': '', 'endereco': '', 'numero': '', 
            'cep': '', 'endereco': '', 'numero': '', 
            'bairro':'', 'cidade':'', 'estado':''
            
        };
        
        _div_profile.addClass('hidden');
        if (_div_form_cliente.css('display')!='block') {  $('a#btn_change_clientes').trigger('click');  }
    }
    
    $('input#UsuarioId').val( obj.id );
    $('input#UsuarioNome').val( obj.nome ); validaCampo($('input#UsuarioNome'), true);
    $('input#UsuarioEmail').val( obj.email ); validaCampo($('input#UsuarioEmail'), true);
    $('input#UsuarioCpf').val( obj.cpf ); validaCampo($('input#UsuarioCpf'), true);
    $('input#UsuarioTelefone').val( obj.telefone ); validaCampo($('input#UsuarioTelefone'), true);
    $('input#UsuarioTelefone2').val( obj.telefone_2 );
    $('input#UsuarioCep').val(obj.cep );
    $('input#UsuarioEndereco').val(obj.endereco );
    $('input#UsuarioNumero').val(obj.numero );
    $('input#UsuarioBairro').val(obj.bairro );
    $('input#UsuarioCidade').val(obj.cidade );
    $('input#UsuarioEstado').val(obj.estado );
    
    /* Html */
    _div_profile.find('#profile-avatar').html( foto );
    _div_profile.find('.profile-name').html( obj.nome );
    _div_profile.find('.profile-email').html( obj.email );
    _div_profile.find('.profile-phone').html( obj.telefone );    
}


function calcular() {
    var total = 0;
    $.each(table_produtos.find('tbody tr'), function (key, obj) {
        total_obj = moeda2float($(this).find('input[name$="[qtde]"]').val()) * moeda2float($(this).find('input[name$="[valor]"]').val());
        total_obj = (isNaN(total_obj) ? 0 : total_obj);

        total += total_obj;

        $(this).find('td.valor_total_produto').html('R$ ' + float2moeda(total_obj));
    });

    total += moeda2float($('#PedidoValorFrete').val());
    $('#PedidoValorTotal').val('R$ ' + float2moeda(total));
}

jQuery(document).ready(function() {
    CKEDITOR.disableAutoInline = true;
    $('textarea#PedidoObservacao').ckeditor();


    $('#PedidoAdminCadastroForm').submit(function(){
        if (table_produtos.find('tbody tr').length==0) {
            dialog('Erro', 'Necess�rio acrescentar produtos.');
            return false;
        }
    });

    $('a#btn_change_customer').click(function() {
        preencheUsuario();
    });    

    calcular();
    table_produtos.on('change', 'input[name$="[qtde]"], input[name$="[valor]"]', function () {
        calcular();
    });
    $('#PedidoValorFrete').change(function () { calcular(); });

    table_produtos.on('click', 'a.btn_remove_produto', function () {
        console.log('a');
        var $this = $(this).closest('tr');
        confirmLink(function () {
            $this.fadeOut(function () {
                $(this).remove();
                calcular();
            });
        });
    });

    $('a.btn_produto').click(function () {
        var row = $(this).closest('tr');

        var row_id = parseInt(row.attr('id').replace('row_', ''));
        var nm_produto = 'Novo';

        $("#dialog_form_produto").dialog({
            autoOpen: false,
            title: 'Produto: ' + nm_produto,
            bgiframe: true,
            modal: true,
            resizable: false,
            draggable: false,
            width: 500,
            open: function () {
                var $dialog = $(this);
                $dialog.find('select[name="data[categoria_id]"]').val('').trigger('change.select2');
                $dialog.find('select[name="data[produto_id]"]').html('').val('').trigger('change.select2');
            },
            buttons: {
                Ok: function () {
                    var $dialog = $(this);

                    var prod_id = $dialog.find('select[name="data[produto_id]"]').val();
                    
                    if ((prod_id != null)  ) {
                        var Produto = objArray('produtos', prod_id).Produto;
                        console.log( Produto );

                        if (table_produtos.find('input[name$="[produto_id]"][value="' + prod_id + '"]').length > 0) {
                            dialog('Erro', 'Este produto j� est� na lista. Altere a quantidade');

                        } else { // Produto n�o est� na lista
                            var $_html = '<tr id="row_#key#">\
                                            <td>'+ Produto.titulo + '</td>\
                                            <td><input type="text" class="form-control integer w-50" name="data[PedidoProduto][#key#][qtde]" value="1" /></td>\
                                            <td><input type="text" class="form-control moeda w-100" name="data[PedidoProduto][#key#][valor]" value="'+ ((Produto.valor)?Produto.valor:0) + '" /></td>\
                                            <td class="ta-c valor_total_produto"></td>\
                                            <td class="ta-c auto-width">\
                                                <input type="hidden" name="data[PedidoProduto][#key#][id]" value="" />\
                                                <input type="hidden" name="data[PedidoProduto][#key#][produto_id]" value="'+ prod_id + '" />\
                                                <a class="btn btn-gradient btn-default tip btn_remove_produto" title="Excluir Produto"><i class="fa fa-trash-o"></i></a>\
                                            </td>\
                                        </tr>';

                            if (row_id == 0) {
                                key_list_product++;
                                var $newObj = $($_html.replace(/#key#/g, key_list_product));
                                table_produtos
                                    .find('tbody')
                                    .append($newObj);

                                $newObj.find('input.moeda').moeda({
                                    prefix: 'R$ ',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    clearPrefix: true
                                });
                                $newObj.on('keypress', function (evt) {
                                    var theEvent = evt || window.event;
                                    var key = theEvent.keyCode || theEvent.which;
                                    var arr_allow = new Array(8, 9, 13, 27, 35, 36, 37, 38, 39, 39, 116);
                                    if (jQuery.inArray(key, arr_allow) < 0) {
                                        key = String.fromCharCode(key);
                                        var regex = /[0-9]/;
                                        if (!regex.test(key)) {
                                            theEvent.returnValue = false;
                                            if (theEvent.preventDefault) theEvent.preventDefault();
                                        }
                                    }
                                });
                            }
                        }
                        calcular();
                        $dialog.dialog('close');

                    } else {
                        dialog('Erro', 'Necess�rio escolher produto e tamanho.');
                    }
                },
                Fechar: function () { $(this).dialog('close'); }
            }
        });
        $("#dialog_form_produto").dialog('open');
    });

    slt_categoria.on('change', function () {
        var categoria_id = $(this).val();
        arr_produtos = objArray('produtos');

        _html_produtos = '';
        if (categoria_id!='') {
            $.each(arr_produtos, function(key, obj){
                if (obj.Produto.categoria_id == categoria_id) {
                    _html_produtos += '<option value="' + obj.Produto.id + '">' + obj.Produto.titulo + '</option>';
                }
            });
        }
        slt_produto.html(_html_produtos).val('').trigger('change.select2');
    });
    slt_produto.on('change', function () {
        var produto_id = $(this).val();
        
        _html_tamanhos = '';
        if (categoria_id != '') {
            var obj_produto = objArray('produtos', produto_id);
            var arr_tamanho = obj_produto.TamanhoFrete;
    
            _html_tamanhos = '<option>Selecione</option>';
            $.each(arr_tamanho, function (key, obj) {
                _html_tamanhos += '<option value="' + obj.id + '">' + obj.titulo + '</option>';
            });
        }
    });

    
    
    $('form#VendaAdminCadastroForm').submit(function(){
        var boo = validaFormulario( _div_form_cliente );
        if (!boo &&  _div_form_cliente.css('display')!='block') {  $('a#btn_change_clientes').trigger('click');  }
    });
    
});