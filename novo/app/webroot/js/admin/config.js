$(document).ready(function(){
    // Confirma sair da p�gina
    $(window).bind("beforeunload",function(event) {
        if ($('form[editado]').length>0) return "As altera��es n�o foram gravadas. Deseja sair mesmo assim?";
    });
    
    // Plugins
    $(".tip").tooltip();
    $("a.fancybox").fancybox();
    //$('select.select2').select2();
        
    $( '.bt_list_table > button.dropdown-toggle, .bt_list_table > ul.dropdown-menu' ).hover(function(){
        $(this).parent('div.btn-group').addClass('open');
    }, function(){
        $(this).parent('div.btn-group').removeClass('open');
    });

    $('.btn_toogle').click(function () {
        var $this = $(this);
        var $icon = $this.children('i');
        var $div = $('div#' + $this.attr('data-element-toogle'));

        $div.fadeToggle('fast', function () { $icon.removeClass('fa-arrow-down fa-arrow-up').addClass('fa-arrow-' + (($div.css('display') == 'block') ? 'up' : 'down')); });
    });
        

    $('input.url').keyup(function (event) {
        $(this).val($(this).val().replace(/^https?\:\/\//i, ''));
    });
    
    $('input.timepicker')
        .removeAttr('maxlength')
        .mask("99/99/9999 99:99")
        .datetimepicker({
            step: 10,
            format:	'd/m/Y H:i',
        	formatDate:	'd/m/Y',
            lang: 'pt',inline:true,
            datepicker: true
        });
                
    $( 'input.datepicker' ).dp({
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:c",
        dateFormat: 'dd/mm/yy', 
        altFormat: 'yy-mm-dd'
    });           
    $( 'input.moeda' ).moeda({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.',
        clearPrefix: true
    });
    $( 'input.double' ).moeda({
        prefix: '',
        centsLimit: 3,
        centsSeparator: '.',
        thousandsSeparator: '',
        clearPrefix: true
    });
    
    $( 'input.input_filter' ).inputF();
    
        
    
    
    /** Bot�es */
    $( 'a#bt_atualizar' ).click(function(){
       window.location = window.location;
    });
    $( 'a.bt_voltar' ).click(function(){
        if ($(this).attr('location')=='') {
            window.history.back();
        } else {
            window.location = $(this).attr('location');
        }
    });
    $( 'a.bt_salvar, input[type="submit"]' ).click(function(){
        $(this).closest('form').removeAttr('editado').find('input[name="after_action"]').val( $(this).attr('id') );
        $(this).closest('form').submit();
    });
    
    /** Formul�rio */
    $('form').submit(function() {
        var validate = true;
        
        $(this).find('input, select').each(function(){
            if ($(this).hasClass('error')) {
                validate = false;
            }            
        });
        
        if (validate) {
            $(this).find('input[required="required"]').each(function() {
                if ($(this).val()=='') {
                    validaCampo( $(this), false  );      
                    validate = false;
                } else {
                    validaCampo( $(this), true  );
                }
            });
            
            $(this).find('div.required').find('select').each(function() {
                if ($(this).val()=='') {
                    validaCampo( $(this), false  );
                    validate = false;
                } else {
                    validaCampo( $(this), true  );
                }
            });
        }
        
        if (!validate) { dialog('Erro', 'Favor verificar os campos inv�lidos.') };
        return validate;
    }).each(function(){
        if ($(this).hasClass('verifyEditable')) {
            $(this).find('input, select, textarea').not('.not_change_form').change(function(){
                var obj_form = $(this).closest('form');
                obj_form.attr('editado', false);
                
                /** Se tiver form tiver "model" e id for <> '' remove valida��o do form */
                if (obj_form.attr('data-model')) {
                    var model = obj_form.attr('data-model');
                    id = ((typeof obj_form.find('input[name="data['+model+'][id]"]') !== 'undefined') ? obj_form.find('input[name="data['+model+'][id]"]').val() : '');
                    if (id=='') {
                        obj_form.removeAttr('editado');
                    }
                }
            });
        }
    });
    
    $('div.required').find('select').change(function() {
        if ($(this).val()=='') {
            validaCampo( $(this), false  );
            validate = false;
        } else {
            validaCampo( $(this), true  );
        }
    });

    $('input.integer').on('keypress', function (evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        var arr_allow = new Array(8, 9, 13, 27, 35, 36, 37, 38, 39, 39, 116);
        if (jQuery.inArray(key, arr_allow) < 0) {
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    });
    $('input.number').on('keypress', function (evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        var arr_allow = new Array(8, 9, 13, 27, 35, 36, 37, 38, 39, 39, 46, 116);
        if (jQuery.inArray(key, arr_allow) < 0) {
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    });
    $( 'input[required="required"], textarea[required="required"]' ).change(function() {
        if ($(this).val()=='') {
            validaCampo( $(this), false  );                
        } else {
            validaCampo( $(this), true  );
        }
    });
    $( 'input.email' ).change(function() {
        if (!validaEmail($(this).val())){
            validaCampo( $(this), false, 'E-mail inv�lido.'  );                
        } else {
            validaCampo( $(this), true  );
        }
    });
    
    $( "input.cpf" )
        .removeAttr('maxlength')
        .mask("999.999.999-99")
        .change(function(){
          if (!validaCPF( $(this).val() )) {
            validaCampo( $(this), false, 'CPF inv�lido.' );
          } else {
            validaCampo( $(this), true  );
          }
    });
    $( "input.cnpj" )
        .removeAttr('maxlength')
        .mask("99.999.999/9999-99")
        .change(function(){
            if ($(this).val()!='') {
                if (!validaCNPJ( $(this).val() )) {
                    validaCampo( $(this), false, 'CNPJ inv�lido.' );
                } else {
                    validaCampo( $(this), true  );
                }
            } else {
                validaCampo( $(this), true  );
            }
    });
    $( "input.cep" )
        .removeAttr('maxlength')
        .mask("99999-999")
        .change(function(){
            if ($(this).val() != "") {
                var name_search = $(this).attr('name').replace('[cep]','');
    			endereco = buscaEndereco( $(this).val() );
                if (endereco) {                    
                    $(this).closest('form').find( 'input[name="'+ name_search + '[endereco]"]' ).val( endereco.logradouro );
                    $(this).closest('form').find( 'input[name="'+ name_search + '[bairro]"]' ).val( endereco.bairro );
                    $(this).closest('form').find( 'input[name="'+ name_search + '[cidade]"]' ).val( endereco.cidade );
                    $(this).closest('form').find( 'select[name="'+ name_search + '[estado]"]' ).val( endereco.uf );
                } else {
                    dialog('Erro', 'N�o foi poss�vel localizar este CEP em nosso sistema.');
                }	
    		}	
        });
    $("input.telefone")
        .removeAttr('maxlength')
        .mask("(99) 9999-9999?9")
        .change(function () { adjustMaskPhone($(this)); })
        .each(function () { adjustMaskPhone($(this)); });
    
    $(document).on('click', 'a.confirmLink', function(event) {
        event.preventDefault();        
        confirmLink( $(this).attr('href') );
    });    
    $('.dropdown-menu').on('click', 'a.confirmLink', function(event) {
        event.preventDefault();        
        confirmLink( $(this).attr('href') );
    });  
});