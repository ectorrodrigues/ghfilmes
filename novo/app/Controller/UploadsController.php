<?php
App::uses('AppController', 'Controller');
class UploadsController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'uploads', 'action' => 'admin_index');
	var $component_name = 'Upload';

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Upload->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Upload.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Upload.{$name} =" => $value)); break;
            }
        }
                
   
        $limit = $this->Search->getLimit();
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Upload->recursive = 0;
		$this->set('uploads', $this->paginate());
	}


	public function admin_cadastro($id = null) { 
		$Upload = null;
		if ($id<>null) {
            if (!$this->Upload->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Upload = $this->Upload->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Upload['Upload']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Upload->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Upload;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Upload->id = $id;
		if (!$this->Upload->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Upload->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
    
    
	public function admin_upload($id = null) {
        $this->autoRender = false;
        
        $msg = '<span style="color:red; ">Nenhum arquivo enviado.</span>';
        if (isset($_FILES['upload'])) {
            $data = array(
                'Upload' => array(
                    'arquivo' => $_FILES['upload'],
                )
            );
            if ($boo = $this->Upload->save($data)) {
                $this->Upload->recursive = -1;
                $element = $this->Upload->findById( $boo['Upload']['id'] );
                
                $msg = '<div style="color: green;">Arquivo: '. $element['Upload']['_Arquivo']['img_path'] .'</div>
                        <script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$_GET['CKEditorFuncNum'].', "'. $element['Upload']['_Arquivo']['img_path'] .'", "");</script>
                ';
                
            } else {
                $arrError = $this->Upload->validationErrors['arquivo'];
                
                if (count($arrError)>0) {
                    $msg = '<span style="color:red;">'. $arrError[0] .'</span>';
                } else {
                    $msg = '<span style="color:red;">N�o foi poss�vel enviar o arquivo.</span>';
                }
            }        
        }
        echo utf8_encode($msg); die;
	}
    
}
