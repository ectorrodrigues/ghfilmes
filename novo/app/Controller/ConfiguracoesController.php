<?php
App::uses('AppController', 'Controller');
class ConfiguracoesController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'configuracoes', 'action' => 'admin_index');
	var $component_name = 'Configura��o';
    
    public function isAuthorized($user = null) {        
        $boo = parent::isAuthorized($user);
        if ($boo) {
            if ($this->Auth->user('nivel_id')==1) {
                $boo = true;
            } else {
                $boo = false;
            }
        }
        return $boo;
    }

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Configuracao->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Configuracao.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Configuracao.{$name} =" => $value)); break;
            }
        }
                
   
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Configuracao->recursive = 0;
		$this->set('configuracoes', $this->paginate());
	}


	public function admin_cadastro($id = null) { 
		$Configuracao = null;
		if ($id<>null) {
            if (!$this->Configuracao->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Configuracao = $this->Configuracao->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Configuracao['Configuracao']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Configuracao->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Configuracao;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Configuracao->id = $id;
		if (!$this->Configuracao->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Configuracao->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
