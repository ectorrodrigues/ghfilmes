<?php
App::uses('AppController', 'Controller');
class NewsletteresController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'newsletteres', 'action' => 'admin_index');
    public $uses = array('Newsletter', 'Usuario');
	var $component_name = 'Newsletter';

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Newsletter->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'email':
                    $conditions[] = array('AND' => array("Newsletter.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Newsletter.{$name} =" => $value)); break;
            }
        }                
   
        $limit = $this->Search->getLimit();
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        $newsletter = $this->paginate('Newsletter');
        
		$this->Newsletter->recursive = 0;
		$this->set('newsletteres', $newsletter);
	}


	public function admin_cadastro($id = null) { 
		$Newsletter = null;
		if ($id<>null) {
            if (!$this->Newsletter->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Newsletter = $this->Newsletter->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Newsletter['Newsletter']['email'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Newsletter->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Newsletter;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Newsletter->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
    
    
	public function admin_exportar() {	
        
	       header('Content-Type: text/html; charset=utf-8');
           
        $this->paginate = array( 'limit' => 99999999999 );  
 		$data = $this->paginate('Newsletter');
        
        /*
       
       $path = WEBROOT_DIR.DS.'files'.DS.'export.csv'; debug($path); die;
        $df = fopen( $path , 'w');
        fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
       
       die;
       */
       
		$this->response->download("export.csv");
        $this->response->type('csv');
        $this->response->charset('UTF-8');
        
		$this->set(compact('data'));
 		$this->layout = 'ajax';
	}
    
    
}
