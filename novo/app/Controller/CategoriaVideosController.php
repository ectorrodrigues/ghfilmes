<?php
App::uses('AppController', 'Controller');
class CategoriaVideosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'categoria_videos', 'action' => 'admin_index');
	var $component_name = 'Categoria Video';

	public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->CategoriaVideo->recursive = -1;
        parent::beforeFilter();
    }

    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));

		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();

		$conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("CategoriaVideo.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("CategoriaVideo.{$name} =" => $value)); break;
            }
        }


        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );

		$this->CategoriaVideo->recursive = 0;
		$this->set('categoriaVideos', $this->paginate());
	}


	public function admin_cadastro($id = null) {
		$CategoriaVideo = null;
		if ($id<>null) {
            if (!$this->CategoriaVideo->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $CategoriaVideo = $this->CategoriaVideo->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$CategoriaVideo['CategoriaVideo']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );

        // $_old_foto = (($CategoriaVideo<>null) ? $this->CategoriaVideo->getPathFile('foto') . $CategoriaVideo['CategoriaVideo']['foto'] : '');
        $_old_icone = (($CategoriaVideo<>null) ? $this->CategoriaVideo->getPathFile('icone') . $CategoriaVideo['CategoriaVideo']['icone'] : '');

		if ($this->request->is(array('post', 'put'))) {


			if ($this->CategoriaVideo->save($this->request->data)) {

				// Remove arquivos antigos
				// if ((is_file($_old_foto))&&(!empty($this->request->data['CategoriaVideo']['foto']['name']))) { unlink($_old_foto); }
				if ((is_file($_old_icone))&&(!empty($this->request->data['CategoriaVideo']['icone']['name']))) { unlink($_old_icone); }


    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');

                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}

		} else {
			$this->request->data = $CategoriaVideo;
		}
	}

	public function admin_excluir($id = null) {
		$this->CategoriaVideo->id = $id;
		if (!$this->CategoriaVideo->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->CategoriaVideo->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
