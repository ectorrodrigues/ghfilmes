<?php
App::uses('AppController', 'Controller');
class DashboardController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'dashboard', 'action' => 'admin_index');
	var $component_name = 'In�cio';

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];        
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', ($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => $this->component_name, 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    
	public function admin_index() {
        $this->set('bodyClass', 'profile-page');   
	}
    
	public function index() {
        if (!$this->Auth->loggedIn()) {
            $this->redirect($this->Auth->logout());
        }
	}
    
}
