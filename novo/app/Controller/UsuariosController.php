<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsuariosController extends AppController {

    var $base_url = array('admin' => true, 'controller' => 'usuarios', 'action' => 'admin_index');
    public $helpers = array( 'Timthumb.Timthumb', 'Breadcrumb', 'GFunction', 'CakePtbr.Estados' );
    
    var $component_name = 'Usu�rio';

    
    public function isAuthorized($user = null) {  
        $boo = parent::isAuthorized($user);
        //debug($user); die;
        if ($boo) {

            if ($user['nivel_id']==1) {
                $boo = true;
            } else {

                switch ($this->params['action']) {
                    case 'admin_perfil':
                    case 'admin_logout':
                        $boo = true; break;
                    
                    default: $boo = false; break;
                }
            }
        }  
        return $boo;
    }
    
    public function beforeFilter() {
        /** Contador de acessos */        
        $this->base_url['action'] = $this->request->params['action'];        
        parent::beforeFilter();
    }
                    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
        if ($this->request->params['action']!='admin_perfil') {
            $this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
        }  
		parent::beforeRender();
    }
        
    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
           
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'nome':
                case 'email':
                    $conditions[] = array('AND' => array("Usuario.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Usuario.{$name} =" => $value)); break;
            }
        }
        
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );

		$niveis = $this->Usuario->Nivel->find('list');
		$this->set(compact('niveis'));
        
    	$this->Usuario->recursive = 0;       
        $usuarios = $this->paginate();
        
        $this->set('usuarios',$usuarios);
    }
    
    public function admin_cadastro($id = null) {
    	$Usuario = null;
        if ($id<>null) {
            if (!$this->Usuario->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	    $this->Usuario->recursive = -1;
                $Usuario = $this->Usuario->find('first', array(
					'conditions' => array('AND'=>array('Usuario.id'=>$id)),
                ));
                //debug($Usuario); die;
                $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Usuario['Usuario']['nome'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) ); 
        
        $_old_foto = (($Usuario<>null) ? $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'] : '');
        
    	if ($this->request->is('post') || $this->request->is('put')) {   
            //debug( $this->request->data ); die;
    		if ($this->Usuario->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
                
                // Remove arquivos antigos
                if ((is_file($_old_foto))&&(!empty($this->request->data['Usuario']['foto']['name']))) { unlink($_old_foto); }
                                
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}
    	} else {
			$this->request->data = $Usuario;
		}
        
		$niveis = $this->Usuario->Nivel->find('list');
		$this->set(compact('niveis'));
    }
    
    public function admin_excluir($id = null) {
    	$this->Usuario->id = $id;
    	if (!$this->Usuario->exists()) {
    		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
    	} else {
    	   $Usuario = $this->Usuario->findById($id);
    	}
    	if ($this->Usuario->delete()) {
            @unlink( $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'] );
    		$this->Session->setFlash(__(singular($this->component_name).' exclu�do com sucesso.'), 'flash_ok');
    	} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o foi exclu�do.'), 'flash_error');
		}
    	$this->redirect(array('action' => 'index'));
    }
    
    public function admin_remover_foto($id = null) {
		$this->Usuario->id = $id;
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
        
        $Usuario = $this->Usuario->findById($id);
        $path = $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'];
        if (is_file($path)) {unlink($path); }
        
        if ($this->Usuario->saveField('foto', '')) {
			$this->Session->setFlash(__('Foto removida.'), 'flash_ok');
            $this->redirect(array('action' => 'cadastro', $Usuario['Usuario']['id'] ));
		}
		$this->Session->setFlash(__('N�o foi poss�vel remover a foto.'), 'flash_error');
		$this->redirect(array('action' => 'index'));
	}      
    
    public function admin_login() {
        if ($this->Auth->loggedIn()) {
            $this->redirect( $this->Auth->loginRedirect );
        }        
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect( array('admin'=>true, 'controller'=>'dashboard', 'action'=>'index') );
            } else {
                $this->Session->setFlash(__(singular($this->component_name).' ou senha inv�lidos.'), 'flash_error');
            }
        }
    }
    
    public function admin_logout() {
        $this->Session->destroy();
        return $this->redirect($this->Auth->logout());
    }
    
    public function admin_perfil() {
		$this->Usuario->id = $this->Auth->user('id');
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('Usu�rio inv�lido.'));
		} else {
    	   $Usuario = $this->Usuario->findById( $this->Auth->user('id') );
           $this->Breadcrumb->addBreadcrumb(array('title' => 'Perfil: '.$Usuario['Usuario']['nome'], 'url' => $this->base_url ));
    	}
        
        $_old_foto = (($Usuario<>null) ? $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'] : '');
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Usuario->id = $this->Auth->user('id'); // Garante que o usu�rio editado � o que ta logado            
            if ($this->Usuario->save($this->request->data)) {
    			$this->Session->setFlash(__('Perfil salvo com sucesso.'), 'flash_ok');
                
                // Remove arquivos antigos
                if ((is_file($_old_foto))&&(!empty($this->request->data['Usuario']['foto']['name']))) { unlink($_old_foto); }
                
                $this->request->data['after_action'] = 'referer';
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__('Perfil n�o pode ser salvo. Tente novamente.'), 'flash_error');
    		}
    	} else {
			$this->request->data = $Usuario;
		}  
	}
    
    public function admin_remover_avatar() {
		$this->Usuario->id = $this->Auth->user('id');
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('Usu�rio inv�lido.'));
            
		} else {
    	   $Usuario = $this->Usuario->findById( $this->Auth->user('id') );
    	}
        
        $path = $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'];
        if (is_file($path)) { unlink($path); }
        
        if ($Usuario['Usuario']['foto']!='') {
            if ($this->Usuario->saveField('foto', '')) {
    			$this->Session->setFlash(__('Foto removida.'), 'flash_ok');
    		} else {
    		  $this->Session->setFlash(__('N�o foi poss�vel remover a foto.'), 'flash_error');
            }
        }
		$this->redirect(array('action' => 'perfil'));
	}   
    
    
    public function admin_ajax() {
        
        $view = new View(null);
        $Timthumb = $view->loadHelper('Timthumb.Timthumb');   
        
        $this->Usuario->recursive = -1;
        $this->autoRender = false;
        $str_retorno = '';
       
        $data = $this->request->data;
        if ( (isset($data['tipo_acao'])) ) {
            $arr_retorno = $arr_erro = array();
                    
            switch( $data['tipo_acao'] ) {
                case 'busca_registro':
                    
                    $arr_conditions = array();
                    //$arr_conditions[] = array('AND' => array("Usuario.state_id" => 1) );
                    if (isset($data['busca']) && !empty($data['busca'])) {
                        
                        switch($data['campo']) {
                            case 'all':
                                $arr_conditions[] = array('OR' => array( 
                                                                array('AND'=>array('Usuario.nome LIKE' => '%' . utf8_decode($data['busca']) . '%')),
                                                                array('AND'=>array('Usuario.email LIKE' => '%' . utf8_decode($data['busca']) . '%')),
                                                                //array('AND'=>array( converteReplaceSql('Usuario.telefone_1', array('(', ')', '-', ' ')). " LIKE" => '%' . preg_replace("/\D+/", "", $data['busca'])  . '%')),
                                                                //array('AND'=>array( converteReplaceSql('Usuario.telefone_2', array('(', ')', '-', ' ')). " LIKE" => '%' . preg_replace("/\D+/", "", $data['busca'])  . '%')), 
                                                                //array('AND'=>array( converteReplaceSql('Usuario.cpf', array('.', '-')). " LIKE" => '%' . preg_replace("/\D+/", "", $data['busca'])  . '%')),
                                ) );
                                
                                $numeral = preg_replace("/\D+/", "", $data['busca']);                                
                                if (!empty($numeral)) {
                                    $arr_conditions[0]['OR'][] = array('AND'=>array( converteReplaceSql('Usuario.telefone_1', array('(', ')', '-', ' ')). " LIKE" => '%' . $numeral  . '%'));
                                    $arr_conditions[0]['OR'][] = array('AND'=>array( converteReplaceSql('Usuario.telefone_2', array('(', ')', '-', ' ')). " LIKE" => '%' . $numeral  . '%'));
                                    $arr_conditions[0]['OR'][] = array('AND'=>array( converteReplaceSql('Usuario.cpf', array('.', '-')). " LIKE" => '%' . $numeral  . '%')); 
                                }
                                
                            break;
                            
                            case 'email':
                                $arr_conditions[] = array('AND' => array("Usuario.email LIKE" => '%' . utf8_decode($data['busca']) . '%') );
                                break;
                                
                            case 'nome':
                            default:
                                $arr_conditions[] = array('AND' => array("Usuario.nome LIKE" => '%' . utf8_decode($data['busca']) . '%') );
                                break;
                        }
                    }
                    
                    $usuarios = $this->Usuario->find('all', array(
                                        'conditions' => $arr_conditions,
                                        'recursive' => -1
                    ));
                    $usuarios = $this->Usuario->arrFoto($usuarios, 'Usuario', 'foto', 'img/avatar-user.png', 70, 70);
                    
                    $str_retorno = json_encode( utf8IsoConverter( array('status' => true,
                                                                        'retorno' => $usuarios,
                    ) ) );
                break;   
            }
            
        } else {
            $str_retorno = 'Necess�rio informar tipo de busca.';
        }
        
        echo utf8_encode( $str_retorno );
        die;
    }
    
    
    
    public function cadastro() {
        $this->layout = 'site';
        
        if ($this->Auth->loggedIn()) {
            $this->redirect(array('admin'=>false, 'controller' => 'dashboard', 'action'=>'index'));
        }
        
    	if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->data; //debug($data); die;
            if (isset($data['action'])) {
                switch($data['action']) {
                    case 'login':
                        $this->request->data['Usuario'] = ( (isset($data['UsuarioLogin'])) ? $data['UsuarioLogin'] : array());
                        unset( $this->request->data['UsuarioLogin'] );

                        if ($this->Auth->login()) {
                            $this->redirect(array('controller'=>'carrinhos', 'action' => 'index'));
                            // $this->redirect( $this->Auth->loginRedirect );                            
                        } else {
                            $this->Session->setFlash('Usu�rio ou senha incorreto.', 'flash_error');
                        }
                    break;
                    
                    case 'cadastro':
                        $captcha = $this->Session->read('captcha_code');
                        if (isset($data['Usuario']['captcha']) && (strtoupper($data['Usuario']['captcha'])==strtoupper($captcha)) ) {
                            
                            $this->request->data['Usuario']['nivel_id'] = 2;
                            
                                // debug( $this->request->data ); die;
                            if ($this->Usuario->save($this->request->data)) {
                                if ($this->Auth->login()) {
                                    $this->redirect(array('controller'=>'carrinhos', 'action' => 'index'));
                                } else {
                                    $this->Session->setFlash('Ocorreu algum problema ao entrar no sistema. Tente novamente.', 'flash_error');
                                }
                            } else {
                                unset($data['Usuario']['password'], $data['Usuario']['captcha']);

                                // debug($this->Usuario->validationErrors); die();

                                $this->Session->setFlash('N�o foi poss�vel cadastrar usu�rio.', 'flash_error');
                            }
                        } else {
                            unset($data['Usuario']['password'], $data['Usuario']['captcha']);
                            $this->Session->setFlash('C�digo anti-spam incorreto.', 'flash_error');
                        }
                        
                        $this->request->data = $data;
                        
                        /** Fim Cadastro */
                    break;
                    
                    case 'esqueci_senha':
                        debug( $data );
                        
                        die; /** Fim Esqueci Senha */
                    break;
                }
            }
    	}
    }
    
    
    public function perfil() {        
        $Usuario = $this->Usuario->find('first', array('conditions'=>array('Usuario.id'=>$this->Auth->user('id')), 'recursive'=>-1)  );
        
    	if ($this->request->is('post') || $this->request->is('put')) {
            if (($this->request->data['Usuario']['password']=='')||($this->request->data['Usuario']['password']==null)) {
                unset($this->request->data['Usuario']['password']);
            }
            
            $this->request->data['Usuario']['id'] = $this->Auth->user('id');
            
    		if ($this->Usuario->save($this->request->data)) {
    			$this->Session->setFlash(__('Dados salvo com sucesso.'), 'flash_ok');
                $Usuario = $this->Usuario->find('first', array('conditions'=>array('Usuario.id'=>$this->Auth->user('id')), 'recursive'=>-1)  );
    		} else {
    			$this->Session->setFlash(__('Ops, ocorreu algum erro. Tente novamente.'), 'flash_error');
    		}
    	}
        $this->request->data = $Usuario;
    }
    
    public function remover_avatar() {
		$this->Usuario->id = $this->Auth->user('id');
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('Usu�rio inv�lido.'));
            
		} else {
    	   $Usuario = $this->Usuario->findById( $this->Auth->user('id') );
    	}
        
        $path = $this->Usuario->getPathFile('foto') . $Usuario['Usuario']['foto'];
        if (is_file($path)) { unlink($path); }
        
        if ($Usuario['Usuario']['foto']!='') {
            if ($this->Usuario->saveField('foto', '')) {
    			$this->Session->setFlash(__('Foto removida.'), 'flash_ok');
    		} else {
    		  $this->Session->setFlash(__('N�o foi poss�vel remover a foto.'), 'flash_error');
            }
        }
		$this->redirect(array('action' => 'perfil'));
	}
    
    public function sair() {
        $this->redirect($this->Auth->logout());
    }
    
    
    public function esqueci_senha() {
        if ($this->request->is('post')) {
            $email = $this->request->data['Usuario']['email'];
            
            $Usuario = $this->Usuario->findByEmail( $email );
            if($Usuario) {
                $new_pass = date('Y') . rand(1000, 9999);
                $this->request->data['Usuario']['id'] = $Usuario['Usuario']['id'];
                $this->request->data['Usuario']['password'] = $new_pass;
                if ($this->Usuario->save( $this->request->data )) {
                    
                    try {
                        $Email = new CakeEmail( 'smtp' );
                        $Email->subject('[GH Filmes] Esqueci minha senha - ' . date('d/m/Y H:i:s'));                        
                        $Email->to( $Usuario['Usuario']['email'] );
                                                
                        $Email->template( 'esqueci_senha' );
                        $Email->helpers('Html');
                        $Email->viewVars( array(
                            'url_base' => $this->webroot,
                            'usuario' => $Usuario,
                            'senha' => $new_pass,
                        ) );
                                             
                        $Email->send();
                        $this->Session->setFlash(__('Enviamos uma nova senha para seu e-mail.'), 'flash_ok');
                        
                    } catch (Exception $e) {
                        $this->Session->setFlash( $e->getMessage() , 'flash_error');                
                    }
                    
                } else {
                    $this->Session->setFlash(__('N�o foi poss�vel salvar sua nova senha.'), 'flash_error');
                }                
            } else {
                $this->Session->setFlash(__('Usu�rio n�o encontrado.'), 'flash_error');
            }
            
            $this->redirect(array('action' => 'esqueci_senha'));
        }
    }


    public function ajax() {
        
        $this->autoRender = false;
        $str_retorno = '';
       
        $data = $this->request->data;
        if ( (isset($data['tipo_acao'])) ) {
            
            $boo = false;
            $arr_erro = array();
                                 
            switch( $data['tipo_acao'] ) {
                



            }
        } else {
            $str_retorno = json_encode( utf8IsoConverter( array('status' => false,
                                                                'retorno' => 'Necess�rio informar tipo de busca.',
            ) ) );
        }
        
        echo utf8_encode( $str_retorno );
        die;
    }
        
    
}