<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

App::import('Vendor', 'autoload', array('file' => 'autoload.php'));

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class PagesController extends AppController {

    public $uses = array('Produto', 'Contato', 'CategoriaVideo', 'Conteudo');
    public $components = array('RequestHandler');

	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
        }

		// Verifica qual layout usar
		if ($path[0]=='admin') {
            $this->layout = 'admin';

		} else if ($page=='javascript') {
            $this->layout = 'javascript';

		} else if ($page=='home') {
            $this->redirect(array('controller' => 'pages', 'action' => 'display', 'index'), 301);

        } else {
            $this->layout = 'site';
        }

        if (in_array($path[0], array('index', 'home', 'contato', 'pedido-personalizado'))) {
            // // Banner APP CONTROLLER
            // $condition_banner = array();
            // $condition_banner[] = array('OR' => array(
            //                                 array('AND' => array("Banner.data_inicio <=" => date('Y-m-d')) ),
            //                                 array('AND' => array("Banner.data_inicio"=>null) )
            // ) );
            // $condition_banner[] = array('OR' => array(
            //                                 array('AND' => array("Banner.data_fim >=" => date('Y-m-d')) ),
            //                                 array('AND' => array("Banner.data_fim"=>null) )
            // ) );
            // $BannersHome = $this->Banner->find('all', array(
            //     'conditions' => array_merge($condition_banner, array(array('AND' => array('Banner.tamanho_id' => 1)))),
            //     'order' => 'RAND()',
            // ) );
            // $this->set(compact('BannersHome'));


            $parceiros = $this->Parceiro->find('all');
            $this->set(compact('parceiros'));

        }

        if (in_array($path[0], ['index', 'institucional'])) {

            $title_for_layout = '';
        //     $this->layout = 'aguarde';

        // } else if ($path[0]=='home') {


            if ($path[0]=='index') {
                $body_class = 'home';
            }

            // Categorias Home
            if (!empty($this->_configuracoes[7]['Configuracao']['configuracao'])) {
                $arr_cats = array();
                $codigos = array_filter(explode(';', $this->_configuracoes[7]['Configuracao']['configuracao']));
                foreach($codigos as $cod_semi) {
                    if (strpos($cod_semi, '-')===false) {
                        $arr_cats[] = (int) trim($cod_semi);
                    } else {
                        $aux = explode('-', $cod_semi);
                        if ($aux[0]<$aux[1]) { $ini = $aux[0]; $fim = $aux[1]; } else { $ini = $aux[1]; $fim = $aux[0]; }

                        for ($i=$ini; $i<=$fim; $i++) {
                            $arr_cats[] = (int)$i;
                        }
                    }
                }
                $arr_cats = array_filter($arr_cats);

                $categorias_home = $this->CategoriaVideo->find('all', array(
                    'recursive'=>-1, 'conditions' => array('CategoriaVideo.id'=>$arr_cats), 'limit' => 4,
                ));

            } else {
                $categorias_home = $this->CategoriaVideo->find('all', array(
                    'recursive'=>-1, 'limit' => 4, 'order' => 'rand()'
                ));
            }


            $videos_home = $this->CategoriaVideo->Video->find('all', array(
                'limit' =>4, 'recursive'=>-1, 'order' => array('Video.destaque'=>'DESC', 'RAND()')
            ));

            $parceiros = $this->Parceiro->find('all', array( 'recursive'=>-1, 'order' => array('RAND()') ));


            // debug( $parceiros ); die;
            
            $Conteudo = $this->Conteudo->findById(1);
            $this->set(compact('Conteudo'));
            $this->set(compact('body_class', 'categorias_home', 'videos_home', 'parceiros'));

        }


        // Send e-mail
        if ( ($this->request->is('post') || $this->request->is('put')) ) {

            $data = $this->request->data;
            if (array_key_exists('Contato', $data)) {
                $this->Contato->set( $this->data );

                if ($this->Contato->validates()) {

                    $captcha = $this->Session->read('captcha_code');

                    //debug($data);                 debug($captcha); die;
                    if (isset($data['Contato']['captcha']) && (strtoupper($data['Contato']['captcha'])==strtoupper($captcha)) ) {
                        try {

                            // Busca e-mail de contato
                            $this->loadModel('Configuracao');
                            $config = $this->Configuracao->find('all');

                            $Email = new CakeEmail( 'smtp' );
                            $Email->subject('[GH Filmes] '. (($data['Contato']['tipo_contato']=='pedido') ? 'Pedido personalizado ' : 'Contato ') .' em ' . date('d/m/Y H:i:s'));

                            $Email->replyTo( $data['Contato']['email'] );
                            $Email->to( $config[1]['Configuracao']['configuracao'] );

                            $Email->template( 'contato' );
                            $Email->helpers('Html');
                            $Email->viewVars( array(
                                'url_base' => $this->webroot,
                                'data' => $data,
                            ) );
                            $boo = $Email->send();

                            if ($boo) {
                                unset($this->request->data['Contato']);
                                $this->Session->setFlash(__('Mensagem enviada com sucesso.'), 'flash_ok');
                            } else {
                                $this->Session->setFlash('N�o foi poss�vel enviar.', 'flash_error');
                            }

                        } catch (Exception $ex) {
                            $this->Session->setFlash('N�o foi poss�vel enviar o e-mail.', 'flash_error');
                        }
                    } else {
                        $this->Session->setFlash('C�digo anti-spam incorreto.', 'flash_error');
                    }
                    unset($this->request->data['Contato']['captcha']);

                } else {
                    $this->Session->setFlash('Favor preencher os campos obrigat�rios.', 'flash_error');
                }
            }
        }



        $this->set( 'isMobile',  $this->RequestHandler->isMobile()    );
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}

    public function admin_icones() { }
}
