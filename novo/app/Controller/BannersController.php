<?php
App::uses('AppController', 'Controller');
class BannersController  extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'banners', 'action' => 'admin_index');
	var $component_name = 'Banner';
    
    public function isAuthorized($user = null) {        
        $boo = parent::isAuthorized($user);
        if ($boo) {
            if ($this->Auth->user('nivel_id')==1) {
                $boo = true;
            } else {
                $boo = false;
            }
        }
        return $boo;
    }

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Banner->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Banner.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Banner.{$name} =" => $value)); break;
            }
        }
                
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Banner->recursive = 0;
        $banners = $this->paginate();
		$this->set('banneres', $banners);
        
        $tamanhos = $this->Banner->Tamanho->find('list');
        $this->set(compact( 'tamanhos' ));
	}


	public function admin_cadastro($id = null) { 
		$Banner = null;
		if ($id<>null) {
            if (!$this->Banner->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Banner = $this->Banner->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Banner['Banner']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
        $_old_foto = (($Banner<>null) ? $this->Banner->getPathFile('foto') . $Banner['Banner']['foto'] : '');
        
		if ($this->request->is(array('post', 'put'))) {
		
            // Remove arquivos antigos
            if ((is_file($_old_foto))&&(!empty($this->request->data['Banner']['foto']['name']))) { unlink($_old_foto); }
                
			if ($this->Banner->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Banner;
		}
        
        
        $tamanhos = $this->Banner->Tamanho->find('list');
        $this->set(compact( 'tamanhos' ));
	}

	public function admin_excluir($id = null) {	
		$this->Banner->id = $id;
		if (!$this->Banner->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Banner->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
