<?php
App::uses('AppController', 'Controller');
class ConteudosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'conteudos', 'action' => 'admin_index');
	var $component_name = 'Conteudo';
    
    public function isAuthorized($user = null) {        
        $boo = parent::isAuthorized($user);
        if ($boo) {
            if ($this->Auth->user('nivel_id')==1) {
                $boo = true;
            } else {
                $boo = false;
            }
        }
        return $boo;
    }

    public function beforeFilter() {        
        $this->base_url['action'] = $this->request->params['action'];
        $this->Conteudo->recursive = -1;

        $this->set('title_for_layout', ( ucwords($this->params['type']) ));
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
		
		$this->Breadcrumb->addBreadcrumb(array('title' => ( ucwords($this->params['type']) ), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Conteudo.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Conteudo.{$name} =" => $value)); break;
            }
        }
        
        switch($this->params['type']) {
            case 'blog':
                $conditions[] = array('AND' => array("Conteudo.tipo_id =" => 1));
            break;
            case 'conteudos':
                $conditions[] = array('AND' => array("Conteudo.tipo_id =" => 2));
            break;
        }
        
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Conteudo->recursive = 0;
		$this->set('conteudos', $this->paginate());
	}
    
	public function admin_cadastro($id = null) { 
		$Conteudo = null;
		if ($id<>null) {
            if (!$this->Conteudo->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Conteudo = $this->Conteudo->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Conteudo['Conteudo']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
        $_old_foto = (($Conteudo<>null) ? $this->Conteudo->getPathFile('foto') . $Conteudo['Conteudo']['foto'] : '');
        
		if ($this->request->is(array('post', 'put'))) {
		  
            switch($this->params['type']) {
                case 'blog':
                    $this->request->data['Conteudo']['tipo_id'] = 1;
                break;
                case 'conteudos':
                    $this->request->data['Conteudo']['tipo_id'] = 2;
                break;
            }
            
            
			if ($this->Conteudo->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                // Remove arquivos antigos
                if ((is_file($_old_foto))&&(!empty($this->request->data['Conteudo']['foto']['name']))) { unlink($_old_foto); }
                
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Conteudo;
		}
	}
    
	public function admin_excluir($id = null) {	
		$this->Conteudo->id = $id;
		if (!$this->Conteudo->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Conteudo->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
    
    public function index( $id=null ) {
        
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();        
        switch($this->params['type']) {
            case 'blog':
                $conditions[] = array('AND' => array("Conteudo.tipo_id =" => 1));
            break;
            case 'conteudos':
                $conditions[] = array('AND' => array("Conteudo.tipo_id =" => 2));
            break;
        }

        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'keyword':
                    $conditions[] = array('OR' => array( 
                        array('AND' => array("Conteudo.conteudo LIKE" => '%' . $value . '%') ), 
                        array('AND' => array("Conteudo.titulo LIKE" => '%' . $value . '%') )
                    ) ); break;
            }
        }
        
        
		$this->Conteudo->recursive = 0;
        
        $this->paginate = array( 'conditions' => $conditions, 'order'=>array('Conteudo.id'=>'DESC') );
        $conteudos = $this->paginate();
		$this->set('conteudos', $conteudos);
    }
    
    
    public function ver( $id=null ) {
        if ($id<>null) {
            if (!$this->Conteudo->exists($id)) {
                throw new NotFoundException(__('P�gina n�o encontrada.'));
        	} else {
        	   $this->Conteudo->recursive = 1;
                $Conteudo = $this->Conteudo->findById($id);
                $Conteudo = $this->Conteudo->arrFoto($Conteudo, 'Conteudo', 'foto', 'img/placeholder.jpg', 100, 100);

                $this->set('title_for_layout', ($this->component_name) . ' - '. $Conteudo['Conteudo']['titulo']);
        	}
    	} else {
    	   throw new NotFoundException(__('P�gina n�o encontrada.'));    	   
        }
        
        $title_page = $Conteudo['Conteudo']['titulo'];
        $keywords = $Conteudo['Conteudo']['keyword'];
        $description = $Conteudo['Conteudo']['description'];
        
        
        if ($this->params['type']=='servicos') {
            $this->set('body_class', 'page_servicos');
        }
        $this->set('title_for_layout', $title_page);
		$this->set(compact('Conteudo', 'keywords', 'description'));
	}
    
}
