<?php
App::uses('AppController', 'Controller');
class NiveisController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'niveis', 'action' => 'admin_index');
	var $component_name = 'N�vel';
    
    public function isAuthorized($user = null) {        
        $boo = parent::isAuthorized($user);
        if ($boo) {
            if ($this->Auth->user('nivel_id')==1) {
                $boo = true;
            } else {
                $boo = false;
            }
        }
        return $boo;
    }

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];        
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
        $conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Nivel.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Nivel.{$name} =" => $value)); break;
            }
        }
           
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Nivel->recursive = 0;
		$this->set('niveis', $this->paginate());
	}


	public function admin_cadastro($id = null) {         
		$Nivel = null;
		if ($id<>null) {
            if (!$this->Nivel->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Nivel = $this->Nivel->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Nivel['Nivel']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Nivel->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Nivel;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Nivel->id = $id;
		if (!$this->Nivel->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Nivel->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
