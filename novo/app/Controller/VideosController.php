<?php
App::uses('AppController', 'Controller');
class VideosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'videos', 'action' => 'admin_index');
	var $component_name = 'Video'; //Arrumar
	public $components = array('Paginator', 'Session');//Arrumar

public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Video->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
		
		$conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Video.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Video.{$name} =" => $value)); break;
            }
        }
                
		$categoriaVideos = $this->Video->CategoriaVideo->find('list');
		$this->set(compact('categoriaVideos'));
   
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Video->recursive = 0;
		$this->set('videos', $this->paginate());
	}


	public function admin_cadastro($id = null) { 
		$Video = null;
		if ($id<>null) {
            if (!$this->Video->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Video = $this->Video->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Video['Video']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		$_old_foto = (($Video<>null) ? $this->Video->getPathFile('foto') . $Video['Video']['foto'] : '');
		
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Video->save($this->request->data)) {
				
				// Remove arquivos antigos
				if ((is_file($_old_foto))&&(!empty($this->request->data['Video']['foto']['name']))) { unlink($_old_foto); }

    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Video;
		}
		$categoriaVideos = $this->Video->CategoriaVideo->find('list');
		$this->set(compact('categoriaVideos'));
	}

	public function admin_excluir($id = null) {	
		$this->Video->id = $id;
		if (!$this->Video->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Video->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function categorias($selected_category_id=null) {


		$categorias = $this->Video->CategoriaVideo->find('all', array(
			'fields' => array('CategoriaVideo.*', 'COUNT(Video.id) as qtde_videos'),
			'recursive'=>-1,
			'group' => 'CategoriaVideo.id',
			'joins' => array(
				array(
					'table' => 'videos',
					'type' => 'LEFT',
					'alias' => 'Video',
					'conditions'=> array('CategoriaVideo.id=Video.categoria_video_id')
				),
			)
		));

		// debug( $this->Video->getDataSource()->getLog(false, false) );
		// debug($categorias); die;

		$videos = Array();
		if ($selected_category_id) {
			$videos = $this->Video->find('all', array('recursive'=>-1, 'conditions'=>array('Video.categoria_video_id'=>$selected_category_id)));
		} else {
            $videos = $this->Video->find('all', array(
                'limit' =>3, 'recursive'=>-1, 'order' => array('Video.destaque'=>'DESC', 'RAND()')
            ));			
		}

		$this->set(compact('categorias', 'selected_category_id', 'videos'));
	}

	public function assistir($id=null) {
		if (!$this->Video->exists($id)) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		} else {
			$Video = $this->Video->find('first', array('recursive'=>-1, 'conditions'=>array('Video.id'=>$id)));
		}

		// debug( $this->Video->getDataSource()->getLog(false, false) );
		// debug($Video); die;
		
        $title_page = $Video['Video']['titulo'];
        $keywords = $Video['Video']['keyword'];
        $description = $Video['Video']['description'];
        
        $this->set('title_for_layout', $title_page);
		$this->set(compact('Video', 'keywords', 'description'));
	}
}
