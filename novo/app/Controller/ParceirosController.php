<?php
App::uses('AppController', 'Controller');
class ParceirosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'parceiros', 'action' => 'admin_index');
	var $component_name = 'Parceiro';

	public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Parceiro->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
		$conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Parceiro.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Parceiro.{$name} =" => $value)); break;
            }
        }
                
   
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Parceiro->recursive = 0;
		$this->set('parceiros', $this->paginate());
	}


	public function admin_cadastro($id = null) { 
		$Parceiro = null;
		if ($id<>null) {
            if (!$this->Parceiro->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Parceiro = $this->Parceiro->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Parceiro['Parceiro']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );

        $_old_foto = (($Parceiro<>null) ? $this->Parceiro->getPathFile('foto') . $Parceiro['Parceiro']['foto'] : '');
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Parceiro->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                // Remove arquivos antigos
				if ((is_file($_old_foto))&&(!empty($this->request->data['Parceiro']['foto']['name']))) { unlink($_old_foto); }
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Parceiro;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Parceiro->id = $id;
		if (!$this->Parceiro->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Parceiro->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function index() {
		$parceiros = $this->Parceiro->find('all');

		$this->set(compact('parceiros'));
	}
}
