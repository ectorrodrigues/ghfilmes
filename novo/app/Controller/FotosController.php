<?php
App::uses('AppController', 'Controller');
class FotosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'fotos', 'action' => 'admin_cadastro');
	var $component_name = 'Foto';
    
    public function isAuthorized($user = null) {        
        $boo = parent::isAuthorized($user);
        if ($boo) {
            if ($this->Auth->user('nivel_id')==1) {
                $boo = true;
            } else {
                $boo = false;
            }
        }
        return $boo;
    }

    public function beforeFilter() {
        $this->set('title_for_layout', plural($this->component_name));
        $this->base_url['action'] = $this->request->params['action'];
        $this->Foto->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
		
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Conte�dos', 'url' => array('controller'=>'conteudos') ));
		parent::beforeRender();
    }
    
    public function admin_cadastro($id=null) {
        $this->set('bodyClass', 'dynamic-gallery-page');
        $this->Breadcrumb->addBreadcrumb(array('title' => 'Fotos'));
        
        $Conteudo = null;
        
        if ((!$id) && (!$this->Foto->Conteudo->exists($id))) {
            throw new NotFoundException('N�o foi poss�vel identificar conteudo.');
        } else {
            $this->Foto->Conteudo->recursive = -1;
            $Conteudo = $this->Foto->Conteudo->findById($id);
        }
        
        $this->set('Conteudo', $Conteudo);
	}
    
    public function admin_ajax() {
        
        $this->autoRender = false;
        $str_retorno = '';
       
        $data = $this->request->data;
        $file = $this->params->form['arquivo'];
        
        
        if ( (isset($data['tipo_acao'])) ) {
            
            $boo = false;
            $arr_erro = array();
            $arr_cadastro = array();
            
            $conteudo_id = null;
            if (isset($data['conteudo_id'])) {
                $conteudo_id = $data['conteudo_id'];
            }
                                 
            switch( $data['tipo_acao'] ) {
                case 'upload':                
                    if (!$conteudo_id) { // Verifica conte�do
                        $boo = false;
                        $arr_erro['id'] = array('N�o foi poss�vel identificar conte�do.');
                        $str_retorno = json_encode( utf8IsoConverter( array('status'=>$boo,
                                                                            'error_input'=> ( $arr_erro )) )
                        );
                        echo utf8_encode( $str_retorno );
                        die;
                    }
            
                    $boo = false;
                    $arr_cadastro['Foto']['conteudo_id'] = $conteudo_id;
                    $arr_cadastro['Foto']['arquivo'] = $file;
                                     
                    if ($this->Foto->save( $arr_cadastro )) {
                        $boo = true;
                    } else {
                        $arr_erro = $this->Foto->validationErrors;
                    }
                                        
                    $str_retorno = json_encode( utf8IsoConverter( array('status'=>$boo,
                                                                        'retorno' => $arr_cadastro,
                                                                        'error_input'=> ( $arr_erro )) )
                    );
                break;                
                
                case 'listar_fotos':                    
                    if (!$conteudo_id) { // Verifica conte�do
                        $boo = false;
                        $arr_erro['id'] = array('N�o foi poss�vel identificar conte�do.');
                        $str_retorno = json_encode( utf8IsoConverter( array('status'=>$boo,
                                                                            'error_input'=> ( $arr_erro )) )
                        );
                        echo utf8_encode( $str_retorno );
                        die;
                    }
                
                    $fotos = array();
                    $boo = true;
                                   
                    $arr_conditions = array();
                    $arr_conditions[] = array('AND' => array("Foto.conteudo_id" => $conteudo_id) );
                    $fotos = $this->Foto->find('all', array(
                                        'conditions' => $arr_conditions,
                                        'recursive' => -1
                    ));
                    $fotos = $this->Foto->arrFoto($fotos, 'Foto', 'arquivo', null, 160, 120);
                    
                    $str_retorno = json_encode( utf8IsoConverter( array('status'=>$boo,
                                                                        'retorno' => $fotos,
                                                                        'error_input'=> ( $arr_erro )) )
                    );
                break;
                
                case 'excluir_foto':                    
                    if (isset($data['id'])) {
                        $this->Foto->id = $data['id'];
                    	if (!$this->Foto->exists()) {
                            $arr_erro['arquivo'] = array( __(singular($this->component_name).' inv�lido.') );
                        } else {
                            $this->Foto->recursive=-1;
                            $Foto = $this->Foto->findById($data['id']);                                                        
                            if ($this->Foto->delete()) {
                                
                                if (isset($data['conteudo_id'])) {
                                    $this->Foto->Conteudo->id = $data['conteudo_id'];
                                    if ($this->Foto->Conteudo->exists()) {
                                        $this->Foto->Conteudo->recursive=-1;
                                        $Conteudo = $this->Foto->Conteudo->findById($data['conteudo_id']);
                                        
                                    }
                                }
                                
                                $boo = true;
                                @unlink( $this->Foto->getPathFile('arquivo') . $Foto['Foto']['arquivo'] );
                            } else {
                                $arr_erro['arquivo'] = array( __(singular($this->component_name).' n�o foi exclu�do.') );
                            }
                    	}
                    } else {
                        $arr_erro['id'] = array('C�digo n�o informado.');   
                    }
                    
                    $str_retorno = json_encode( utf8IsoConverter( array('status'=>$boo,
                                                                        'error_input'=> ( $arr_erro )) )
                    );
                break;
                
            }
            
        } else {
            $str_retorno = 'Necess�rio informar tipo de busca.';
        }
        
        echo utf8_encode( $str_retorno );
        die;
    }
    
}
