<?php
App::uses('Controller', 'Controller');

class AppController extends Controller {

	public $uses = array( 'Conteudo', 'Banner', 'Configuracao', 'Newsletter', 'Parceiro');
    public $helpers = array( 'Timthumb.Timthumb', 'Breadcrumb', 'GFunction' );
    public $components = array(
        'Search', 'Breadcrumb', 'Session', 'Auth','Captcha',
    );
    public $time_filter = '-1 minutes'; // Tempo que grava��o filtro da pesquisa
    public $userLogado;
    public $_configuracoes;

    public function isAuthorized($user = null) {
        $boo = true;
        if (isset($this->params['prefix']) && ($this->params['prefix']=='admin')) {
            if ($this->params['action']=='admin_logout') {
                $boo=true;
            } else {
                if (($this->Auth->user('nivel_id')==1)||($this->Auth->user('nivel_id')==2)) {
                    $boo = true;
                } else {
                    $boo = false;
                }
            }
        }
        return (boolean)$boo;
    }

    public function beforeRender() {
		$this->Breadcrumb->addBreadcrumb(array('title' => '<i class="fa fa-home"></i> Home', 'url' => array('admin'=>true, 'controller' => 'dashboard', 'action' => 'index') ));
		$this->set('breadcrumbs', $this->Breadcrumb->getBreadcrumbs());

        /** Newsletter */
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->data;
            if ((isset($data['Newsletter'])) && (!empty($data['Newsletter']['email']))) {
                if ($this->Newsletter->save($data)) {
                    unset($this->request->data['Newsletter']['email']);
                    $this->Session->setFlash(__('Inscri��o realizada com sucesso.'), 'flash_ok');
                } else {
                    $this->Session->setFlash(__('N�o foi poss�vel fazer a inscri��o.'), 'flash_error');
                }
            }
        }

        if ($this->name=='CakeError') {
            if (!$this->_isAdminMode()) {
                $this->layout = 'error';
            }
        }
    }

    public function beforeFilter() {
        /** Auth */
        $this->Auth->unauthorizedRedirect = false;
        $this->Auth->authError = "Sem permiss�o para acessar.";

        if ($this->_isAdminMode()) {
            $this->layout = 'admin';

            AuthComponent::$sessionKey = 'Auth.Admin';
            $this->Auth->loginAction = array('admin'=>true, 'controller' => 'usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('admin'=>true, 'controller' => 'dashboard', 'action' => 'index');
            $this->Auth->logoutRedirect = array('admin'=>true, 'controller' => 'usuarios', 'action' => 'login');
            $this->Auth->authorize = 'Controller';
            $this->Auth->authenticate = array(
                'Form' => array(
                    'userModel' => 'Usuario',
                    'fields' => array('username' => 'email'),
                    'scope' => array('Usuario.nivel_id' => array(1) )
                )
            );
            $this->Auth->deny();
            $this->Auth->allow('login');

        } else { // Site


            $this->layout = 'site';

            AuthComponent::$sessionKey = 'Auth.Site';
            $this->Auth->loginAction = array('admin'=>false, 'controller' => 'usuarios', 'action' => 'cadastro');
            $this->Auth->loginRedirect = array('admin'=>false, 'controller' => 'dashboard', 'action' => 'index');
            $this->Auth->logoutRedirect = array('admin'=>false, 'controller' => 'usuarios', 'action' => 'cadastro');
            $this->Auth->authorize = 'Controller';
            $this->Auth->authenticate = array(
                'Form' => array(
                    'userModel' => 'Usuario',
                    'fields' => array('username' => 'email'),
                )
            );
            if (in_array($this->request->params['controller'], array('pedidos')) && ($this->request->params['action']<>'transacao')) {
                $this->Auth->deny();
                $this->Auth->allow('login');
                //debug($this->request->params['controller']); die;
            } else {
                $this->Auth->allow();
            }




            /** Busca variaveis */
            $menus = $this->Conteudo->find('all', array(
                'conditions'=>array('AND'=>array('Conteudo.tipo_id'=>1)),
                'order' => array('Conteudo.ordem'=>'ASC'), 'recursive' => -1
            ) );
            $configuracoes = $this->Configuracao->find('all');
            $configuracoes = Set::combine($configuracoes, '{n}.Configuracao.id', '{n}');
            // debug($configuracoes);
            $this->_configuracoes = $configuracoes;

            // Banner
            $condition_banner = array();
            $condition_banner[] = array('OR' => array(
                                            array('AND' => array("Banner.data_inicio <=" => date('Y-m-d')) ),
                                            array('AND' => array("Banner.data_inicio"=>null) )
            ) );
            $condition_banner[] = array('OR' => array(
                                            array('AND' => array("Banner.data_fim >=" => date('Y-m-d')) ),
                                            array('AND' => array("Banner.data_fim"=>null) )
            ) );
            $BannersHome = $this->Banner->find('all', array(
                'conditions' => array_merge($condition_banner, array(array('AND' => array('Banner.tamanho_id' => 1)))),
                'order' => 'RAND()',
            ) );
            $this->set(compact('BannersHome'));

            $parceiros = $this->Parceiro->find('all');
            $this->set(compact('parceiros'));

            $this->set(compact('menus', 'configuracoes'));
        }
        $this->Auth->allow('image');

        $usuario = ClassRegistry::init('Usuario');
        $usuario = $usuario->find('first', array(
                                            'recursive'=>0,
                                            'conditions' => array('Usuario.id' => $this->Auth->user('id')),
                                            'limit' => 1,
        ) );

        if (count($usuario)==0) {
            $usuario = null;
        } else {
            //debug($usuario); die;
        }

        $this->set('userLogado', $usuario);
        $this->userLogado = $usuario;

        $arr_carrinho = (($this->Session->read('carrinho')) ? $this->Session->read('carrinho') : array() );
        $this->set('qtd_produto_carrinho', count($arr_carrinho));
        /*
        // Banner
        $condition_banner = array();
        $condition_banner[] = array('OR' => array(
                                        array('AND' => array("Banner.data_inicio <=" => date('Y-m-d')) ),
                                        array('AND' => array("Banner.data_inicio"=>null) )
        ) );
        $condition_banner[] = array('OR' => array(
                                        array('AND' => array("Banner.data_fim >=" => date('Y-m-d')) ),
                                        array('AND' => array("Banner.data_fim"=>null) )
        ) );
        $BannersInternas = $this->Banner->find('all', array(
            'conditions' => array_merge($condition_banner, array(array('AND' => array('Banner.tamanho_id' => 2)))),
            'order' => 'RAND()',
        ) );
        $this->set(compact('BannersInternas')); */
    }

    public function _isAdminMode() {
        $adminRoute = Configure::read('Routing.prefixes');
        if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
            return true;
        } else {
            return false;
        }
    }

    public function redirectBeforeSaveForm() {
		$to = (isset($this->request->data['after_action']) ? $this->request->data['after_action'] : '');

		switch( $to ) {
			case 'back':
				$this->redirect(array('action' => 'index')); break;
			case 'edit':
				$this->redirect(array('action' => 'cadastro', $this->{$this->modelClass}->id )); break;
			case 'save':
				$this->redirect(array('action' => 'cadastro')); break;
			case 'referer':
				$this->redirect($this->referer(array('action' => 'index'))); break;
			default:
				$this->redirect(array('action' => 'cadastro')); break;
		}
    }


}
