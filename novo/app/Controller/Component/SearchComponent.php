<?php
App::uses('Component', 'Controller');
class SearchComponent extends Component {
    
    var $components = array('Session');
    
    private $arr_condition = array();
    
    public function startup(Controller $controller) {        
		$this->controller = $controller;
    }

    
    /*  
     * @usage Verify exist parameters to filter
     * @return array 
     */ 
    public function getCondition() {
        
        $aux_filter = array(); $reset = false;
        $this->arr_condition = array();
        
        $model = $this->controller->modelClass;
        $named = $this->controller->request->params['named']; 
        
        $str_controller_action = $this->controller->request->params['controller'].'.'.$this->controller->request->params['action'];
        $filtro = (($this->Session->check('Filter.List'))  ?  unserialize($this->Session->read('Filter.List'))  :  array()) ;
        $filter_url = array('controller' => $this->controller->request->params['controller'], 'action' => $this->controller->request->params['action'], 'page' => 1);
        
        //debug( $named );
        //debug( $filtro );
        //debug( $filter_url ); die;

        // Direciona Form        
        
        // Filtra pela Form Filtro
        if ( isset($this->controller->data['Filter']) ) {
            
            foreach ($this->controller->data['Filter'] as $name => $value) {
                if ($value) { $filter_url[$name] = ($value); }
            }
            return $this->controller->redirect($filter_url); die;
            
        } else if (count($named)==0) { // Limpa Form
        
            if ( isset($filtro[$str_controller_action]) && isset($this->controller->request->params['prefix']) && ($this->controller->request->params['prefix']=='admin') ) {
                foreach ($filtro[$str_controller_action] as $name => $value) {
                    if (($value)&&($name<>'time')) { $filter_url[$name] = urldecode($value); }
                } 
                $filter_url['page'] = 1;
                return $this->controller->redirect( $filter_url); die;   
            }
            
        // Filtra pela URL
        } else if (count($named)>0) {
            foreach ($named as $name => $value) {
                if (!in_array($name, array('page','sort', 'direction', 'limit', 'controller', 'action', 'time'))) {                    
                    switch( $name ) {
                        case 'limpar_filtro': $aux_filter[] = array(); $reset=true; break(2);
                        default: $this->arr_condition[$name] = $value; break;
                    }
                    $this->controller->request->data['Filter'][$name] = $value;
                }
                $aux_filter[$name] = ($value);
            }
            $aux_filter['time'] = strtotime('now');
            $filtro[$this->controller->request->params['controller'].'.'.$this->controller->request->params['action']] = $aux_filter;
            
        // Direciona Session
        } else if ( isset($filtro[$str_controller_action]) ) {
            
            if ($filtro[$str_controller_action]['time']>strtotime($this->controller->time_filter)) {
                foreach ($filtro[$str_controller_action] as $name => $value) {
                    if (($value)&&($name<>'time')) { $filter_url[$name] = urldecode($value); }
                }                
                return $this->controller->redirect( $filter_url); die;                        
            } else {
                unset($filtro[$str_controller_action]);
            }
        }
        $this->Session->write('Filter.List', serialize($filtro));
        if ($reset) { $this->controller->redirect(array('action'=>'index')); }
        return $this->arr_condition; 
    } 
    
    
    /*  
     * @usage Verify exist parameters to filter
     * @return array 
     */ 
    public function getSort() {        
        $aux_filter = array();        
        $named = $this->controller->request->params['named'];
        
        if (count($named)>0) {
            foreach ($named as $name => $value) {
                if (in_array($name, array('sort', 'direction'))) {
                    $aux_filter[$name] = ($value);
                }
            }
        }
        return $aux_filter; 
    }
    
    /*  
     * @usage Verify exist parameters to filter
     * @return array 
     */ 
    public function getLimit($limit = 25) {        
        $aux_filter = array();        
        $named = $this->controller->request->params['named'];
        
        if (count($named)>0) {
            foreach ($named as $name => $value) {
                if (in_array($name, array('limit'))) {
                    $limit = ($value);
                }
            }
        }
        return $limit; 
    }  
} 
?>