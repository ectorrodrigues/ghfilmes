<?php
App::uses('AppController', 'Controller');
class AjaxController extends AppController {

	public $name = 'Ajax';
    public $uses = array('ValorFrete', 'Produto');

	public function buscaCep() {
        $this->autoRender = false;
		$param = $this->request->data;
                
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep=". $param['cep']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        echo $retorno = curl_exec($ch);
        curl_close($ch);
        die;
	}
    
    public function get_captcha() {
        $this->autoRender = false;
    		
        $random = generateRandomString(5, '123456789ABCDEFGHIJKLMPRSTUVWXYZ');
        $this->Session->write('captcha_code', $random);  
    		
        $settings = array(
            'characters' => $random,
            'winHeight' => 50,         // captcha image height 
            'winWidth' => 200,		   // captcha image width
            'fontSize' => 22,          // captcha image characters fontsize 
            'fontPath' => WWW_ROOT . 'fonts/tahomabd.ttf',    // captcha image font
            'noiseColor' => '#ccc',
            'lineNoise' => true,
            'bgColor' => '#fff',
            'noiseLevel' => '100',
            'textColor' => '#000',
            'lineColor' => '#ccc'
        );
    		
        $img = $this->Captcha->ShowImage( $settings );
        echo $img; 
    }

	public function frete() {
        $this->autoRender = false;
        $data = $this->request->data;

        if (!empty($data['cep'])) {
            $this->Session->write('cep_frete', $data['cep']);
        }

        // Busca CEP origem
        $this->loadModel('Configuracao');
        $config = $this->Configuracao->find('all');
        $cep_origem = $config[9]['Configuracao']['configuracao'];

        $this->Produto->recursive = -1;
        $Produto = $this->Produto->find('first', array(
            'conditions' => array('Produto.id'=>$data['id_produto']), 'recursive'=>-1, 'contain'=>array('TamanhoProduto'=>array('TamanhoFrete'))
        ));
        if (count($Produto)>0) {
            $Produto['TamanhoProduto'] = Set::combine($Produto['TamanhoProduto'], '{n}.id', '{n}');

            $key_produto = $data['id_produto'].'-'.$data['id_tamanho'];
            $arr_carrinho = array( $key_produto => $Produto );
            $arr_carrinho[$key_produto]['qtde'] = $data['qtde'];
            $arr_carrinho[$key_produto]['tamanho_produto_id'] = $data['id_tamanho'];
            
            // Captura tamanho da embalagem (unificada)
            $embalagens = $this->ValorFrete->montaCaixas( $arr_carrinho, false );
            if (count($embalagens)>0) {
                $arr_correio = array(
                    'PAC' => array('valor'=>0, 'prazo'=>0, 'embalagens'=>0),
                    'SEDEX' => array('valor'=>0, 'prazo'=>0, 'embalagens'=>0),
                    'erro'=>false
                );
                foreach($embalagens as $embalagem) {
                    $frete = calculaFrete('41106,40010', str_replace('-','',$cep_origem), str_replace('-','',$data['cep']), ceil($embalagem['peso']/1000), $embalagem['altura'], $embalagem['largura'], $embalagem['comprimento'], 0, $embalagem['valor'] );
                    foreach($frete['cServico'] as $freteCorreio) {
                        if ($freteCorreio['Erro']==0) {
                            if ($freteCorreio['Codigo']==41106) { // PAC
                                $arr_correio['PAC']['valor'] += converteMoedaDouble($freteCorreio['Valor']);
                                $arr_correio['PAC']['prazo'] = $freteCorreio['PrazoEntrega'];
                                $arr_correio['PAC']['embalagens']++;

                            } else if ($freteCorreio['Codigo']==40010) { //SEDEX
                                $arr_correio['SEDEX']['valor'] += converteMoedaDouble($freteCorreio['Valor']);
                                $arr_correio['SEDEX']['prazo'] = $freteCorreio['PrazoEntrega'];
                                $arr_correio['SEDEX']['embalagens']++;
                            }
                            
                        } else {
                            $arr_correio['erro'] = $frete['MsgErro'];
                        }
                    }
                }
                $arr_correio['PAC']['valor'] = converteDoubleMoeda($arr_correio['PAC']['valor']);
                $arr_correio['SEDEX']['valor'] = converteDoubleMoeda($arr_correio['SEDEX']['valor']);

                $arr = array('status'=> ($arr_correio['erro']==false), 'msg'=>$arr_correio);

            } else {
                $arr = array('status'=>false, 'msg'=>'N�o gerou embalagens.');
            }
        } else {
            $arr = array('status'=>false, 'msg'=>'N�o foi poss�vel encontrar produto.');
        }
        
        // debug($arr); die;
        echo json_encode(utf8IsoConverter($arr));
        die;
	}
}
