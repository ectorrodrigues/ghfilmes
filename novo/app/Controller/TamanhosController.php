<?php
App::uses('AppController', 'Controller');
class TamanhosController extends AppController {

	public $base_url = array('admin'=>true, 'controller' => 'tamanhos', 'action' => 'admin_index');
	var $component_name = 'Tamanho';

    public function beforeFilter() {
        $this->base_url['action'] = $this->request->params['action'];
        $this->Tamanho->recursive = -1;
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $this->set('base_url', $this->base_url );
        $this->set('title_for_layout', plural($this->component_name));
		
		$this->Breadcrumb->addBreadcrumb(array('title' => plural($this->component_name), 'url' => array_merge($this->base_url, array('action'=>'index')) ));
		parent::beforeRender();
    }

    public function admin_index() {
        $arr_conditions = $this->Search->getCondition();
        
				$conditions = array();
        foreach($arr_conditions as $name=>$value) {
            switch( $name ) {
                case 'titulo':
                    $conditions[] = array('AND' => array("Tamanho.{$name} LIKE" => '%' . $value . '%') ); break;
                default:
                    $conditions[] = array('AND' => array("Tamanho.{$name} =" => $value)); break;
            }
        }
                
   
        if (!isset($limit)) { $limit = 25; }
        $this->paginate = array( 'conditions' => $conditions, 'limit' => $limit );
        
		$this->Tamanho->recursive = 0;
		$this->set('tamanhos', $this->paginate());
	}


	public function admin_cadastro($id = null) { 
		$Tamanho = null;
		if ($id<>null) {
            if (!$this->Tamanho->exists($id)) {
        		throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
        	} else {
        	   $Tamanho = $this->Tamanho->findById($id);
               $this->Breadcrumb->addBreadcrumb(array('title' => $id.': '.$Tamanho['Tamanho']['titulo'], 'url' => array_merge($this->base_url,array($id)) ));
        	}
    	}
		$this->Breadcrumb->addBreadcrumb(array('title' => 'Cadastro', 'url' => $this->base_url ) );
			
		if ($this->request->is(array('post', 'put'))) {
		
			if ($this->Tamanho->save($this->request->data)) {
    			$this->Session->setFlash(__(singular($this->component_name).' gravado com sucesso.'), 'flash_ok');
				
                $this->redirectBeforeSaveForm(); // Direciona Form
    		} else {
    			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser gravado. Tente novamente.'), 'flash_error');
    		}	
		
		} else {
			$this->request->data = $Tamanho;
		}
	}

	public function admin_excluir($id = null) {	
		$this->Tamanho->id = $id;
		if (!$this->Tamanho->exists()) {
			throw new NotFoundException(__(singular($this->component_name).' inv�lido.'));
		}
		if ($this->Tamanho->delete()) {
			$this->Session->setFlash(__(singular($this->component_name).' exclu�do.'), 'flash_ok');
		} else {
			$this->Session->setFlash(__(singular($this->component_name).' n�o pode ser exclu�do.'), 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
